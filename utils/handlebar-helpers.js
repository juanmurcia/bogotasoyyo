"use strict";

const moment = require('moment');
const _ = require('lodash');
const m2f = require('mimetype-to-fontawesome')({ prefix: 'fa-' })
const TIPOS_PROYECTO = {
    hoja_de_vida: "Hoja de Vida",
    comercial: "Comercial",
    primera_reunion: "Primera Reunión",
    visita_territorio: "Visita Territorio",
    gestion_entidad_privada: "Gestión entidad Privada",
    gestion_educativa: "Gestión educativa",
    gestion_entidad_publica: "Gestión entidad Pública",
    cita_interpersonal: "Cita Interpersonal",
    consecucion_recurso: "Consecusión recurso",
    programar_llamada: "Programar llamada",
    programar_reunion: "Programar reunión",
    realizar_pago: "Realizar pago",
    reunion_lideres: "Reunión líderes",
    reunion: "Reunión"
};

module.exports = (handlebars) => {
    let helpers = {
        lower: (str) => {
            return str.toLowerCase();
        },
        getModelPath: (str) => {
            return global.getModelPath(str);
        },
        getMes: (str) =>{
            let mes = {
                '1' : 'Enero', '2' :'Febrero', '3': 'Marzo', '4': 'Abril', '5': 'Mayo', '6': 'Junio', '7': 'Julio', '8':'Agosto',
                '9' :'Septiembre', '10' : 'Octubre', '11':'Noviembre', '12' : 'Diciembre'
            }
            return mes[str];
        },
        raw: (options) => {
            return options.fn(this);
        },
        percent: (val, operator, total) => {
            let porcentaje = (val * 100) / total; 
            return _.round(porcentaje,2);
        },
        count: (data) => {
            return _.size(data);
        },
        sum: (data, key) => {
            let suma = 0;
            for (var i in data) {
                suma += (key in data[i] && !isNaN(parseInt(data[i][key]))) ? parseInt(data[i][key]) : 0;
            }
            return suma;
        },
        countCond: (data, key, operator, val) => {
            console.log(data+' : '+key+' : '+operator+' : '+val);
            let count = 0;
            _.each(data, (el) => {
                switch (operator) {
                    case '==':
                        if (el[key] == val) { count++ }
                        break;
                    case '===':
                        if (el[key] === val) { count++ }
                        break;
                    case '!=':
                        if (el[key] != val) { count++ }
                        break;
                    case '!==':
                        if (el[key] !== val) { count++ }
                        break;
                    case '<':
                        if (el[key] < val) { count++ }
                        break;
                    case '<=':
                        if (el[key] <= val) { count++ }
                        break;
                    case '>':
                        if (el[key] > val) { count++ }
                        break;
                    case '>=':
                        if (el[key] >= val) { count++ }
                        break;
                    case '&&':
                        if (el[key] && val) { count++ }
                        break;
                    case '||':
                        if (el[key] || val) { count++ }
                        break;
                }
            });

            return count;
        },
        sumCond: (data, key, operator, val, sum) => {
            let count = 0;
            _.each(data, (el) => {
                switch (operator) {
                    case '==':
                        if (el[key] == val) { count+= el[sum] }
                        break;
                    case '===':
                        if (el[key] === val) { count+= el[sum] }
                        break;
                    case '!=':
                        if (el[key] != val) { count+= el[sum] }
                        break;
                    case '!==':
                        if (el[key] !== val) { count+= el[sum] }
                        break;
                    case '<':
                        if (el[key] < val) { count+= el[sum] }
                        break;
                    case '<=':
                        if (el[key] <= val) { count+= el[sum] }
                        break;
                    case '>':
                        if (el[key] > val) { count+= el[sum] }
                        break;
                    case '>=':
                        if (el[key] >= val) { count+= el[sum] }
                        break;
                    case '&&':
                        if (el[key] && val) { count+= el[sum] }
                        break;
                    case '||':
                        if (el[key] || val) { count+= el[sum] }
                        break;
                }
            });

            return count;
        },
        tipoProyecto: (tipo) => {
            return (tipo in TIPOS_PROYECTO) ? TIPOS_PROYECTO[tipo] : '';
        },
        estadoTarea: (tarea) => {
            return (moment(tarea.fin).isBefore(new Date())) ? 'expired' : 'active';
        },
        edad: (fecha) =>{
            var nacimiento = moment(fecha);
            var hoy = moment();
            var anios = hoy.diff(nacimiento,"years");

            var cumple = nacimiento.month();
            var actual = hoy.month();

            //Valida si cumple años en el mes, para sumar un año
            if(cumple == actual){
                cumple = nacimiento.day();
                actual = hoy.day();

                if(cumple >= actual){
                    anios += 1;
                }
            }

            return anios;
        },
        money: (amount) => {
            var formatMoney = (n, c, d, t) => {
                var c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "." : d,
                    t = t == undefined ? "," : t,
                    s = n < 0 ? "-" : "",
                    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                    j = (j = i.length) > 3 ? j % 3 : 0;
                return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
            };

            return formatMoney(amount, 0);
        },
        countPercent: (data, key, operator, val) => {
            let count = 0;

            _.each(data, (el) => {
                switch (operator) {
                    case '==':
                        if (el[key] == val) { count++ }
                        break;
                    case '===':
                        if (el[key] === val) { count++ }
                        break;
                    case '!=':
                        if (el[key] != val) { count++ }
                        break;
                    case '!==':
                        if (el[key] !== val) { count++ }
                        break;
                    case '<':
                        if (el[key] < val) { count++ }
                        break;
                    case '<=':
                        if (el[key] <= val) { count++ }
                        break;
                    case '>':
                        if (el[key] > val) { count++ }
                        break;
                    case '>=':
                        if (el[key] >= val) { count++ }
                        break;
                    case '&&':
                        if (el[key] && val) { count++ }
                        break;
                    case '||':
                        if (el[key] || val) { count++ }
                        break;
                }
            });

            return (count * 100) / (_.size(data));
        },
        getIcon: (mime) => {
            return m2f(mime);
        },
        join: (data) => {
            //if(_.isPlainObject(data)){
                if (_.isPlainObject(data[0]) && '_id' in data[0]) {
                    let ids = [];
                    _.each(data, (e) => { ids.push(e._id); });
                    data = ids;
                }
                
                return data.join('|');    
            //}             
            //return data;   
        },
        etiqueta: (e) => {
            e = e.split(':');
            return e[0].charAt(0).toUpperCase() + e[0].slice(1).toLowerCase() + ': ' + e[1];
        },
        date: (e) => { return (e) ? moment(e).format('YYYY-MM-DD') : ''; },
        dateTime: (e) => { return (e) ? moment(e).format('YYYY-MM-DD hh:mm A') : ''; },
        dateHour: (e) => { return (e) ? moment(e).format('hh:mm A') : ''; },
        dateInput: (e) => { return (e && moment(e).format('MM/DD/YYYY') != 'Invalid date') ? moment(e).format('MM/DD/YYYY') : ''; },
        toJSON: (obj) => {
            return JSON.stringify(obj);
        },
        get: (obj, key, match, get) => {
            for (var i in obj) {
                if (obj[i][key] == match) {
                    return obj[i][get];
                }
            }
            return '';
        },
        ifCond: (v1, operator, v2, options) => {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '!=':
                    return (v1 != v2) ? options.fn(this) : options.inverse(this);
                case '!==':
                    return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        },
        numberFormat: (e, options)=>{
            // Helper parameters
            var dl = options.hash['decimalLength'] || 0;
            var ts = options.hash['thousandsSep'] || '.';
            var ds = options.hash['decimalSep'] || ',';

            // Parse to float
            var value = parseFloat(e);

            // The regex
            var re = '\\d(?=(\\d{3})+' + (dl > 0 ? '\\D' : '$') + ')';

            // Formats the number with the decimals
            var num = value.toFixed(Math.max(0, ~~dl));

            // Returns the formatted number
            return (ds ? num.replace('.', ds) : num).replace(new RegExp(re, 'g'), '$&' + ts);
        }
    };

    for (var name in helpers) {
        handlebars.registerHelper(name, helpers[name]);
    }
};