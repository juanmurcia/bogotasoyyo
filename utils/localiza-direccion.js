"use strict";
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const _           = require('lodash');
const request     = require('request');
const cheerio     = require('cheerio');
const diacritics  = require('diacritics').remove;
const iconv       = require('iconv');
const uuid        = require('uuid/v4');
const decode      = new iconv.Iconv('iso-8859-1', 'utf-8');
const FileCookieStore = require('tough-cookie-filestore');

const ENDPOINT = "https://lupap.com/address/bogota/{direccion}";

module.exports = (direccion, callback) => {
  let dir_final = direccion.replace('Nº','');
  dir_final = dir_final.replace('No.','');
  dir_final = dir_final.replace('#','');
  dir_final = dir_final.replace('-',' ');
  dir_final = dir_final.replace('  ',' ');

  let arr_dir = dir_final.split(' ');
  _.each(arr_dir, (puesto) => {
    dir_final = dir_final.replace(' ','+');
  });

  let url_dir = ENDPOINT.replace('{direccion}', dir_final);
  console.log('url_dir');
  console.log(url_dir);
  
  request({url: url_dir}, (error, response, body) => {

    let infoPuesto = {
      departamento: '',
      municipio: '',
      direccion: '',
      mesa: '',
      nombre: ''
    };

    if(error) {
      console.log(error);
      callback(false);
      return false;
    }

    console.log('=============> body <========');
    console.log(body);
    var textChunk = body.toString('utf8');
    console.log('=============> body (2) <========');
    console.log(textChunk);

    callback(infoPuesto);
  });
};
