module.exports = function () {
  "use strict";

  return {
    html: '<%= config.html %>',
    css: '<%= config.destination.css %>',
  };
};
