$(window).on('siteReady', function() {
	let result =  $('input[name="results"]').val();
	result = JSON.parse(result);

    var localidad = L.layerGroup();
    var indicador = L.layerGroup();

    $.each(result, function( index, value ) {
    	var mensaje = "<div><h4><b>"+value.nombre+"</b></h4><hr><br><ul><li>Pot. Declarado : "+value.potencial+"</li><li>Pot. Real : "+value.personajes+"</li></ul></div>";

    	if(value.geo != '0'){
			var geo = value.geo;
    		geo = geo.split(',');
    		let color = 'green';

    		if(value.avance < 20){
    			color = 'red';
    		}else if(value.avance < 40){
    			color = 'orange';
    		}else if(value.avance < 70){
    			color = 'yellow';
    		}

    		L.marker([geo[0], geo[1]]).bindPopup(mensaje).addTo(localidad);
    		L.circle([geo[0], geo[1]], {
			    color: color,
			    fillColor: color,
			    fillOpacity: 0.5,
			    radius: 1000
			}).addTo(indicador);	
    	}		
	});

	var mbAttr = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ';
		mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

	var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
		streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});

	var map = L.map('mapId', {
		center: [4.6306374, -74.1231804],
		zoom: 11.75,
		minZoom: 0,
		maxZoom: 18,
		zoomSnap: 0,
		zoomDelta: 0.25,
		layers: [grayscale, localidad, indicador]
	});

	var baseLayers = {
		"Grayscale": grayscale,
		"Streets": streets
	};

	var overlays = {
		"Localidades": localidad,
		"Indicador": indicador
	};

	L.control.layers(baseLayers, overlays).addTo(map);
	map.on('click', onMapClick);	
});

function onMapClick(e) {
	toastr.info("You clicked the map at " + e.latlng);
}