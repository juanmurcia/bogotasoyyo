$(window).on('siteReady', function() {

    let obj = {
        id:'tbRepresentantes',
        type:'GET',
        url:'/admin/representantes/list',
        columns: [
            {data: 'foto_perfil', name: 'foto_perfil'},            
            {data: 'nombre', name: 'nombre'},            
            {data: 'referido', name: 'referido'},            
            {data: 'localidad', name: 'localidad'},            
            {data: 'potencial_declarado_urna', name: 'potencial'},            
            {data: 'asesor', name: 'asesor'},            
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    };
    tableSearch(obj);

    setTimeout(function(){
        $('.info').click(function() {
            var personaje = $(this).data('toolbar');
            $.getJSON("/api/tree/personajes/"+personaje+"/detail", function(data) {
                data.result.forEach(function(persona, index) {
                    toastr.info("<ul><li><b>Tel Fijo:</b>"+persona.telefono_fijo.indicativo+"-"+persona.telefono_fijo.numero+"</li><li><b>Tel Celular:</b>"+persona.telefono_movil+"</li><li><b>Dirección:</b>"+persona.direccion_residencia+"</li><li><b>Email:</b>"+persona.email_personal+"</li></ul>",'Informacion Personaje '+ persona.nombre);
                });
            });
        })

        $('.detalle').click(function() {
            var personaje = $(this).data('toolbar');
            window.location.href = "/admin/personajes/"+personaje+"";
        })

        $('.editar').click(function() {
            var personaje = $(this).data('toolbar');
            window.location.href = "/admin/personajes/"+personaje+"/edit";
        })


        $('.addAjax').click(function() {

            $(this).select2({
              placeholder: {
                id: '-1',
                text: 'Buscar...'
              },
              ajax: {
                url: "/api/search/" + $(this).data('model'),
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  return {
                    q: params.term,
                    page: params.page
                  };
                },
                cache: false
              },
              escapeMarkup: function (markup) { return markup; },
              minimumInputLength: 1
            });
        });

        $('.dropAjax').dblclick(function() {
            let personaje_id = $(this).data('personaje');
            let asesor_id = 0;
            console.log('Asesor Delete ....');
            $.ajax({
                type:'POST',
                url: '/api/asesor/'+personaje_id,
                data: {asesor:asesor_id},
                cache: false,
                success: function(result){                
                    toastr.success('Asesor Eliminado');
                    location.reload();
                }
            });
        });

        $('[name=asesor]').change(function() {
            /* Act on the event */
            let personaje_id = $(this).data('personaje');
            let asesor_id = $(this).val();
            console.log('Asesor Change ....');
            $.ajax({
                type:'POST',
                url: '/api/asesor/'+personaje_id,
                data: {asesor:asesor_id},
                cache: false,
                success: function(result){                
                    toastr.success('Asesor Asignado');
                    location.reload();
                }
            });
        });

        $('[name=ver-contacto]').click(function(){
            window.open('/admin/contactos/', '_blank');
        });

    },2000);    
});

function verImagen(obj){
    var imagen = $(obj).data('src');
    console.log(imagen);
    $("#imagenZoom").attr('src', imagen);
    $('#imagenPersonaje').modal('show');
};