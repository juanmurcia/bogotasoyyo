$(window).on('siteReady', function() {
    var personaje = null;

    $.ajax({
        type: 'GET',
        url: '/api/personajes/countdel',
        success: function(result){
            $('.eliminados').html(result);                  
        }
    });

    $('.addAjax').click(function() {

        $(this).select2({
          placeholder: {
            id: '-1',
            text: 'Buscar...'
          },
          ajax: {
            url: "/api/search/" + $(this).data('model'),
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term,
                page: params.page
              };
            },
            cache: false
          },
          escapeMarkup: function (markup) { return markup; },
          minimumInputLength: 1
        });
    });

    $('.dropAjax').dblclick(function() {
        let personaje_id = $(this).data('personaje');
        let asesor_id = 0;
        console.log('Asesor Delete ....');
        $.ajax({
            type:'POST',
            url: '/api/asesor/'+personaje_id,
            data: {asesor:asesor_id},
            cache: false,
            success: function(result){                
                toastr.success('Asesor Eliminado');
                location.reload();
            }
        });
    });

    $('[name=exportar-csv]').click(function(){
        let data = {};
        $( "[name=csv]" ).prop('checked', true);

        $("#frmBusqPersonajes").find(':input').each(function() {
            var element= this
            if(element.type == 'checkbox'){
                if(element.name != ''){
                    data[element.name] = element.checked;
                }
            }else{
                if(element.name != '' && element.value != ''){
                    data[element.name] = element.value;
                }  
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/personajes/list',
            data: data,
            success: function(result){
                var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(result);
                var downloadLink = document.createElement("a");
                downloadLink.href = uri;
                downloadLink.download = document.title ? document.title.replace(/ /g, "_") + ".csv" : "data.csv";
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);            
                toastr.success('Excel Generado');
            }
        });
        $( "[name=csv]" ).prop('checked', false);
    });    

    $('.personajeDet').click(function(){
        window.open('/admin/personajeDel', '_blank');
    });

    $('.organizacionDet').dblclick(function(){
        if($(this).attr('id')){
            window.open('/admin/organizaciones/'+$(this).attr('id'), '_blank');
        }else{
            toastr.error('Personaje sin Organización');
        }
        
    });

    let obj = {
        id:'tbPersonajes',
        type:'GET',
        url:'/admin/personajes/list',
        form:'frmBusqPersonajes',
        columns: [
            {data: 'nombre', name: 'nombre'},            
            {data: 'descripcion', name: 'descripcion'},
            {data: 'referido', name: 'referido'},
            {data: 'localidad', name: 'localidad'},                       
            {data: 'fecha_nacimiento', name: 'fecha_nacimiento'},
            {data: 'items', name: 'items', align: 'center', orderable: false, searchable: true},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    };
    tableSearch(obj);
});

function verImagen(imagen){
    $("#imagenZoom").attr('src', imagen);
    $('#imagenPersonaje').modal('show');
};

function fnDetalle(obj){
    var personaje = $(obj).data('toolbar');
    window.location.href = "/admin/personajes/"+personaje;
}

function fnEditar(obj){    
    var personaje = $(obj).data('toolbar');
    window.location.href = "/admin/personajes/"+personaje+"/edit";
}

function fnInfo(obj){    
    var id = $(obj).data('toolbar');
    $.getJSON("/api/tree/personajes/"+id+"/detail", function(data) {
        data.result.forEach(function(persona, index) {
            toastr.info("<ul><li><b>Tel Fijo:</b>"+persona.telefono_fijo.indicativo+"-"+persona.telefono_fijo.numero+"</li><li><b>Tel Celular:</b>"+persona.telefono_movil+"</li><li><b>Dirección:</b>"+persona.direccion_residencia+"</li><li><b>Email:</b>"+persona.email_personal+"</li></ul>",'Informacion Personaje '+ persona.nombre);
        });
    });
}
