$(document).ready(function() {
	//variables
	var pass1 = $('[name=passwd]');
	var pass2 = $('[name=passwd2]');
	var container = $('[name=passAlert]');
	var confirmacion = "&nbsp;&nbsp;<i class='icon wb-check-circle font-size-15'>Las contraseñas coinciden&nbsp;&nbsp;";
	var longitud = "&nbsp;&nbsp;<i class='icon wb-alert-circle font-size-15'> La contraseña debe ser mayor a 6 caracteres&nbsp;&nbsp;";
	var negacion = "&nbsp;&nbsp;<i class='icon wb-alert-circle font-size-15'>No coinciden las contraseñas&nbsp;&nbsp;";
	var vacio = "&nbsp;&nbsp;<i class='icon wb-info-circle font-size-15'>La contraseña no puede estar vacía&nbsp;&nbsp;";
	//oculto por defecto el elemento span
	var span = $('<span></span>').appendTo(container);
	span.hide();
	//función que comprueba las dos contraseñas
	function coincidePassword(){
		var valor1 = pass1.val();
		var valor2 = pass2.val();
		//muestro el span
		span.empty();
		span.show().removeClass();
		$('#registrar').attr("disabled", true);
		//condiciones dentro de la función
		if(valor1 != valor2){
			span.append(negacion).addClass('counter-label red-600');
		}
		else if(valor1.length==0 || valor1==""){
			span.append(vacio).addClass('counter-label orange-600');
		}
		else if(valor1.length<6 ){
			span.append(longitud).addClass('counter-label red-600');
		}
		else if(valor1.length!=0 && valor1==valor2){
			span.append(confirmacion).removeClass("counter-label red-600").addClass('counter-label green-600');
			$('#registrar').attr("disabled", false);
		}

	}
	//ejecuto la función al soltar la tecla
	pass1.keyup(function(){
		coincidePassword();
	});
	pass2.keyup(function(){
		coincidePassword();
	});

	$('[name=registrar]').hover(function(){
		$uploadCrop.result({
            type: 'canvas',
            size: 'original'
        }).then(function (resp) {
        	console.log('Cargando img ...');
            if(resp) {
                $('#imagebase64').val(resp);
            }
        });
	});

	$('[name=documento]').blur(function(){
		var documento = $('[name=documento]').val();
		if(documento.length > 6){
			$.getJSON('/api/personaje/'+documento, function(data) {
				if(data.error){
					toastr["error"](data.error,'Error Validacion');
					$('[name=documento]').select();
				}else{
					toastr["success"]('Documento encontrado','Listo');
					let nombre = ucWords(data.nombre);
					$('[name=personaje]').val(data._id);
					$('[name=name]').val(nombre.replace(' ','')).prop('disabled',false);
					$('[name=email]').val(data.email_personal).prop('disabled',false);
					$('[name=role]').prop('disabled',false);
					$('[name=passwd]').prop('disabled',false);
					$('[name=passwd2]').prop('disabled',false);
				}				
			})
		}else{
			toastr["info"]('Debe diligenciar el documento');
			$('[name=documento]').select();
		}

	});

	//Importar imagen
	var $uploadCrop = null;
	function readFile(input) {
			if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        
	        reader.onload = function (e) {
				$('#upload-editor, #upload-controls').slideDown();
	        	$uploadCrop.bind({
	        		url: e.target.result
	        	}).then(function(){
	        		console.log('jQuery bind complete');
	        	});
	        	
	        }
	        
	        reader.readAsDataURL(input.files[0]);
	        $('#ext').val(input.files[0].name.split('.').pop().toLowerCase());
	    }
	    else {
	        alert("Navegador no soporta la edición de imagenes.");
	    }
	}

	$('input[name="pic"]').on('change', function () { readFile(this); });
	$uploadCrop = new Croppie(document.querySelector('#upload-editor'), {
	    enableExif: true,
	    enableOrientation: true,
	    viewport: {
	        width: 300,
	        height: 300,
	        type: 'square'
	    },
	    boundary: {
	        width: 300,
	        height: 300
	    }
	});

	$('.rotar').on('click', function(ev) {
		$uploadCrop.rotate(parseInt($(this).data('deg')));
	});
});