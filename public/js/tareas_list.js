$(document).ready(function() {
  $('.close').click(function(){
    $('#filtro').hide( 'slide', {direction: 'left'}, 1000 );
    $('.show').show();
    $('.calendario').show();
  });

  $('.show').click(function(){
    $('.calendario').hide( 'slide', {direction: 'right'}, 1000 );
    $('#filtro').show();
    $('.show').hide();
  });

});