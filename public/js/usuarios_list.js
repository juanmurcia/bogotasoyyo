$(window).on('siteReady', function() {
    let obj = {
        id:'tbUsuarios',
        type:'GET',
        url:'/admin/usuarios/list',
        columns: [
            {data: 'name', name: 'nombre'},
            {data: 'email', name: 'email'},
            {data: 'role', name: 'perfil'},
            {data: 'createdAt', name: 'fec_cre'},
            {data: 'estado', name: 'estado'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: false}
        ]
    };
    tableSearch(obj);

    setTimeout(function(){
        $('.tokenUser').each(function() {       
            $(this).click(function(event) {
                let id = $(this).data('user');
                $('#userPass').val(id);
            });
        });

        if($('#userRole').val() != 'admin'){
            $('.page-content').remove();
            $(location).attr('href', '/')
        }
    },2000);
	

});