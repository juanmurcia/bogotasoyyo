$(document).ready(function() {
    $(window).one('siteReady', function() {

        //Carga los select con el plugin      
        $('.selectAjax').each(function() {
            $(this).select2({
                placeholder: {
                    id: '-1',
                    text: 'Buscar...'
                },
                ajax: {
                    url: "/api/search/" + $(this).data('model'),
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    cache: false
                },
                escapeMarkup: function(markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 3
            });
        });

        $('.selectAjax').parent().find('label').append(' <span class="fa fa-times-circle clear-select"></span>');

        $('.clear-select').click(function() {
            $(this).prev('select').val('').trigger('change');
        });
        
    });
});
