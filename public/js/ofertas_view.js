$(window).on('siteReady', function() {
    
  $('.agregar-elemento').click(function() {
    $(this).toggleClass('hidden-xs-up');
    $(this).prev().toggleClass('hidden-xs-up');
  });
  
  $('.cancelar-elemento').click(function() {
    $(this).parents('form').toggleClass('hidden-xs-up');
    $(this).parents('form').next().toggleClass('hidden-xs-up');
  });
  
  $('.datepicker').each(function() {
    $(this).datepicker();
  });
  
  $('.VerHojaVida').each(function() {       
    $(this).click(function(){
      var id = $(this).data('personaje');        
      searchHojaVida(id);
    });
  });

});

function agregaHojaVida(campo){
  var data = campo.split('|');
  var path = window.location.pathname;
  path = path.replace('admin', 'api');

  $.post(path + '/addhoja', {set: data[0]});
  searchHojaVida(data[1]);
}

function eliminaHojaVida(campo){

  var path = window.location.pathname;
  path = path.replace('admin', 'api');

  $.post(path + '/drophoja', {set: campo});
  location.reload();
}

function searchHojaVida(id){
  $.getJSON('/api/search/hojasdevida?personaje=' + id,function(data) {
    var html = '<table class="table table-hover mt-30"><th>Personaje</th><th>Profesion</th><th>Prioridad</th><th>Action</th>';

    $.each(data, function( index, value ) {
      html += '<tr><td>'+value.personaje.nombre+'</td><td>'+value.profesion+'</td><td>'
                +'<div class="example">'
                  +'<div class="raty" data-score="'+value.prioridad+'" data-number="5"></div>'
                +'</div></td>'
                  +'<td><a href="javascript:void(0)" class="btn btn-info btn-icon btn-outline btn-round" id="'+value._id+'|'+id+'" onclick="agregaHojaVida(this.id);" >'
                          +'<i class="icon wb-plus" aria-hidden="true"></i>'
                  +'</a></td>';
    });
    html +='</table>';

    $('#contenHV').html(html);
    $('#modalHojasDeVida').modal('show');

    $('.raty').each(function() {
      $(this).raty({path : '/', score : $(this).data('score'), readOnly : true });
    });
  });
} 







