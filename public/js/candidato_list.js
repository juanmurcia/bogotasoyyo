$(window).on('siteReady', function() {
    $('[name=consultaPer]').click(function(){
        consulta(); 
    });
    
    $('[name=frmBusq]').click(function(event){
        $("[name=busqueda]").fadeToggle("slow");
        setTimeout(function(){
            $("[name=busqueda]").fadeOut("slow")
        }, 30000);
    });   

    setTimeout(consulta(), 2000);
});

function color(data){
    $('[name=color]').val(data);
}

function consulta(){    
    let obj = {
        id:'tbCandidatoP',
        type:'GET',
        destroy:true,
        url:'/candidato/ctoPersonajes/list',
        form:'frmBusqPersonajes',
        columns: [
            {data: 'foto', name: 'foto', orderable: false, searchable: true},            
            {data: 'nombre', name: 'nombre'},            
            {data: 'grupo', name: 'grupo'},
            {data: 'referido', name: 'referido'},
            {data: 'localidad', name: 'localidad'},
            {data: 'descripcion', name: 'descripcion'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    };

    $("[name=busqueda]").fadeToggle("slow");
    tableSearch(obj);
    setTimeout(function(){
        $('[name=color]').val('');

        $('.addAjax').each(function(){
            $(this).click(function() {
                $(this).select2({
                  placeholder: {
                    id: '-1',
                    text: 'Buscar...'
                  },
                  ajax: {
                    url: "/api/search/" + $(this).data('model'),
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                      return {
                        q: params.term,
                        page: params.page
                      };
                    },
                    cache: false
                  },
                  escapeMarkup: function (markup) { return markup; },
                  minimumInputLength: 1
                });
            });
        });


        $('.dropAjax').dblclick(function() {
            let personaje_id = $(this).data('personaje');

            $.ajax({
                type:'POST',
                url: '/api/grupo/'+personaje_id,
                data: {grupo: 0},
                cache: false,
                success: function(result){                
                    toastr.success('Grupo Eliminado');
                    consulta();
                }
            });
        });

        $('[name=grupo]').change(function() {
            /* Act on the event */
            console.log('Grupo Change ....');
            $.ajax({
                type:'POST',
                url: '/api/grupo/'+$(this).data('personaje'),
                data: {grupo:$(this).val()},
                cache: false,
                success: function(result){                
                    toastr.success('Grupo Asignado');
                    consulta();
                }
            });
        });
    }, 4000);    
}

function verImagen(imagen){
    $("#imagenZoom").attr('src', imagen);
    $('#imagenPersonaje').modal('show');
};