$(document).ready(function() {
	let obj = {
        id:'tbLocalidades',
        type:'GET',
        url:'/admin/localidades/list',
        columns: [
            {data: 'nombre', name: 'nombre'},            
            {data: 'potencial', name: 'pot_declarado'},
            {data: 'personajes', name: 'pot_real'},
            {data: 'avance', name: 'avance', orderable: false, searchable: true}
        ]
    };
    tableSearch(obj);

    /*
	let result =  $('input[name="results"]').val();
	result = JSON.parse(result);

	let potencial = 0;
	let personaje = 0;
	let avance = 0;

	$.each(result, function(i, item) {
		potencial += item.potencial;
		personaje += item.personajes;
	});

	avance = (personaje * 100) / potencial;
	$('#declarado').html(potencial);
	$('#real').html(personaje);
	*/
});