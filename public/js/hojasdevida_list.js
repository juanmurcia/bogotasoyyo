$(window).on('siteReady', function() {

    let obj = {
        id:'tbHojasVida',
        type:'GET',
        url:'/admin/hojasdevida/list',
        columns: [
            {data: 'personaje.nombre', name: 'nombre'},            
            {data: 'referido.nombre', name: 'referido'},
            {data: 'profesion', name: 'profesion'},
            {data: 'fecha_entregada', name: 'fec_entrega'},                       
            {data: 'ubicacion', name: 'ubicacion'},
            {data: 'prioridad', name: 'prioridad', orderable: false, searchable: true},
            {data: 'estado', name: 'estado'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    };
    tableSearch(obj);

    setTimeout(function(){
    	$('.detalle').click(function() {
            var personaje = $(this).data('toolbar');
            window.location.href = "/admin/hojasdevida/"+personaje;
        })
    
        $('.editar').click(function() {
            var personaje = $(this).data('toolbar');
            window.location.href = "/admin/hojasdevida/"+personaje+"/edit";
        })

        $('.eliminar').click(function() {
            var personaje = $(this).data('toolbar');
            window.location.href = "/admin/hojasdevida/"+personaje+"/delete";
        })

        $('.rating').each(function() {
            $(this).raty({path : '/', score : $(this).data('score'), readOnly : true });
        })
    },1000);
    
});