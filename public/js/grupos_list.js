$(window).on('siteReady', function() {

    let obj = {
        id:'tbGrupos',
        type:'GET',
        url:'/admin/grupos/list',
        columns: [
            {data: 'nombre', name: 'nombre'},            
            {data: 'creador.name', name: 'creador'},
            {data: 'createdAt', name: 'fec_cre'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    };
    tableSearch(obj);

    setTimeout(function(){
        $('.editar').click(function() {
            var id = $(this).data('toolbar');
            window.location.href = "/admin/grupos/"+id+"/edit";
        })

        $('.eliminar').click(function() {
            var id = $(this).data('toolbar');
            window.location.href = "/admin/grupos/"+id+"/delete";
        })
    },1000);
    
});