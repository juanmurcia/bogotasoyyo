$(window).on('siteReady', function() {

    let obj = {
        id:'tbEntidades',
        type:'GET',
        url:'/admin/entidades/list',
        columns: [
            {data: 'nombre', name: 'nombre'},            
            {data: 'direccion', name: 'direccion'},
            {data: 'createdAt', name: 'fec_cre'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    };
    tableSearch(obj);

    setTimeout(function(){
        $('.editar').click(function() {
            var personaje = $(this).data('toolbar');
            window.location.href = "/admin/entidades/"+personaje+"/edit";
        })

        $('.eliminar').click(function() {
            var personaje = $(this).data('toolbar');
            window.location.href = "/admin/entidades/"+personaje+"/delete";
        })
    },1000);
    
});