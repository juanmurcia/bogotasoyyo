$(window).on('siteReady', function() {
    
  $('.agregar-elemento').click(function() {
    $(this).toggleClass('hidden-xs-up');
    $(this).prev().toggleClass('hidden-xs-up');
  });
  
  $('.cancelar-elemento').click(function() {
    $(this).parents('form').toggleClass('hidden-xs-up');
    $(this).parents('form').next().toggleClass('hidden-xs-up');
  });
  
  $('.datepicker').each(function() {
    $(this).datepicker();
  });  

  $('.check-ajax').on('change', function() {
    var checked = false;
    if($(this).is(':checked')) {
      checked = true;
    }
    
    $.post('/api/task/' + $(this).data('id'), {checked: checked}, function(response) {})
  });
});