$(window).on('siteReady', function() {
  let obj = {
    id:'tbContactos',
    type:'GET',
    url:'/lider/contactos/list',
    columns: [
        {data: 'documento', name: 'documento'},            
        {data: 'nombre', name: 'nombre'},
        {data: 'tel_fijo', name: 'tel_fijo'},
        {data: 'telefono_movil', name: 'telefono_movil'},
        {data: 'email', name: 'email'},
        {data: 'createdAt', name: 'fec_cre'},
        {data: 'estado_contacto', name: 'estado'},
        {data: 'creador.name', name: 'creador'},
        {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
    ]
  };

  tableSearch(obj);
});