$(window).on('siteReady', function() {

    let obj = {
        id:'tbOfertas',
        type:'GET',
        url:'/admin/ofertas/list',
        columns: [
            {data: 'personaje.nombre', name: 'personaje'},            
            {data: 'entidad.nombre', name: 'entidad'},
            {data: 'titulo', name: 'titulo'},
            {data: 'fec_inicio', name: 'fec_inicio'},
            {data: 'fec_fin', name: 'fec_fin'},
            {data: 'vacantes', name: 'vacantes'},
            {data: 'ofertar', name: 'ofertar'},
            {data: 'estado', name: 'estado'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    };
    tableSearch(obj);

    setTimeout(function(){
        $('.detalle').click(function() {
            var personaje = $(this).data('toolbar');
            console.log(personaje);
            
            window.location.href = "/admin/ofertas/"+personaje;
        });

        $('.editar').click(function() {
            var personaje = $(this).data('toolbar');
            console.log(personaje);
            
            window.location.href = "/admin/ofertas/"+personaje+"/edit";
        });

        $('.eliminar').click(function() {
            var personaje = $(this).data('toolbar');
            console.log(personaje);
            
            window.location.href = "/admin/ofertas/"+personaje+"/delete";
        });
    },2000);
});