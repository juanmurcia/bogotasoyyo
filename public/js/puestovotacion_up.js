$(document).ready(function() {
    $(window).one('siteReady', function() {
        $('[name=registrar]').click(function() {
            console.log('Click Registrar');
            var data = jQuery.parseJSON($('[name=datos]').val());
            var type = $('[name=tipo]').val();

            $.each(data, function( index, value ) {
                value.municipio = 'BOGOTA. D.C.';
                value.departamento = 'BOGOTA. D.C.';
                value.nombre = value.nombre.toUpperCase();

                if(type == 1){
                    $.post('/cargues/puestosdevotacion/new', {data: value}, function(response) {
                        let mensaje = response.split('|');
                        toastr[mensaje[0]](mensaje[1]);
                        if(mensaje[0] == 'error'){
                            console.log(value);
                        }
                    });
                }else{
                    $.post('/cargues/puestosdevotacion/edit', {data: value}, function(response) {
                        let mensaje = response.split('|');
                        toastr[mensaje[0]](mensaje[1]);
                        if(mensaje[0] == 'error'){
                            console.log(value);
                        }
                    });
                }
                
            });
        })


    });
});