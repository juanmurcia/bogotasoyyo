$(document).ready(function() {
    $(window).one('siteReady', function() {
        let obj = {
            id:'tbPuesto',
            type:'GET',
            colTotal:[['4','t'],['5','t'],['6','t'],['7','t'],['8','t'],['9','t']],
            url:'/admin/puestosdevotacion/list',
            columns: [
                {data: 'nombre', name: 'nombre'},            
                {data: 'localidad.nombre', name: 'localidad'},
                {data: 'barrio', name: 'barrio'},
                {data: 'direccion', name: 'direccion'},
                {data: 'censo_mujer', name: 'censo_mujer',className: "text-right"},
                {data: 'censo_hombre', name: 'censo_hombre',className: "text-right"},
                {data: 'potencial_electoral', name: 'potencial_electoral',className: "text-right"},
                {data: 'votos', name: 'votos',className: "text-right"},
                {data: 'registrados', name: 'registrados',className: "text-right"},                
                {data: 'partido', name: 'partido',className: "text-right"},
                {data: 'potencial', name: 'potencial',className: "text-right"},
                {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
            ]
        };
        tableSearch(obj);

        var general = $("[name=general]").val();
        var partido = $("[name=partido]").val();
        var localidad = $("[name=localidad]").val();
        var serie = $("[name=serie]").val();

        $( "[name=general]" ).remove();
        $( "[name=partido]" ).remove();
        $( "[name=localidad]" ).remove();
        $( "[name=serie]" ).remove();

        partido = JSON.parse(partido);        
        general = JSON.parse(general);
        localidad = JSON.parse(localidad);
        serie = JSON.parse(serie);
        
        Morris.Bar({
          element: 'barLocalidadPartido',
          data: partido,
          xkey: 'label',
          ykeys: ['value'],
          labels: ['Votos Partido Cambio Radical'],
          barGap: 6,
          barSizeRatio: 0.35,
          smooth: true,
          gridTextColor: '#474e54',
          gridLineColor: '#eef0f2',
          goalLineColors: '#e3e6ea',
          gridTextFamily: Config.get('fontFamily'),
          gridTextWeight: '300',
          numLines: 6,
          gridtextSize: 14,
          resize: true,
          barColors: [Config.colors("primary", 500)]
        });

        Morris.Donut({
          element: 'donutGeneral',
          data: general,
          // barSizeRatio: 0.35,
          resize: true,
          colors: [Config.colors("red", 500), Config.colors("primary", 500), Config.colors("blue-grey", 300)]
        });
        Morris.Donut({
          element: 'donutLocalidadPartido',
          data: partido,
          // barSizeRatio: 0.35,
          resize: true,
          colors: [Config.colors("red", 500), Config.colors("primary", 500), Config.colors("blue-grey", 300)]
        });
        
        Highcharts.theme = {
          colors: ['#7cb5ec', '#f7a35c', '#90ee7e', '#7798BF', '#aaeeee', '#ff0066','#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
          chart: {backgroundColor: null,height: 750,style: {fontFamily: 'Dosis, sans-serif'}},
          title: {style: {fontSize: '16px',fontWeight: 'bold',textTransform: 'uppercase'}},
          subtitle: {style: {color: 'black'}},
          tooltip: {borderWidth: 0,backgroundColor: 'rgba(219,219,216,0.8)',shadow: false},
          legend: {itemStyle: {fontWeight: 'bold',fontSize: '13px'}},
          xAxis: {gridLineWidth: 1,labels: {style: {fontSize: '12px'}}},
          yAxis: {minorTickInterval: 'auto',title: {style: {textTransform: 'uppercase'}},labels: {style: {fontSize: '12px'}}},
          plotOptions: {candlestick: {lineColor: '#404048'}},
          // General
          background2: '#F0F0EA'
        };
        // Apply the theme
        Highcharts.setOptions(Highcharts.theme);
        //tercer gráfico
        Highcharts.chart('barLocalidadPartidos', {
          chart: {
              type: 'bar'
          },
          title: {
              text: 'Votaciones x Localidad x Partido'
          },
          xAxis: {
              categories: localidad
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'Total Votaciones'
              }
          },
          legend: {
              reversed: false
          },
          plotOptions: {
              series: {
                  stacking: 'normal'
              }
          },
          series: serie
        });

        $('[name=exportar-csv]').click(function(){
            let data = {};
            $( "[name=csv]" ).prop('checked', true);

            $("#frmBusqPuestos").find(':input').each(function() {
                var element= this
                if(element.type == 'checkbox'){
                    if(element.name != ''){
                        data[element.name] = element.checked;
                    }
                }else{
                    if(element.name != '' && element.value != ''){
                        data[element.name] = element.value;
                    }  
                }
            });

            $.ajax({
                type: 'GET',
                url: '/admin/puestosdevotacion/list',
                data: data,
                success: function(result){
                    var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(result);
                    var downloadLink = document.createElement("a");
                    downloadLink.href = uri;
                    downloadLink.download = document.title ? document.title.replace(/ /g, "_") + ".csv" : "data.csv";
                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    document.body.removeChild(downloadLink);            
                    toastr.success('Excel Generado');
                }
            });
            $( "[name=csv]" ).prop('checked', false);
        });  
    });
});