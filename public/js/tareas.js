var Tareas = function() {
  console.log('sortable--');
  /*$('.columna .columna-contenido').sortable({
    connectWith: '.columna .columna-contenido',
    placeholder: "tarea-highlight",
    //handle: '.btn',
    stop: function( event, ui ) {
      var element = $(ui.item[0]);
      var columna = element.parents('.columna-contenido');

      $.post('/api/tarea/update', {_id: element.data('id'), set: {'tareas.$.estado': columna.data('tipo')}});
    }
  }).disableSelection();*/

  $('.actualiza-estado').on('change', function() {
    $.post('/api/tarea/update', {_id: $(this).data('id'), set: {'tareas.$.estado': $(this).val()}});
  });

  $('.agregar-elemento').click(function() {
    $(this).toggleClass('hidden-xs-up');
    $(this).prev().toggleClass('hidden-xs-up');
  });

  $('.cancelar-elemento').click(function() {
    $(this).parents('form').toggleClass('hidden-xs-up');
    $(this).parents('form').next().toggleClass('hidden-xs-up');
  });

  $('.tarea-comment-form').on('submit', function(e) {
    e.preventDefault();
    e.stopPropagation();

    var id = $(this).data('id');
    var comentario = $(this).find('textarea').val();

    $(this)[0].reset();
    $(this).toggleClass('hidden-xs-up');
    $(this).next().toggleClass('hidden-xs-up');

    $('.app-message-chats[data-id="' + id + '"] h5').remove();

    // Dejar comentario:
    $.post('/api/tarea/comentario', {_id: id, comentario: comentario}, function(response) {
      // Agrego este comentario "virtual":
      var html = $('#chatTemplate').html();
          html = html.replace('[comentario]', comentario);
          html = html.replace('[fecha]', new Date().getTime()/1000);
          html = html.replace('[id]', id);
          html = html.replace('[comentario_id]', response._id);

      $('.app-message-chats[data-id="' + id + '"] .chats').prepend(html);
      bindComentarios();
    });
  });

  bindComentarios();
}

function bindComentarios() {
  $('.chat .eliminar-comentario').off().click(function() {
     var id = $(this).data('id');
     var comentario = $(this).data('comment');
     $.post('/api/tarea/comentario/delete', {_id: id, comentario: comentario});
     $(this).parents('.chat').slideUp(function() {
       $(this).remove();
     })
  });
}
