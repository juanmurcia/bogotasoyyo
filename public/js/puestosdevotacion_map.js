$(window).on('siteReady', function() {
	let result =  $('input[name="results"]').val();
	result = JSON.parse(result);
    var localidad = {};
    var indicador = {};
    var overlays = {};

    $.each(result, function( index, value ) {
    	if(value.localidad){
    		localidad[value.localidad.nombre] = L.layerGroup();
    		indicador[value.localidad.nombre] = L.layerGroup();	
    	}
    });

    $.each(result, function( index, value ) {
    	//var mensaje = "<div><h4><b>"+value.nombre+"</b></h4><hr><br><ul><li>Pot. Declarado : "+value.potencial+"</li><li>Pot. Real : "+value.personajes+"</li></ul></div>";

    	if(value.localidad && value.localizacion != '0' && value.localizacion){
    		var mensaje = "<div>"+
    			"<h4><b>"+value.nombre+"</b></h4><hr>"+
    			"<h5>"+value.direccion+"</h5><h5>"+value.barrio+","+value.localidad.nombre+"</h5>"+
    			"<hr>"+
    			"<div class='float-rigth' style='cursor:pointer' onclick='goStreet(this);' data-refer='"+value.localizacion+"' ><i class='icon fa-street-view' aria-hidden='true'></i></div>"+
    			"</div>";
			var geo = value.localizacion;
    		geo = geo.split(',');
    		let color = 'black';
            let potencial = value.potencial;

            let partido = 'PARTIDO X';
            let votos = 0;
            potencial = jQuery.map( potencial, function( a ) {
                if(a.total > votos){
                    partido = a.partido;
                    votos = a.total;
                }
                return a;
            });

    		if(partido == 'PARTIDO CAMBIO RADICAL'){
    			color = 'purple';
    		}else if(partido == 'PARTIDO ALIANZA VERDE'){
                color = 'green';
            }else if(partido == 'PARTIDO CONSERVADOR COLOMBIANO'){
                color = 'blue';
            }else if(partido == 'PARTIDO CENTRO DEMOCRÁTICO'){
                color = 'grey';
            }else if(partido == 'LIBRES'){
                color = 'yellow';
            }else if(partido == 'PARTIDO LIBERAL COLOMBIANO'){
                color = 'red';
            }

    		var dmarker = localidad[value.localidad.nombre];
    		var dcircle = indicador[value.localidad.nombre];


    		L.marker([geo[0], geo[1]]).bindPopup(mensaje).addTo(dmarker);
    		L.circle([geo[0], geo[1]], {
			    color: color,
			    fillColor: color,
			    fillOpacity: 0.5,
			    radius: 100
			}).bindPopup(mensaje).addTo(dcircle);	
    	}		
	});

	var mbAttr = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ';
		mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

	var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
		streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});

    var layer = [];
    var constant = 1;
	layer[0] = grayscale;

	$.each(result, function( index, value ) {
    	if(value.localidad){
    		/*
    		layer[constant] = localidad[value.localidad.nombre];
    		constant ++;
			*/
    		layer[constant] = indicador[value.localidad.nombre];
    		constant ++;
    	}
    });

	var map = L.map('mapId', {
		center: [4.6306374, -74.1231804],
		zoom: 11.75,
		minZoom: 0,
		maxZoom: 18,
		zoomSnap: 0,
		zoomDelta: 0.25,
		layers: layer
	});

	var baseLayers = {
		"Grayscale": grayscale,
		"Streets": streets
	};

	$.each(result, function( index, value ) {
    	if(value.localidad){
    		overlays[value.localidad.nombre] = localidad[value.localidad.nombre];
    		//overlays['IND -'+value.localidad.nombre] = indicador[value.localidad.nombre];	
    	}
    });

	L.control.layers(baseLayers, overlays).addTo(map);
});


function goStreet(obj){
	var geo = $(obj).data('refer');
	var url = 'http://maps.google.com/maps?q=&layer=c&cbll='+geo+'&cbp=11,270,0,0,0';
	window.open(url, '_blank');
}

