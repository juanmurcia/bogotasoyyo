$(document).ready(function() {

	$('[name=acceso]').click(function(){
		cargarContacto();
	});

	$("[name = documento]").keypress(function(e) {
        if (e.which == 13) {
    		cargarContacto();
            return false;
        }
    });
});

function cargarContacto(){
	var documento = $('[name=documento]').val();

	if(documento.length > 6){
		$('#newContact .panel-body').addClass('loading');
        $.getJSON('/api/document/' + $('input[name=documento]').val(), function(data) {            	
        	$('#newContact .panel-body').removeClass('loading');
        	let antecedentes = '';
        	if(data.antecedentes){
        		antecedentes = data.antecedentes.replace('<!--?xml version="1.0" encoding="utf-8"?-->','');
            	antecedentes = antecedentes.replace('h1','h3');
            	antecedentes = antecedentes.replace('h2','h4');	                	                	
        	}

            if(!antecedentes){
            	toastr["error"](data.error);
            	$('[name=documento]').select();
			}else if(antecedentes.indexOf("VIGENCIA") > -1){
				toastr["error"](antecedentes, 'Documento Invalido para continuar el registro');
            	$('[name=documento]').select();
            }else if(data.puesto_votacion.municipio != 'BOGOTA. D.C.'){
            	toastr["error"]('Documento no esta habilitado para votar en BOGOTA D.C');
				$('[name=documento]').select();
            }else {
                if ('error' in data) {
					toastr["error"]('Documento Invalido para continuar el registro');
					$('[name=documento]').select();
                }else if (data && 'nombre' in data) {
                    toastr["success"]('Documento Valido para continuar el registro');
					
					$('.identContact').hide();	                    	
					$('.detailContact').show();	                    	
					$('.btnRegistro').show();	                    	
                    $('input[name="nombre"]').val(data.nombre);
                    $('[name="lbNombre"]').text(data.nombre);
                    $('#registrar').attr("disabled", false); 
                }
            }
        })
	}else{
		toastr["info"]('Debe diligenciar el documento');
		$('[name=documento]').select();
	}

}