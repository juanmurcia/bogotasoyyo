(function(global, factory) {
    if (typeof define === "function" && define.amd) {
        define('/Autoload', ['exports', 'jquery'], factory);
    } else if (typeof exports !== "undefined") {
        factory(exports, require('jquery'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod.exports, global.jQuery);
        global.Autoload = mod.exports;
    }
})(this, function(exports, _jquery) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    
    exports.getInstance = exports.run = exports.Autoload = undefined;

    var _jquery2 = babelHelpers.interopRequireDefault(_jquery);

    var Autoload = function() {
        function Autoload() {
            var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
            babelHelpers.classCallCheck(this, Autoload);
            this.onLoadCallback = null;
            this.loading = 0;
            this.queued = [];

            return babelHelpers.possibleConstructorReturn(this, (Autoload.__proto__ || Object.getPrototypeOf(Autoload)).apply(this, arguments));
        }

        babelHelpers.createClass(
            Autoload, [{
                key: 'run',
                value: function run() {
                    var self = this;
                    var pluginsToLoad = false;
                    var onLoadCallback = false;

                    if(arguments.length > 0) {
                      if(typeof arguments[0] == 'object') {
                        pluginsToLoad = arguments[0];
                        if(typeof arguments[1] == 'function') {
                          onLoadCallback = arguments[1];
                        }
                      } else if(typeof arguments[0] == 'function') {
                        onLoadCallback = arguments[0];
                      }
                    }

                    console.log('Autoload initialized.');

                    (0, _jquery2.default)('[data-plugin]', 'body').each(function() {
                        var $this = (0, _jquery2.default)(this),
                            name = $this.data('plugin');
                        if (name && self.queued.indexOf(name) < 0) {
                            self.loadPlugin(name);
                        }
                    });

                    if(pluginsToLoad) {
                      for(var i in pluginsToLoad) {
                        this.loadPlugin(pluginsToLoad[i]);
                      }
                    }

                    this.onLoadCallback = onLoadCallback;
                }
            }, {
                key: 'loadPlugin',
                value: function loadPlugin(name) {
                    this.queued.push(name);
                    switch (name) {
                        case 'matchHeight':
                            this.load('matchheight/jquery.matchHeight-min.js', 'js');
                            this.load('matchheight.js', 'plugin');
                            break;
                        case 'slidepanel':
                            this.load('slidepanel/jquery-slidePanel.min.js', 'js');
                            this.load('slidepanel/slidePanel.min.css', 'css');
                            this.load('slidepanel.js', 'plugin');
                            break;
                        case 'screenfull':
                            this.load('screenfull/screenfull.js', 'js');
                            break;
                        case 'TouchSpin':
                            this.load('bootstrap-touchspin/bootstrap-touchspin.css', 'css');
                            this.load('bootstrap-touchspin/bootstrap-touchspin.min.js', 'js');
                            this.load('bootstrap-touchspin.js', 'plugin');
                            break;
                        case 'jquery-selective':
                            this.load('jquery-selective/jquery-selective.css', 'css');
                            this.load('jquery-selective/jquery-selective.min.js', 'js');
                            break;
                        case 'select2':
                            this.load('select2/select2.min.css', 'css');
                            this.load('select2/select2.min.js', 'js');
                            this.load('select2.js', 'plugin');
                            break;
                        case 'datepicker':
                            this.load('bootstrap-datepicker/bootstrap-datepicker.min.css', 'css');
                            this.load('bootstrap-datepicker/bootstrap-datepicker.js', 'js');
                            this.load('bootstrap-datepicker.js', 'plugin');
                            break;
                        case 'timepicker':
                            this.load('jt-timepicker/jquery.timepicker.min.js', 'js');
                            this.load('jt-timepicker/jquery-timepicker.min.css', 'css');
                            this.load('jt-timepicker.js', 'plugin');
                            break;
                        case 'tokenfield':
                            this.load('bootstrap-tokenfield/bootstrap-tokenfield.min.css', 'css');
                            this.load('bootstrap-tokenfield/bootstrap-tokenfield.min.js', 'js');
                            this.load('bootstrap-tokenfield.js', 'plugin');
                            break;
                        case 'tabs':
                            //this.load('bootstrap-tokenfield/bootstrap-tokenfield.min.css', 'css');
                            break;
                        case 'alertify':
                            this.load('alertify/alertify.min.css', 'css');
                            this.load('alertify/alertify.js', 'js');
                            break;
                        case 'zoom':
                            this.load('zoom/zoom.css', 'css');
                            this.load('zoom/zoom.js', 'js');
                            break;
                        case 'menu':
                            this.load('menu', 'plugin');
                            break;
                        case 'datepair':
                            this.load('datepair/datepair.min.js', 'js');
                            this.load('datepair.js', 'plugin');
                            break;
                        default:
                            return;
                            break;
                    }

                    console.info('Autoloading plugin', name);
                }
            }, {
                key: 'load',
                value: function load(path, type) {
                    var set = (type == 'plugin') ? 'js/Plugin' : 'vendor';
                    var self = this;

                    path = [(window.location.origin == 'file://') ? '..' : window.location.origin, 'assets', set, path].join('/');

                    switch (type) {
                        case 'js':
                        case 'script':
                        case 'plugin':
                            self.loading++;
                            var _script = (0, _jquery2.default)('<script>')
                              .appendTo('head')
                              .attr('type', 'text/javascript')
                              .attr('src', path)
                              .on('load', function() {
                                self.loading--;
                                self.checkAllLoaded();
                              });
                            /*
                            _jquery.getScript(path).done(function() {
                                self.loading--;
                                self.checkAllLoaded();
                            }).fail(function() {
                              console.error('Autoload: failed to retrieve script for plugin at', path);
                            });
                            */
                            break;
                        case 'css':
                        case 'style':
                            self.loading++;
                            var _style = (0, _jquery2.default)('<link>')
                                .appendTo('head')
                                .attr({
                                    type: 'text/css',
                                    rel: 'stylesheet',
                                    href: path
                                });

                            self.cssIsLoaded(_style[0], function() {
                                self.loading--;
                                self.checkAllLoaded();
                            })
                            break;
                        default:
                            console.warn('Autoload: Unsupported type', type);
                            break;
                    }
                }
            }, {
                key: 'checkAllLoaded',
                value: function checkAllLoaded() {
                    if (this.loading === 0 && this.onLoadCallback) {
                        try {
                            console.log('Autoload done, triggering callback..');
                            this.onLoadCallback();
                        } catch (e) {
                            console.error('Autload:', e);
                        }
                    }
                }
            }, {
                key: 'cssIsLoaded',
                value: function cssIsLoaded(cssStylesheet, callback) {
                    var self = this;
                    var cssLoaded = 0;
                    try {
                        if (cssStylesheet.sheet && cssStylesheet.sheet.cssRules.length > 0)
                            cssLoaded = 1;
                        else if (cssStylesheet.styleSheet && cssStylesheet.styleSheet.cssText.length > 0)
                            cssLoaded = 1;
                        else if (cssStylesheet.innerHTML && cssStylesheet.innerHTML.length > 0)
                            cssLoaded = 1;
                    } catch (ex) {}

                    if (cssLoaded) {
                        try {
                            callback();
                        } catch (e) {
                            console.error('Autoload.cssIsLoaded', e);
                        }
                    } else {
                        setTimeout(function() {
                            self.cssIsLoaded(cssStylesheet, callback);
                        }, 50);
                    }
                }
            }]
        );

        return Autoload;
    }();

    var instance = null;

    function getInstance() {
      if (!instance) {
        instance = new Autoload();
      }
      return instance;
    }

    function run() {
      var auto = getInstance();
      auto.run.apply(auto, arguments);
    }

    exports.default = Autoload;
    exports.Autoload = Autoload;
    exports.run = run;
    exports.getInstance = getInstance;
});
