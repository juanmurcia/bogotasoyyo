/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2017 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(document, window, $) {
  'use strict';

  var Site = window.Site;

  $(document).ready(function($) {
    Site.run();

    let valor = 0;
    $('.pie-progress').each(function(){
      valor = $(this).attr('id');
      if(valor){
        $(this).asPieProgress('go', valor+'%');  
      }else{
        console.log("Sin Datos PieProgress");
      }
      
    });
  });

  console.log('PieProgress...');
})(document, window, jQuery);
