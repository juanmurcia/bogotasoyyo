/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2017 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(document, window, $) {
  'use strict';

  var Site = window.Site;

  $(document).ready(function($) {
    Site.run();
  });

  // Example Gauge Dynamic
  // ---------------------
  $(document).ready(function($) {
    var dynamicGauge = $("#exampleDynamicGauge").data('gauge');
    console.log('Gauge 1');
    console.log(dynamicGauge);
    /*setInterval(function() {
      var random = Math.round(Math.random() * 1000);

      var options = {
        strokeColor: Config.colors("primary", 500)
      };
      if (random > 700) {
        options.strokeColor = Config.colors("pink", 500);
      } else if (random < 300) {
        options.strokeColor = Config.colors("green", 500);
      }

      dynamicGauge.setOptions(options)
        .set(random);
    }, 1500); */
  });

  // Example Donut Dynamic
  // ---------------------
  $(document).ready(function($) {
    setTimeout(function(){
      $('.grafDonut').each(function(){
        console.log('Gauge 2');

        var dynamicDonut = $(this).data('donut');
        console.log(dynamicDonut);
        var random = 0;

        if($(this).data('porcentaje')){
          random = $(this).data('porcentaje');
        }
        if(random == 0) random = 1;

        var options = {
          strokeColor: Config.colors("primary", 500)
        };
        if (random > 70) {
          options.strokeColor = Config.colors("green", 500);
        } else if (random < 30) {
          options.strokeColor = Config.colors("pink", 500);
        }

        dynamicDonut.setOptions(options)
          .set(random);
      })
    },6000);
  });
})(document, window, jQuery);
