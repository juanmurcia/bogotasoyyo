"use strict";
const moment = require('moment');
const async  = require('async');
const _      = require('lodash');

module.exports = () => {  
  console.log('Obteniendo Cumpleaños del dia...');

  async.parallel([
    (callback) => {
      let nextDays = [];
      let today = Date.now();
      let oneday = (1000 * 60 * 60 * 24);
      let inDays = Date.now() + (oneday * 7);

      //  make an array of all the month/day combos for the next 30 days    
      for (var i = today; i < inDays; i = i + oneday) {
          let thisday = new Date(i);
          nextDays.push({
              "m": thisday.getMonth() + 1,
              "d": thisday.getDate()
          });
      }

      global.models.Personaje.aggregate([{
              $project: {
                  m: { $cond: [{ $ifNull: ['$fecha_nacimiento', false] }, { $month: '$fecha_nacimiento' }, -1] },
                  d: { $cond: [{ $ifNull: ['$fecha_nacimiento', false] }, { $dayOfMonth: '$fecha_nacimiento' }, -1] },
                  nombre: "$nombre",
                  fecha_nacimiento: "$fecha_nacimiento",
                  foto_perfil: "$foto_perfil",
                  telefono_movil: "$telefono_movil",
                  email_personal: "$email_personal"
              }
          },
          {
              $match: {
                  $or: nextDays,
                  deleted: {
                      $ne: true
                  }
              }
          },
          {
              $group: {
                  _id: {
                      month: "$m",
                      day: "$d",
                  },
                  personajes: { $push: { _id: "$_id", fecha_nacimiento: "$fecha_nacimiento", nombre: "$nombre", foto_perfil: "$foto_perfil", telefono_movil: "$telefono_movil", email_personal: "$email_personal", color: "$color" } }
              }
          },
          {   $sort : { 
                  fecha_nacimiento : 1 
              } 
          }
      ]).exec((err, personajes_cumple) => {
          callback(err, personajes_cumple);
      });
    }
  ], (err, data) => {
    if(err) {
      console.log('[recordatorio] Error:', err);
    } else {      
      let [cumple] = data;
      let enviar = _.size(cumple);

      if(enviar > 0){
        let send_email = {};
        send_email['juanmurcia.1402@gmail.com'] = 'Juan Murcia';
        send_email['gestionessm2017@gmail.com'] = 'Gestiones SM';
        send_email['smoralespersonal@gmail.com'] = 'Santiago Morales';


        global.sendMail(
          ' ',
          send_email,
          'Recordatorio Cumpleaños de la Semana',
          'recordatorio',
          {datos: cumple},
          (status) => {}
        );
      }      
      console.log('Fin.');
    }
  });
};
