"use strict";
const moment = require('moment');
const async  = require('async');
const _      = require('lodash');

module.exports = () => {  
  console.log('... Actualizando token acceso ...');

  async.parallel([
    (callback) => {
      let longitud = 15;
      let caracteres = "ABCDEFGHIJKLMNPQRTUVWXYZ2346789";
      let passToken = "";
      let i = 0;
      for (i=0; i<longitud; i++) 
        passToken += caracteres.charAt(Math.floor(Math.random()*caracteres.length));      

      global.models.User.updateMany({"role":{ $in: [ "admin", "contador" ] }},{token: passToken}).exec((err, usuario) => {
        if(err){
          req.flash('error', "Contraseña Incorrecta");
          return res.sendStatus(500);
        }

        callback(null, '{"Actualizando"}');
      });
    }
  ], (err, data) => {
    if(err) {
      console.log('[Pass Token] Error:');
      console.log(err);
    } 

    console.log('Fin.');
  });
};