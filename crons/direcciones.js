"use strict";
const moment = require('moment');
const async  = require('async');
const _      = require('lodash');
const LocalizaDireccion = require('../utils/localiza-direccion');

module.exports = () => {  
  console.log('Geo - Referencia direccion puestos Votacion ...');

  async.parallel([
    (callback) => {

      global.models.PuestoVotacion.find().exec((err, PuestosVotacion) => {
        if(err){
          req.flash('error', err.messagge);
          return res.sendStatus(500);
        }

        let total = _.size(PuestosVotacion);
        _.each(PuestosVotacion, (puesto) => {
          console.log(puesto.nombre);
          LocalizaDireccion(puesto.direccion, (details) => {
            console.log('details');
            console.log(details);

            if(!details) {
              res.json({error: 'El documento no existe en la registraduria nacional.'});
              return;
            }

            if(total == 0){
              callback(null, total);
            }
          });         

          total = total - 1;
        });  
      });
    }
  ], (err, data) => {
    if(err) {
      console.log('[recordatorio] Error:', err);
    } else {
      

      console.log('Fin.');
    }
  });
};
