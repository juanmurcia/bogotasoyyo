"use strict";
const moment = require('moment');
const async  = require('async');
const _      = require('lodash');
const Antecedentes = require('../utils/antecedentes');
const PuestoVotacion = require('../utils/puesto-votacion');

module.exports = () => {  
  console.log('Obteniendo Puesto de Votacion !');

  async.parallel([
    (callback) => {
      global.models.Personaje.find().exec((err, personajes) => {
        _.each(personajes, (personaje) => {
          
          console.log('Personaje :'+personaje.nombre);
          Antecedentes(personaje.documento, (details) => {
            if(!details) {
              console.log('err0');
              console.log(err);
            }
            PuestoVotacion(personaje.documento, (puesto) => {
              personaje.antecedentes = details.antecedentes;
              puesto.nombre = _.toUpper(puesto.nombre);
              //Validacion para recargar puestos de Votación
              if(typeof personaje.puesto_votacion != 'undefined'){
                if(!personaje.puesto_votacion){
                  personaje.puesto_votacion = {
                    nombre : '-'
                  };  
                }
              }

              if(typeof personaje.puesto_votacion == 'undefined' || personaje.puesto_votacion.nombre != puesto.nombre) {
                global.models.PuestoVotacion.findOne({ nombre: puesto.nombre }).lean().exec((err, existing_puesto) => {
                  if (!existing_puesto) {
                    let nuevo = global.models.PuestoVotacion(puesto);
                        nuevo.save((err, saved) => {
                          if(err) {
                            console.log('err1');
                            console.log(err);
                          }

                          personaje.puesto_votacion = saved._id;
                          personaje.mesa_votacion = puesto.mesa;
                          personaje.save((err) => {
                            if(err){
                              console.log('err2');
                              console.log(err);
                            }
                            console.log('Actualizado NEW '+puesto.nombre);
                          });
                        });
                  } else {
                    personaje.puesto_votacion = existing_puesto._id;
                    personaje.mesa_votacion = puesto.mesa;
                    personaje.save((err) => {
                      if(err){
                        console.log('err3');
                        console.log(err);
                      }
                      console.log('Actualizado OLD '+puesto.nombre);
                    });
                  }
                });
              } 
            });

          });

        });

        callback(err, personajes);
      });
    },
    (callback) => {
      global.models.PuestoVotacion.find({ municipio: new RegExp('BOGOTA. D.C.', 'ig') },{nombre : 1, registrados : 1}).exec((err, existing_puesto) => {        
        _.each(existing_puesto, (puesto) => {
          console.log('Actualiza Puesto '+puesto.nombre);
          puesto.registrados = [];

          global.models.Personaje.find({ puesto_votacion: puesto._id },{nombre: 1}).lean().exec((err, personajes) => {
            _.each(personajes, (personaje) => {
              console.log(' ===> Personaje '+personaje.nombre);
              puesto.registrados.push({'personaje' : personaje._id });
            });
            console.log(puesto);

            puesto.save((err) => {
              if(err){
                console.log('err Puesto Registro');                
                console.log(err);
              }
              console.log('Actualizado => '+puesto.nombre);
            });
          });
        });
      });
    }
  ], (err, data) => {
    if(err) {
      console.log('[Puesto] Error:', err);
    } else {      
      console.log('Fin.');
    }
  });
};
