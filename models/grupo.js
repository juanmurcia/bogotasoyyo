"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();
const mongoose_delete = require('mongoose-delete');

const GrupoSchema = new Schema({
  nombre: {type: String, unique: true, required: true},
  creador: { type: ObjectId, ref: 'User', set: global.ignoreEmpty },
}, { timestamps: true });

GrupoSchema.plugin(mongoose_delete, { deletedAt: true, overrideMethods: ['count', 'find'], deletedBy: true });

GrupoSchema.pre('save', function(next) {
  if(this.isNew) {
    this.wasNew = true;
  }
  next();
});

GrupoSchema.post('save', function(doc) {
  let log = new Log({
    model: 'Grupo',
    document: doc._id,
    user: global.user._id,
    type: (doc.wasNew) ? 'INSERT' : 'UPDATE'
  });

  log.save();
});

module.exports = (connection) => {
  return mongoose.model('Grupo', GrupoSchema);
}
