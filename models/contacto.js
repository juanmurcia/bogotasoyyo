"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();

const ContactoSchema = new Schema({
  	documento: {type: String, unique: true},
  	nombre: String,
  	apellido: String,
  	direccion: String,
  	telefono_movil: String,
	telefono_fijo: {
		indicativo: Number,
		numero: Number
	},
  	email: { type: String, set: global.ignoreEmpty },
  	fec_contacto: { type: Date, set: global.ignoreEmpty },
  	estado_contacto: String,
  	creador: { type: ObjectId, ref: 'User', set: global.ignoreEmpty },
}, { timestamps: true });

ContactoSchema.index({
	documento : 'text',
	creador : 'text'
});

ContactoSchema.pre('save', function (next) {
	if(this.isNew){
		this.wasNew = true;
	}
	next();
});	

ContactoSchema.post('save', function (doc) {
	let log = new Log({
		model : 'Contacto',
		document : doc._id,
		user: global.user._id,
		type: (doc.wasNew) ? 'INSERT' : 'UPDATE'	
	});

	log.save();
});

module.exports = () => {
	return mongoose.model('Contacto', ContactoSchema);
};
