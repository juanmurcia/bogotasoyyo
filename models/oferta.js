"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();

module.exports = (connection) => {
  const OfertaSchema =  new Schema({
    personaje: {type: ObjectId, ref: 'Personaje'},    
    entidad: {type: ObjectId, ref: 'Entidad'},    
    titulo: String,
    descripcion: String,
    //Rango Fechas Convocatoria
    fecha_inicio: Date,
    fecha_fin: Date,
    vacantes : Number,    
    ofertar : Number,   
    ofertado : Number,
    usado : Number, 
    remuneracion : Number,    
    estado : String,
    //Guarda Ofertas a las que ha aplicado la HV
    personaje_ofertado: [{
        personaje: { type: ObjectId, ref: 'Personaje', set: global.ignoreEmpty },
        fecha: Date,
        observacion: String,
        cantidad_ofertada: String,
        creador: { type: ObjectId, ref: 'User', set: global.ignoreEmpty },
    }],
    //Guarda Ofertas a las que ha aplicado la HV
    hoja_vida_enviada: [{
        hoja_vida: { type: ObjectId, ref: 'HojaDeVida', set: global.ignoreEmpty },
        fecha: Date,
        descripcion: String,
        estado: String,
        creador: { type: ObjectId, ref: 'User', set: global.ignoreEmpty },
    }],
    //Guarda Ofertas a las que han aceptado la HV
    hoja_vida_eliminada: [{
        hoja_vida: { type: ObjectId, ref: 'HojaDeVida', set: global.ignoreEmpty },
        fecha: Date,
        descripcion: String,
        creador: { type: ObjectId, ref: 'User', set: global.ignoreEmpty },
    }],
    creador : {type: ObjectId, ref: 'User'}
  }, { timestamps: true });

  OfertaSchema.index({
    descripcion: 'text',
    tipo: 'text'
  });

  OfertaSchema.post('save', function(doc) {
    let log = new Log({
      model: 'Oferta',
      document: doc._id,
      user: global.user._id,
      type: (doc.wasNew) ? 'INSERT' : 'UPDATE'
    });

    log.save();
  });

  return mongoose.model('Oferta', OfertaSchema);
}
