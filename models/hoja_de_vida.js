"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();

const HojaDeVidaSchema = new Schema({
    personaje: {type: String, ref: 'Personaje'},
    referido: {type: String, ref: 'Personaje'},
    prioridad: String,
    profesion: String,
    ubicacion: String, //Carpeta DropBox
    perfil: String,
    fecha_entregada: Date,
    estado: String,
    remuneracion: Number,
    comentarios: [{type: String, ref: 'Comentario'}],
    creador: { type: ObjectId, ref: 'User', set: global.ignoreEmpty }
  }, { timestamps: true });

HojaDeVidaSchema.pre('save', function(next) {
  if(this.isNew) {
    this.wasNew = true;
  }
  next();
});

HojaDeVidaSchema.post('save', function(doc) {
  let log = new Log({
    model: 'HojaDeVida',
    document: doc._id,
    user: global.user._id,
    type: (doc.wasNew) ? 'INSERT' : 'UPDATE'
  });

  log.save();
});

module.exports = (connection) => {
  return mongoose.model('HojaDeVida', HojaDeVidaSchema);
}
