"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();

const UserSchema = new Schema({
  documento: String,
  name: String,
  creador: { type: ObjectId, ref: 'User', set: global.ignoreEmpty },
  personaje: { type: ObjectId, ref: 'Personaje', set: global.ignoreEmpty },
  email: {type: String, unique: true},
  password: String,
  active: {type: Boolean, default: true},
  profile_pic: {type: String, default: '3203981f-93c8-4379-a072-d5ca14606352jpg'},
  recover_string: String,
  role: String,
  token : String 
}, { timestamps: true });

UserSchema.index({
	nombre : 'text',
	email : 'text',
	password : 'text',
	role : 'text'
});

UserSchema.pre('save', function (next) {
	if(this.isNew){
		this.wasNew = true;
	}
	next();
});

UserSchema.post('save', function (doc) {
	let log = new Log({
		model : 'User',
		document : doc._id,
		user: global.user._id,
		type: (doc.wasNew) ? 'INSERT' : 'UPDATE'	
	});

	log.save();
});

module.exports = () => {
  return mongoose.model('User', UserSchema);
};
