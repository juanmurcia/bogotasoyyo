"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();

const LocalidadSchema = new Schema({
  nombre: String,
  latitud: Number,
  longitud: Number
}, { timestamps: true });


LocalidadSchema.pre('save', function(next) {
  if(this.isNew) {
    this.wasNew = true;
  }
  next();
});

LocalidadSchema.post('save', function(doc) {
  let log = new Log({
    model: 'Localidad',
    document: doc._id,
    user: global.user._id,
    type: (doc.wasNew) ? 'INSERT' : 'UPDATE'
  });

  log.save();
});

module.exports = (connection) => {
  return mongoose.model('Localidad', LocalidadSchema);
}
