"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();

const UploadSchema = new Schema({
  model: String,
  document: {type: ObjectId, refPath: 'model'},
  usuario: {type: ObjectId, ref: 'User'},
  originalname: String,
  filename: String,
  path: String,
  filesize: String,
  mimetype: String
}, { timestamps: true });


UploadSchema.pre('save', function(next) {
  if(this.isNew) {
    this.wasNew = true;
  }
  next();
});

UploadSchema.post('save', function(doc) {
  let log = new Log({
    model: 'Upload',
    document: doc.document,
    path: doc.model.toLowerCase() + 's',
    user: global.user._id,
    type: (doc.wasNew) ? 'INSERT' : 'UPDATE'
  });

  log.save();
});

module.exports = () => {
  return mongoose.model('Upload', UploadSchema);
};
