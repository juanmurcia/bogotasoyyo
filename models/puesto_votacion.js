"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();

const PuestoVotacionSchema = new Schema({
  	nombre: {type: String, index: true},
    departamento: String,
    municipio: String,
    direccion: String,
    barrio: String,
    potencial: [{
      total: Number,
      partido: String
    }],
    registrados: [{
      personaje: {type: ObjectId, ref: 'Personaje', set: global.ignoreEmpty}
    }],
    votos: Number,
    potencial_electoral: Number,
    censo_mujer: Number,
    censo_hombre: Number,
    mesas: Number,
    localizacion: String,
  	localidad: { type: ObjectId, ref: 'Localidad', set: global.ignoreEmpty }
}, { timestamps: true });

PuestoVotacionSchema.index({
	nombre : 'text',
	departamento : 'text',
	municipio : 'text'
});

PuestoVotacionSchema.pre('save', function (next) {
	if(this.isNew){
		this.wasNew = true;
	}
	next();
});	

PuestoVotacionSchema.post('save', function (doc) {
	let log = new Log({
		model : 'PuestoVotacion',
		document : doc._id,
		user: global.user._id,
		type: (doc.wasNew) ? 'INSERT' : 'UPDATE'	
	});

	log.save();
});

module.exports = () => {
	return mongoose.model('PuestoVotacion', PuestoVotacionSchema);
};