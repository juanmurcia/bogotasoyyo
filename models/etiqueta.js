"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

module.exports = (connection) => {
  return mongoose.model('Etiqueta',
    new Schema({
      nombre: String
    }, { timestamps: true })
  );
}