"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Log = require('./log')();

const ComentarioSchema = new Schema({
  usuario: {type: ObjectId, ref: 'User'},
  model: String,
  referencia: {type: ObjectId, refPath: 'model'},
  comentario: String,
}, { timestamps: true });

ComentarioSchema.index({
    comentario: 'text'
});

ComentarioSchema.pre('save', function(next) {
  if(this.isNew) {
    this.wasNew = true;
  }
  next();
});

ComentarioSchema.post('save', function(doc) {
  let log = new Log({
    model: 'Comentario',
    document: doc.referencia,
    path: 'personajes',
    user: global.user._id,
    type: (doc.wasNew) ? 'INSERT' : 'UPDATE'
  });

  log.save();
});

module.exports = () => {
  return mongoose.model('Comentario', ComentarioSchema);
}
