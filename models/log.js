"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

/*
  Comentario
  HojaDeVida
  Localidad
  Organizacion
  Personaje
  Proyecto
*/

const LogSchema = new Schema({
  model: String,
  document: {type: ObjectId, refPath: 'model'},
  user: {type: ObjectId, ref: 'User'},
  type: String,
  path: String,
}, { timestamps: true });

module.exports = () => {
  return mongoose.model('Log', LogSchema);
};
