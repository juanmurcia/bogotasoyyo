// This middleware checks if the user has a jwt token and if its valid.
const jwt = require('jsonwebtoken');
const secret = '^vcMIUHLiIxZXj8oQmuJ^!7VAFMvscFG0HyRDfbGszPyWyika1';
const bcrypt = require('bcrypt-nodejs');
const async = require('async');

function jwtCheck(req, res, next) {
  var token = req.cookies.jwt_auth || req.headers['x-access-token'];
    
  if (token) {
    jwt.verify(token, global.secret, function(err, decoded) {
      if (err) {
        console.log('middleware/jwt.js', err);
        return res.redirect('/api/login');
      } else {

        async.parallel([
            (cb) => {

              global.models.User.findOne({"_id":decoded._doc._id}, {token : 1, name : 1 }).exec((err, usuario) => {
                global.user = decoded._doc;
                global.user.role = decoded._doc.role;
                global.user.token = usuario.token;          
                //Asigna Cookie
                res.cookie('role',global.user.role, { expires: new Date(2147483647000), httpOnly: true, sameSite: true });
                req.decoded = decoded._doc;

                cb(err, usuario);
              });            
            },
            (cb) => {

              global.models.User.findOne({"role":"admin"}).exec((err, usuario) => {

                let passToken = bcrypt.hashSync(usuario.token);
                res.cookie('mms_ts', passToken, { expires: new Date(2147483647000), httpOnly: true, sameSite: true });

                cb(err, usuario);
              });

            }
          ], (err, data) => {
          if (err) {
              console.log('Auth err');
              console.log(err);
          }

          next();
        })      
      }
    });
  } else {
    return res.redirect('/api/login');
  }
}

module.exports = jwtCheck;