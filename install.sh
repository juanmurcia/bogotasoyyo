#!/bin/bash

echo "Installing node dependencies and nodemon...";
npm install;
npm install -g nodemon;
echo "Done.";
