"use strict";
process.env.TZ = 'Americas/Bogota';

const express = require('express');
const route = express.Router();
const jwt = require('jsonwebtoken');
const uuid = require('uuid/v4');
const fs = require('fs');
const multer = require('multer');
const async = require('async');

const moment = require('moment');
const _ = require('lodash');
const auth = require('../middleware/auth');
const checkPermission = require('../middleware/security');
const bcrypt = require('bcrypt-nodejs');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });
const acl = require('express-acl');

const puestoVotacion = require('../utils/puesto-votacion');

// File upload settings:

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads');
    },
    filename: function(req, file, cb) {
        cb(null, uuid() + '.' + file.mimetype.split('/')[1])
    }
});

const fileFilter = (req, file, cb) => {
    // To reject this file pass `false`, like so:
    //cb(null, false)
    // To accept the file pass `true`, like so:
    //cb(null, true)
    // You can always pass an error if something goes wrong:
    //cb(new Error('I don\'t have a clue!'))
    cb(null, true);
}

// upload middleware for file uploads.
const upload = multer({ storage: storage, fileFilter: fileFilter });
route.use(acl.authorize);

module.exports = (app, mongoose) => { 

    route.get('/', auth, (req, res) => {   
        require('./actions/dashboard/home_get')(req, res);
    });

    route.get('/error-test', (req, res) => {
        req.flash('error', 'Error message test');
        res.redirect('/admin/');
    });

    route.get('/search', auth, (req, res) => {        
        require('./actions/search')(req, res);
    });

    route.post('/upload', auth, (req, res) => {
        res.redirect('/admin/');
    });

    route.get('/migrate', (req, res) => {
        let cb_length = 0;
        global.models.Personaje.find({}).exec((err, personajes) => {
            _.each(personajes, (personaje) => {
                if (!('formacion_academica' in personaje) || personaje.formacion_academica == '') {
                    cb_length++;
                    return;
                }

                personaje.formacion = [{
                    nivel_academico: personaje.nivel_academico,
                    descripcion: personaje.formacion_academica
                }];

                delete personaje.nivel_academico;
                delete personaje.formacion_academica;

                personaje.save((err) => {
                    if (err) {
                        res.cookie('mensaje','error|Error Transacción : '+err.message);
                    }

                    cb_length++;
                    if (cb_length == personajes.length) {
                        return res.redirect('/admin');
                    }
                });
            });
        });
    });

    route.post('/personajes/:id/offer', auth, (req, res) => {
        // Dejar comentario a un personaje
        global.models.Personaje.findOne({ _id: req.params.id }).exec((err, personaje) => {
            if (err || !personaje) {
                return res.redirect(req.header('Referer') || '/');
            }

            let oferta = global.models.Oferta({
                categoria: req.body.categoria,
                usuario: global.user._id,
                personaje: personaje._id,
                tipo: req.body.tipo,
                descripcion: req.body.descripcion,
                fecha_fin: req.body.fecha_fin
            });

            oferta.save((err, saved) => {
                if (!err) {
                    if (!('ofertas' in personaje)) {
                        personaje.ofertas = [];
                    }

                    personaje.ofertas.push(saved._id);
                    personaje.save(() => {
                        return res.redirect(req.header('Referer') || '/');
                    });
                } else {
                    req.flash('error', err.message);
                    return res.redirect(req.header('Referer') || '/');
                }
            })
        })
    });

    /*route.get('/testuser', (req, res) => {
        
        bcrypt.hash('bogotasoyyo', null, null, function(err, hashed) {
            if (err) throw err;


            global.models.User.create([{
                name: 'Manuel Medina',
                email: 'omarmanuelmedina@gmail.com',
                password: hashed
            }, {
                name: 'Jorge Alvira',
                email: 'jorgeandresalviracruz@gmail.com',
                password: hashed
            }, {
                name: 'Juan Murcia',
                email: 'juanmurcia.1402@gmail.com',
                password: hashed
            }], () => {
                res.redirect('/admin/');
            });
        });
    });

    route.get('/testRepresentante', (req, res) => {
        
        global.models.Personaje.find({"representante" : true},{"documento":1}).lean().exec((err, lider) => {
            if(lider.documento){
                global.models.Personaje.find({"referido" : lider._id}, {"documento":1, "nombre":1,"email_personal":1, "telefono_movil":1, "telefono_fijo":1}).lean().exec((err, contactos) => {                
                    console.log(lider.documento);
                    console.log('contactos');
                    console.log(contactos);
                    global.models.contactos.create({
                        documento
                    });
                });
            }
        });
                
    });
    */
   
    // VIEW Proyectos
    route.get('/proyectos', auth, (req, res) => {
        require('./actions/proyectos/list_get')(req, res);
    });

    // VIEW Proyectos
    // 
    route.get('/proyectos/new', auth, (req, res) => {
        require('./actions/proyectos/new_get')(req, res);
    });

    route.post('/proyectos/:id/eliminar_tarea', auth, (req, res) => {
        global.models.Proyecto.update({ _id: req.params.id }, { $pull: { tareas: { _id: req.body.id } } }).exec((err, updated) => {
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
            }
            res.cookie('mensaje','sucess|Tarea de proyecto eliminada');
            return res.redirect(req.header('Referer') || '/');
        })
    });

    route.post('/ofertas/:id/eliminar_oferta', auth, (req, res) => {


        global.models.Oferta.update({ _id: req.params.id }, {$pull: { personaje_ofertado: { _id: req.body.id } }}).exec((err, updated) => {
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
            }

            global.models.Oferta.findOne({ _id: req.params.id }).exec((err, oferta) => {
                oferta.ofertado = oferta.ofertado - 1;
                oferta.save((err, saved) => {
                    if (err) {
                        res.cookie('mensaje','error|Error Transacción : '+err.message);
                        return res.redirect(req.header('Referer') || '/');
                    }

                    res.cookie('mensaje','sucess|Oferta para personaje eliminada');
                    return res.redirect(req.header('Referer') || '/');
                })
            });            
        })
    });

    route.post('/proyectos/:id/tarea', auth, (req, res) => {
        let tarea = {
            titulo: req.body.titulo,
            descripcion: req.body.descripcion,
            responsable: req.body.responsable,
            personaje: req.body.personaje,
            fecha_inicio: moment(req.body.fecha_inicio + ' ' + req.body.fecha_inicio_time, 'MM/DD/YYYY hh:mma').toDate(),
            fecha_fin: moment(req.body.fecha_inicio + ' ' + req.body.fecha_fin_time, 'MM/DD/YYYY hh:ma').toDate(),
            fecha_creacion: new Date,
            creado: global.user._id
        };

        // Dejar comentario a un modelo
        global.models.Proyecto.findOne({ _id: req.params.id }).exec((err, proyecto) => {
            if (err || !proyecto) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            if (!('tareas' in proyecto)) {
                proyecto.tareas = [];
            }

            proyecto.tareas.push(tarea);
            proyecto.save((err, saved) => {
                if (err) {
                    res.cookie('mensaje','error|Error Transacción : '+err.message);
                    req.flash('error', err.message);
                }

                let usuarios = _.union(proyecto.usuario_responsable, [req.body.responsable]);
                global.models.User.find({ _id: usuarios }).lean().exec((err, users) => {

                    if (!err) {
                        let asyncMail = [];
                        /*
                        _.each(users, (usuario) => {
                            asyncMail.push((callback) => {
                                if (usuario._id.toString() != req.body.responsable) {
                                    global.sendMail(
                                        usuario.name,
                                        usuario.email,
                                        'Se ha creado una nueva tarea al proyecto ' + proyecto.titulo,
                                        'new_task_notice.handlebars', { _id: proyecto._id, nombre: tarea.titulo },
                                        () => {
                                            callback(null);
                                        }
                                    );
                                } else {
                                    global.sendMail(
                                        usuario.name,
                                        usuario.email,
                                        'Tienes una nueva tarea asignada',
                                        'new_task.handlebars', { _id: proyecto.tareas[proyecto.tareas.length - 1]._id, proyecto_id: proyecto._id },
                                        () => {
                                            callback(null);
                                        }
                                    );
                                }
                            });
                        });
                        */

                        async.parallel(asyncMail, (err) => {
                            return res.redirect(req.header('Referer') || '/');
                        });
                    } else {
                        res.cookie('mensaje','error|Error Transacción : '+err.message);                        
                        res.redirect(proyecto._id);
                    }
                });
            })
        })
    });

    route.post('/ofertas/:id/ofertar', auth, (req, res) => {
        let ofertar = {
            personaje: req.body.personaje,
            observacion: req.body.observacion,
            cantidad_ofertada: req.body.cantidad_ofertada,
            cantidad_usada: 0,
            personaje: req.body.personaje,
            fecha: req.body.fecha_ofertar,
            creador: global.user._id
        };
        // Dejar comentario a un modelo
        global.models.Oferta.findOne({ _id: req.params.id }).exec((err, oferta) => {
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            if (!oferta) {
                res.cookie('mensaje','error|Oferta no encotrada');
                return res.redirect(req.header('Referer') || '/');
            }

            if (!('personaje_ofertado' in oferta)) {
                oferta.personaje_ofertado = [];
            }

            oferta.personaje_ofertado.push(ofertar);
            oferta.estado = 'Ofertada';
            oferta.ofertado = oferta.ofertado + 1;
            oferta.save((err, saved) => {
                if (err) {
                    res.cookie('mensaje','error|Error Transacción : '+err.message);
                    return res.redirect(req.header('Referer') || '/');
                }

                res.cookie('mensaje','success|Se oferto a personaje');
                return res.redirect(req.header('Referer') || '/');
            })
        })
    });

    // CRUD Universal:
    route.get('/:path', auth, (req, res) => {
        let extra = {};
        let query = {};

        extra.user = global.user;
        console.log(req.params.path+' Ingresa');
        switch (req.params.path) {
            case 'tareas':
                require('./actions/tareas/list_get')(req, res);
                return;
                break;
            case 'cumplelist':
                require('./actions/personajes/cumple_get')(req, res);
                return;
                break;
            case 'representantes':
                query.representante = true;
                break;
            case 'personajedel':
                extra.eliminados = true;
                req.params.path = 'personajes';
                break;
            case 'puestosdevotacion':
                require('./actions/puestosdevotacion/list_get')(req, res);
                return;
                break;
        }
        let model = global.getModel(req.params.path);
        if (!model) {            
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect('/');
        };

        model.count(query).exec((err, count) => {
            extra.total = count;
            return res.render(req.params.path+'/list', extra);
        });
    });

    // VIEW ALL
    route.get('/:path/list', auth, (req, res) => {
        let extra = {};
        console.log(req.params.path+' LIST')
        switch (req.params.path) {
            case 'localidades':
                require('./actions/localidades/list_get')(req, res);
                return;
                break;
            case 'cumplelist':
                require('./actions/personajes/cumple_get')(req, res);
                return;
                break;
            case 'personajedel':
                req.params.path = 'personajes';
                break;
        }       

        let model = global.getModel(req.params.path);
        if (!model) {
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect('/');
        };

        let query = {};
        let find = {};

        switch (req.params.path) {
            case 'organizaciones':
                query = { nombre: new RegExp(req.query.search, 'ig') };
                break;
            case 'puestosdevotacion':
                query = { $and: [{ municipio: new RegExp('BOGOTA. D.C.', 'ig') },{'potencial.partido' : 'PARTIDO CAMBIO RADICAL'}] };
                break;
            case 'personajes':
                switch (req.query.field) {
                    case 'nombre':
                        query = { nombre: new RegExp(req.query.search, 'ig') };
                        break;
                    case 'email':
                        query = { email_personal: new RegExp(req.query.search, 'ig') };
                        break;
                    case 'telefono':
                        query = {
                            $or: [
                                {telefono_movil: new RegExp(req.query.search, 'ig')},
                                {telefono_movil_alterno: new RegExp(req.query.search, 'ig')},
                                {'telefono_fijo.numero': new RegExp(req.query.search, 'ig')}
                            ]
                        };
                        break;
                    case 'documento':
                        query = { documento: parseInt(req.query.search) };
                        break;

                    case 'formacion':
                        query = {
                            $or: [
                                {'formacion.nivel_academico': new RegExp(req.query.search, 'ig')},
                                {'formacion.descripcion': new RegExp(req.query.search, 'ig')}
                            ]
                        };
                        break;
                }

                if (req.query.referido) {
                    query.referido = req.query.referido;
                }

                if (req.query.sinReferido == 'true') {
                    query.referido = null;
                }

                if (req.query.localidad) {
                    query.localidad = req.query.localidad;
                }
            break;
        }

        switch (req.params.path) {
            case 'representantes' :
                query.representante = true;
                find = {
                    "nombre" : 1, "foto_perfil" : 1, "referido" : 1, "localidad" : 1, "potencial_declarado_urna" : 1, "potencial_declarado_otro" : 1 , 
                    "ofertas" : 1, "proyectos" : 1, "direccion_residencia" : 1, "telefono_fijo.indicativo" :1, "telefono_fijo.numero" : 1, 
                    "puesto_votacion" : 1,"telefono_movil" : 1, "telefono_movil_alternativo" : 1, "email_personal" : 1, "pagina_web": 1, "asesor" : 1
                };
            break;
            case 'personajes' :
                find = {"documento" : 1,"sexo" : 1,"nombre" : 1, "descripcion" : 1, "foto_perfil" : 1, "referido" : 1, "localidad" : 1, "fecha_nacimiento" : 1, "comentarios" : 1 , 
                    "ofertas" : 1, "proyectos" : 1, "direccion_residencia" : 1, "telefono_fijo.indicativo" :1, "telefono_fijo.numero" : 1, 
                    "puesto_votacion" : 1, "telefono_movil" : 1, "telefono_movil_alternativo" : 1, "email_personal" : 1, "pagina_web": 1
                };
            break;
        }

        model = (req.query.deleted) ? model.findDeleted(query) : model.find(query, find);
        model = global.modelPopulate(model, req.params.path);

        switch (req.params.path) {
            case 'personajes':
                model = model.sort({'nombre': 1});
            case 'representantes':
                model = model.sort({'nombre': 1});
            case 'usuarios':
                model = model.sort({'name': 1});
            case 'hojasdevida':
                model = model.sort({'prioridad': 1,'fecha_entregada': 1});
            break;
        }

        //Valida Token de Acceso y Consulta
        let tokenAcces = true;
        /*if ('personajes' == req.params.path) {
            bcrypt.compare(global.user.token, req.cookies.mms_ts, function(err, validation) {
                if(!validation){
                    tokenAcces = false;
                }
            });
        }*/

        if('csv' in req.query && req.query.csv == 'true') {
            model.lean().exec((err, found) => {
                let csv = [];
                let detalle = '';
                let fec_nacimiento = '';
                let nombre = '';
                console.log('Generando Excel');
                switch (req.params.path) {
                    case 'personajes':
                        csv.push({
                            nombre: 'Nombre',
                            documento: 'Documento',
                            descripcion: 'Descripcion',
                            email: 'Email Personal',
                            sexo: 'Sexo',
                            fecha_nacimiento: 'Fecha Nacimiento',
                            telefono_movil: 'Teléfono móvil',
                            telefono_movil_alterno: 'Teléfono móvil alterno',
                            direccion_residencia: 'Dirección residencia',
                            referido: 'Referido por',
                            puesto_votacion: 'Puesto Votacion',
                            municipio: 'Municipio',
                            localidad: 'Localidad'
                        });
                        break;
                    case 'representantes':
                        csv.push({
                            nombre: 'Nombre',
                            documento: 'Documento',
                            telefono_movil: 'Teléfono móvil',
                            direccion_residencia: 'Dirección residencia',
                            referido: 'Referido por',
                            potencial: 'Potencial',
                            puesto_votacion: 'Puesto Votacion',
                            municipio: 'Municipio',
                            localidad: 'Localidad'
                        });
                    break;
                    case 'puestosdevotacion':
                        csv.push({
                            localidad: 'Localidad',
                            barrio: 'Barrio',
                            nombre: 'Nombre',
                            direccion: 'Dirección',
                        });
                    break;
                }
                _.each(found, (e) => {
                    detalle = '';
                    fec_nacimiento = '';
                    nombre = (e.localidad ? e.localidad.nombre : '');
                    switch (req.params.path) {
                        case 'personajes':
                            detalle = e.descripcion;
                            detalle = _.replace(detalle,";",' ');
                            detalle = _.replace(detalle,"",'');
                            detalle = _.replace(detalle,",",' ');
                            detalle = _.replace(detalle,"\n",'');
                            detalle = _.kebabCase(detalle);
                            fec_nacimiento = _.toString(e.fecha_nacimiento);

                            csv.push({
                                nombre: e.nombre,
                                documento: e.documento,
                                descripcion: detalle,
                                email: e.email_personal,
                                sexo: e.sexo,
                                fecha_nacimiento: fec_nacimiento,
                                telefono_movil: e.telefono_movil,
                                telefono_movil_alterno: e.telefono_movil_alterno,
                                direccion_residencia: e.direccion_residencia,
                                referido: (e.referido) ? e.referido.nombre : '',
                                puesto_votacion: (e.puesto_votacion) ? e.puesto_votacion.nombre : '',
                                municipio: (e.puesto_votacion) ? e.puesto_votacion.municipio : '',
                                localidad: (e.puesto_votacion) ? (e.puesto_votacion.localidad) ? e.puesto_votacion.localidad.nombre : '' : ''  
                            });
                            break;
                        case 'representantes':
                            csv.push({
                                nombre: e.nombre,
                                documento: e.documento, 
                                telefono_movil: e.telefono_movil,
                                telefono_movil_alterno: e.telefono_movil_alterno,
                                direccion_residencia: e.direccion_residencia,
                                referido: (e.referido) ? e.referido.nombre : '',                                
                                potencial: e.potencial_declarado_urna,
                                puesto_votacion: e.puesto_votacion.nombre,
                                municipio: e.puesto_votacion.municipio,
                                localidad: (e.puesto_votacion.localidad) ? e.puesto_votacion.localidad.nombre : ''  
                            });
                        break;
                        case 'puestosdevotacion':
                            csv.push({
                                localidad: 
                                nombre,
                                barrio: e.barrio,
                                nombre: e.nombre,
                                direccion: e.direccion,
                            });
                        break;
                    }
                });

                if(tokenAcces){
                    return res.csv(csv);    
                }else{
                    return res.render('personajes/alert');
                }
                
            });
            
            return;
        }        

        model.lean().exec((err, found) => {            
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);                
                return res.redirect('/admin');
            }

            switch (req.params.path) {
                case 'personajes':
                    let personajes = [];               

                    _.each(found, (personaje) => {
                        let referido = (personaje.referido ? personaje.referido.nombre : '');
                        let localidad = (personaje.localidad ? personaje.localidad.nombre : '');
                        let color = (personaje.color ? personaje.color : 'default');
                        let imagen = (personaje.foto_perfil ? '/uploads/'+personaje.foto_perfil : '/portraits/default.png');
                        imagen = "'"+imagen+"'";

                        personajes.push({
                            _id: personaje._id,
                            nombre: '<div class="color-'+color+' verImagen" onclick="verImagen('+imagen+');" style="cursor: pointer">&nbsp;'+personaje.nombre+'</div>',
                            descripcion: (personaje.descripcion ? personaje.descripcion : 'Sin Descripcion'),
                            referido: referido,
                            localidad: localidad,
                            fecha_nacimiento: moment(personaje.fecha_nacimiento).format("YYYY-MM-DD"),
                            items:'<span class="badge badge-success"><i class="fa fa-comment"></i> 1</span>&nbsp;'+
                                  '<span class="badge badge-primary"><i class="wb-clipboard"></i> 1</span>&nbsp;'+
                                  '<span class="badge badge-warning"><i class="wb-plugin"></i> 1</span>',
                            buttons: '<div>'+
                                      '<a href="javascript:void(0)" class="btn btn-sm btn-dark btn-icon btn-outline btn-round" onclick="fnInfo(this)" title="Datos contacto" data-toolbar="'+personaje._id+'" >'+
                                        '<i class="icon fa-phone" aria-hidden="true"></i>'+
                                      '</a>'+
                                      '<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-icon btn-outline btn-round detalle" onclick="fnDetalle(this)" data-toolbar="'+personaje._id+'" title="Ver Detalle" rol="tooltip" >'+
                                        '<i class="icon wb-eye" ></i>'+
                                      '</a>'+
                                      '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round editar" onclick="fnEditar(this)" data-toolbar="'+personaje._id+'" title="Editar Personaje" rol="tooltip" >'+
                                        '<i class="icon wb-pencil" aria-hidden="true"></i>'+
                                      '</a>'+
                                    '</div>'
                        });
                    });

                    if(tokenAcces){
                        return res.json({"data" :personajes});
                    }else{
                        return res.render('personajes/alert');
                    }
                    return;
                break;
                case 'representantes':
                    let representantes = [];               

                    _.each(found, (representante) => {
                        let divAsesor = '';
                        if(representante.asesor){
                            divAsesor = '<div class="dropAjax" data-personaje="'+representante._id+'">'+representante.asesor.name+'</div>';
                        }else{
                            divAsesor = '<select class="form-control selectAjax addAjax" data-personaje="'+representante._id+'" name="asesor" data-model="usuarios"></select>';
                        }

                        let referido = (representante.referido ? representante.referido.nombre : '');
                        let localidad = (representante.localidad ? representante.localidad.nombre : '');
                        let color = (representante.color ? representante.color : 'default');
                        let imagen = (representante.foto_perfil ? '/uploads/'+representante.foto_perfil : '/portraits/default.png');
                        
                        representante.foto_perfil = '<div class="color-'+color+' "><img class="d-flex mr-3 lazy" onclick="verImagen(this);" width="50" data-src="'+imagen+'" src="'+imagen+'" ></div>';
                        representante.referido = referido;
                        representante.localidad = localidad;
                        representante.asesor = divAsesor;
                        representante.buttons = '<div>'+
                                      '<a href="javascript:void(0)" class="btn btn-sm btn-dark btn-icon btn-outline btn-round info" title="Datos contacto" data-toolbar="'+representante._id+'" >'+
                                        '<i class="icon fa-phone" aria-hidden="true"></i>'+
                                      '</a>'+
                                      '<a href="/api/userAdd/'+representante._id+'" title="Crear Usuario Lider" rol="tooltip">'+
                                          '<button class="btn btn-warning btn-icon btn-outline btn-round">'+
                                            '<i class="icon wb-user-add" aria-hidden="true"></i>'+
                                          '</button>'+
                                      '</a> '+
                                      '<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-icon btn-outline btn-round detalle" data-toolbar="'+representante._id+'" title="Ver Detalle" rol="tooltip" >'+
                                        '<i class="icon wb-eye" ></i>'+
                                      '</a>'+
                                      '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round editar" data-toolbar="'+representante._id+'" title="Editar Personaje" rol="tooltip" >'+
                                        '<i class="icon wb-pencil" aria-hidden="true"></i>'+
                                      '</a>'+
                                    '</div>';

                        representantes.push(representante);
                    });

                    return res.json({"data" :representantes});
                    return;
                break;
                case 'hojasdevida':
                    let hojasvida = [];               

                    _.each(found, (hojasdevida) => {
                        if(!hojasdevida.referido){
                            hojasdevida['referido'] = {
                                nombre : ''
                            };
                        }
                        hojasdevida.fecha_entregada = moment(hojasdevida.fecha_entregada).format("YYYY-MM-DD");
                        hojasdevida.prioridad ='<div class="example"><div class="rating" data-score="'+hojasdevida.prioridad+'" data-number="5" data-read-only="true" data-plugin="rating"></div></div>';
                        hojasdevida.buttons = '<div>'+                                  
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-icon btn-outline btn-round detalle" data-toolbar="'+hojasdevida._id+'" title="Ver Hoja de Vida" rol="tooltip" >'+
                                    '<i class="icon wb-eye" ></i>'+
                                  '</a>'+
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round editar" data-toolbar="'+hojasdevida._id+'" title="Editar Hoja de Vida" rol="tooltip" >'+
                                    '<i class="icon wb-pencil" aria-hidden="true"></i>'+
                                  '</a>'+
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round eliminar" title="Eliminar Hoja de Vida" data-toolbar="'+hojasdevida._id+'" >'+
                                    '<i class="icon wb-close" aria-hidden="true"></i>'+
                                  '</a>'+
                                '</div>'

                        hojasvida.push(hojasdevida);
                    }); 

                    return res.json({"data" :hojasvida});
                    return;
                break;
                case 'entidades':
                    let entidades = [];               

                    _.each(found, (entidad) => {
                        entidad.createdAt = moment(entidad.createdAt).format("YYYY-MM-DD");
                        entidad.buttons = '<div>'+                                  
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round editar" data-toolbar="'+entidad._id+'" title="Editar Entidad" rol="tooltip" >'+
                                    '<i class="icon wb-pencil" aria-hidden="true"></i>'+
                                  '</a>'+
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round eliminar" title="Eliminar Entidad" data-toolbar="'+entidad._id+'" >'+
                                    '<i class="icon wb-close" aria-hidden="true"></i>'+
                                  '</a>'+
                                '</div>'

                        entidades.push(entidad);
                    }); 

                    return res.json({"data" :entidades});
                    return;
                break;
                case 'grupos':
                    let grupos = [];               

                    _.each(found, (grupo) => {
                        grupo.createdAt = moment(grupo.createdAt).format("YYYY-MM-DD");
                        grupo.buttons = '<div>'+                                  
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round editar" data-toolbar="'+grupo._id+'" title="Editar Grupo" rol="tooltip" >'+
                                    '<i class="icon wb-pencil" aria-hidden="true"></i>'+
                                  '</a>'+
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round eliminar" title="Eliminar Entidad" data-toolbar="'+grupo._id+'" >'+
                                    '<i class="icon wb-close" aria-hidden="true"></i>'+
                                  '</a>'+
                                '</div>'

                        grupos.push(grupo);
                    }); 

                    return res.json({"data" :grupos});
                break;
                case 'puestosdevotacion':
                    let puestosdevotacion = [];               

                    _.each(found, (puesto) => {
                        puesto.partido = 0;
                        _.each(puesto.potencial, (potencial) => {
                            if(potencial.partido == 'PARTIDO CAMBIO RADICAL'){
                                puesto.partido = potencial.total;
                            }                            
                        });
                        puesto.registrados = _.size(puesto.registrados);
                        puesto.votos = (puesto.votos ? puesto.votos : 0);
                        puesto.potencial = _.round((puesto.partido * 100) / puesto.votos,2);
                        puesto.buttons = '<div>'+
                                  '<a href="/admin/puestosdevotacion/'+puesto._id+'" class="btn btn-sm btn-primary btn-icon btn-outline btn-round detalle" data-toolbar="'+puesto._id+'" title="Ver Info Puesto" rol="tooltip" >'+
                                    '<i class="icon wb-eye" ></i>'+
                                  '</a>'+
                                  '<a href="/admin/puestosdevotacion/'+puesto._id+'/edit" class="btn btn-sm btn-default btn-icon btn-outline btn-round editar" title="Editar Puesto" rol="tooltip" >'+
                                    '<i class="icon wb-pencil" aria-hidden="true"></i>'+
                                  '</a>'+
                                '</div>'

                        puestosdevotacion.push(puesto);
                    }); 

                    return res.json({"data" :puestosdevotacion});
                    return;
                break;
                case 'usuarios':
                    let usuarios = [];               

                    _.each(found, (user) => {
                        user.createdAt = moment(user.createdAt).format("YYYY-MM-DD");
                        user.estado = (user.active ? 'ACTIVO' : 'INACTIVO');
                        user.buttons = '<div>'+
                                    '<a href="javascript:void(0)" class="tokenUser" data-user="'+user._id+'" role="button" data-toggle="modal" data-target="#tokenModal" title="Autorizar" rol="tooltip">'+
                                      '<button class="btn btn-success btn-icon btn-outline btn-round">'+
                                        '<i class="icon wb-thumb-up" aria-hidden="true"></i>'+
                                      '</button>'+
                                    '</a>'+
                                    '<a href="usuarios/'+user._id+'/edit" title="Editar" rol="tooltip">'+
                                      '<button class="btn btn-default btn-icon btn-outline btn-round">'+
                                        '<i class="icon wb-pencil" aria-hidden="true"></i>'+
                                      '</button>'+
                                    '</a>'+
                                    '<a href="usuarios/'+user._id+'/inactivo" title="Inactivar" rol="tooltip">'+
                                      '<button class="btn btn-danger btn-icon btn-outline btn-round">'+
                                        '<i class="icon wb-close" aria-hidden="true"></i>'+
                                      '</button>'+
                                    '</a>'+
                                    '<a href="usuarios/'+user._id+'/reset" title="Reiniciar Pass" rol="tooltip">'+
                                      '<button class="btn btn-primary btn-icon btn-outline btn-round">'+
                                        '<i class="icon wb-loop" aria-hidden="true"></i>'+
                                      '</button>'+
                                    '</a>'+
                                '</div>';

                        usuarios.push(user);
                    }); 

                    return res.json({"data" :usuarios});
                    return;
                break;
                case 'ofertas':
                    let ofertas = [];               

                    _.each(found, (oferta) => {
                        oferta.fecha_inicio = moment(user.fecha_inicio).format("YYYY-MM-DD");
                        oferta.fecha_fin = moment(user.fecha_fin).format("YYYY-MM-DD");
                        oferta.buttons = '<div>'+                                  
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-icon btn-outline btn-round detalle" data-toolbar="'+oferta._id+'" title="Ver Hoja de Vida" rol="tooltip" >'+
                                    '<i class="icon wb-eye" ></i>'+
                                  '</a>'+
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round editar" data-toolbar="'+oferta._id+'" title="Editar Hoja de Vida" rol="tooltip" >'+
                                    '<i class="icon wb-pencil" aria-hidden="true"></i>'+
                                  '</a>'+
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round eliminar" title="Eliminar Hoja de Vida" data-toolbar="'+oferta._id+'" >'+
                                    '<i class="icon wb-close" aria-hidden="true"></i>'+
                                  '</a>'+
                                '</div>';

                        ofertas.push(oferta);
                    }); 

                    return res.json({"data" :ofertas});
                    return;
                break;
            }
            
            return res.json({"data" : response});
        });
    });

    // NEW
     route.get('/:path/new', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new
        extra.user = global.user;
        switch (req.params.path) {
            case 'personajes':
                return require('./actions/personajes/new_get')(req, res, extra);
        }

        res.render(req.params.path + '/new', extra);
    }); 
    
    route.get('/:path/pre-map', auth, (req, res) => {
        res.render(req.params.path + '/pre_map', {layout: 'layouts/empty'});
    });

    route.get('/:path/map', auth, (req, res) => {

        switch (req.params.path) {
            case 'localidades':
                require('./actions/localidades/map_get')(req, res);
                return;
                break;
            case 'puestosdevotacion':
                require('./actions/puestosdevotacion/map_get')(req, res);
                return;
                break;
        }
        res.render(req.params.path + '/map', { user: global.user, layout: 'layouts/empty'});
    });

    // DELETE
    route.get('/:path/:id/delete', auth, (req, res) => {
        let extra = {};
        extra.user = global.user;
        res.render(req.params.path + '/delete', extra);
    });

    route.post('/:path/:id/delete', auth, (req, res) => {
        if (!req.body.eliminar) {
            res.cookie('mensaje','error|Confirme si desea eliminar el registro');
            return res.redirect(req.header('Referer') || '/');
        }

        switch (req.params.path) {
            case 'organizaciones':
                return require('./actions/organizaciones/delete_post')(req, res);
            case 'ofertas':
                return require('./actions/ofertas/delete_post')(req, res);
        }

        let model = global.getModel(req.params.path);
        if (!model){
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect('/admin')
        };
        
        model.findOne({ _id: req.params.id }).exec((error, record) => {
            if (error) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                return res.redirect('/');
            }else if (!record) {
                res.cookie('mensaje','error|Datos no encontrados');
                return res.redirect('/');
            } else {
                record.delete(mongoose.Types.ObjectId(global.user._id), (err) => {
                    if (err) {
                        res.cookie('mensaje','error|Error Transacción : '+err.message);
                        return res.redirect(req.header('Referer') || '/');
                    } else {
                        res.cookie('mensaje','success|Registro Eliminado');
                        return res.redirect('/admin/' + req.params.path);
                    }
                })
            }
        });
    });

    route.get('/:path/:id/inactivo', auth, (req, res) => {

        let model = global.getModel(req.params.path);
        if (!model) {
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect('/admin');
        }
        
         model.findOne({ _id: req.params.id }).exec((err, collection) => {
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                extra.user = global.user;
                return res.redirect(req.header('Referer') || '/', extra);
            }

            if (collection) {
                collection.active = false;
                collection.save((err) => {
                    if (err) {
                        res.cookie('mensaje','error|Error Transacción : '+err.message);
                        extra.user = global.user;
                        return res.redirect(req.header('Referer') || '/', extra);
                    }
                    res.cookie('mensaje','success|Registro Inactivado');
                    res.redirect('/admin/' + req.params.path);
                });
            }
        });
    });

    // RESTORE
    route.get('/:path/:id/restore', auth, (req, res) => {
        let extra = {};
        extra.user = global.user;    
        res.render(req.params.path + '/restore', extra);
    });

    route.post('/:path/:id/restore', auth, (req, res) => {
        if (!req.body.restore) {
            res.cookie('mensaje','warning|Debe seleccionar si esta seguro de realizar la acción');
            return res.redirect(req.header('Referer') || '/');
        }

        let model = global.getModel(req.params.path);
        if (!model){
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect('/admin');
        }

        model.findOne({ _id: req.params.id }).exec((error, record) => {
            if (!error) {
                record.restore((err) => {
                    if (err) {
                        res.cookie('mensaje','error|Error Transacción : '+err.message);
                        extra.user = global.user;
                        return res.redirect(req.header('Referer') || '/', extra);
                    } else {
                        res.cookie('mensaje','success|Registro reestaurado');
                        return res.redirect('/admin/' + req.params.path);
                    }
                })
            } else {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                extra.user = global.user;
                return res.redirect(req.header('Referer') || '/', extra);
            }
        });
    });

    route.get('/:path/:id/edit', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new

        switch (req.params.path) {
            case 'personajes':
                return require('./actions/personajes/edit_get')(req, res, extra);
            case 'proyectos':
                return require('./actions/proyectos/edit_get')(req, res);
            case 'puestosdevotacion':
                return require('./actions/puestosdevotacion/edit_get')(req, res);
        }

        let model = global.getModel(req.params.path);
        if (!model) {
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect(req.header('Referer') || '/');
        }

        model = model.findOne({ _id: req.params.id });
        model = global.modelPopulate(model, req.params.path);
        model.exec((err, found) => {
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                extra.user = global.user;
                return res.redirect(req.header('Referer') || '/', extra);
            }

            if (found) {
                found.user = global.user;
                res.render(req.params.path + '/edit', found);
            } else {
                extra.user = global.user;
                res.cookie('mensaje','warning|Error Transacción : Datos no encontrados');
                res.redirect('/admin/' + req.params.path, extra); // Not found
            }
        });
    });

    route.get('/:path/:id/estructura', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new

        let model = global.getModel(req.params.path);
        model.findOne({ _id: req.params.id }, {_id: 1, nombre: 1, referidos: 1, foto_perfil:1, representante: 1}).exec((error, record) => {
            if (error) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                return res.redirect('/');
            } else {
                res.cookie('mensaje','success|Cargando estructura');
                extra.user = global.user;
                extra.referencia = record;
                extra.layout = 'layouts/empty';
                res.render(req.params.path + '/hierarchy', extra);
            }
        });
       
    });

    route.get('/:path/:id/reset', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new

        let model = global.getModel(req.params.path);
        model.findOne({ _id: req.params.id }).exec((error, record) => {
            if (error) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                return res.redirect('/');
            } else {
                let passReset = bcrypt.hashSync('bogotasoyyo');

                if(record.documento){
                    passReset = bcrypt.hashSync(record.documento);
                }
                
                record.password = passReset;
                record.save((err) => {
                    if (err) {
                        res.cookie('mensaje','error|Error Transacción : '+err.message);
                        console.log('error' + err.message);
                       
                    }
                    res.cookie('mensaje','success|Datos reiniciados');
                    extra.user = global.user;
                    return res.redirect(req.header('Referer') || '/', extra);
                });
            }
        });
       
    });

    route.post('/activateToken', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new
        bcrypt.compare(req.body.inputPass, global.user.password, function(err, validation) {
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                return res.redirect('/admin/usuarios');
            }
            
            if(!validation){
                res.cookie('mensaje','error|Error Transacción : '+validation);
                return res.redirect('/admin');   
            }else{
                let model = global.getModel('usuarios');
                model.findOne({ _id: req.body.userPass }).exec((error, collection) => {
                    if (error) {
                        res.cookie('mensaje','error|Error Transacción : '+err.message);
                        return res.redirect('/');
                    } 


                    collection.token = global.user.token;
                    collection.save((err) => {
                        if (err) {
                            res.cookie('mensaje','error|Error Transacción : '+err.message);
                            extra.user = global.user;
                            return res.redirect(req.header('Referer') || '/', extra);
                        }

                        res.cookie('mensaje','success| Acceso token asignado');
                        res.redirect('/admin/usuarios');
                    });
                });
            }
        });
       
    });

    // INSERT
    route.post('/:path/new', [auth], (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) {
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect(req.header('Referer') || '/');
        }

        req.body.creador = global.user._id;
        switch (req.params.path) {
            case 'personajes':
                //if (req.body.direccion_residencia) req.body.direccion_residencia = req.body.direccion_residencia.join(' ');
                req.body.redes_sociales = [];

                if (req.body.twitter) req.body.redes_sociales.push({ red: 'Twitter', url: req.body.twitter });
                if (req.body.facebook) req.body.redes_sociales.push({ red: 'Facebook', url: req.body.facebook });
                if (req.body.instagram) req.body.redes_sociales.push({ red: 'Instagram', url: req.body.instagram });
                if ('pic_base64' in req.body && req.body.pic_base64.length) {
                    let pic = req.body.pic_base64.replace(/^data:image\/png;base64,/, "");
                    let name = uuid() + req.body.ext;
                    let filename = './public/uploads/' + name;
                    
                    fs.writeFileSync(filename, pic, 'base64');

                    req.body.foto_perfil = name;
                }

                if (req.body.fecha_nacimiento && moment(req.body.fecha_nacimiento, 'YYYY/MM/DD').toDate() != 'Invalid Date') {
                    req.body.fecha_nacimiento = moment(req.body.fecha_nacimiento, 'YYYY/MM/DD').toDate();
                }else{
                    req.body.fecha_nacimiento = null;
                }
                 
                break;
            case 'usuarios':
                req.body.password = bcrypt.hashSync(req.body.passwd);
                if ('pic_base64' in req.body && req.body.pic_base64.length) {
                    let pic = req.body.pic_base64.replace(/^data:image\/png;base64,/, "");
                    let name = uuid() + req.body.ext;
                    let filename = './public/portraits/' + name;
                    
                    fs.writeFileSync(filename, pic, 'base64');

                    req.body.profile_pic = name;
                }
                break;
            case 'hojasdevida':
                req.body.prioridad = req.body.score;
                req.body.estado = 'Recolectada';
                break;
            case 'ofertas':
                req.body.estado = 'No Ofertada';
                req.body.ofertado = 0;
                req.body.usado = 0;
                break;
            case 'proyectos':
                break;
        }

        let newRecord = new model(req.body);

        newRecord.save((err, saved) => {
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            if (saved) {
                res.cookie('mensaje','success|Registro ingresado ');
                switch (req.params.path) {
                    case 'personajes':
                        return require('./actions/personajes/new_post')(req, res, saved);
                    case 'proyectos':
                        return require('./actions/proyectos/new_post')(req, res, saved);
                    case 'hojasdevida':
                        return require('./actions/hojasdevida/new_post')(req, res, saved);
                    case 'grupos':
                        return res.redirect('/admin/grupos');
                    default:
                        res.redirect(saved._id);
                        break;
                }
            }
        });
    });

    // UPDATE
    route.post('/:path/:id/edit', [auth], (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model){
            res.cookie('mensaje','warning|Error Transacción : Modelo No encontrado');
            return res.redirect(req.header('Referer') || '/');
        }

        req.body.creador = global.user._id;
        switch (req.params.path) {
            case 'personajes':
                req.body.redes_sociales = [];
                if (req.body.twitter) req.body.redes_sociales.push({ red: 'Twitter', url: req.body.twitter });
                if (req.body.facebook) req.body.redes_sociales.push({ red: 'Facebook', url: req.body.facebook });
                if (req.body.instagram) req.body.redes_sociales.push({ red: 'Instagram', url: req.body.instagram });
                if ('pic_base64' in req.body && req.body.pic_base64.length) {
                    let pic = req.body.pic_base64.replace(/^data:image\/png;base64,/, "");
                    let name = uuid() + req.body.ext;
                    let filename = './public/uploads/' + name;
                    
                    fs.writeFileSync(filename, pic, 'base64');

                    req.body.foto_perfil = name;
                }

                if (req.body.fecha_nacimiento && moment(req.body.fecha_nacimiento, 'YYYY/MM/DD').toDate() != 'Invalid Date') {
                    req.body.fecha_nacimiento = moment(req.body.fecha_nacimiento, 'YYYY/MM/DD').toDate();
                }else{
                    req.body.fecha_nacimiento = null;
                }
                
                break;
            case 'usuarios':
                if ('pic_base64' in req.body && req.body.pic_base64.length) {
                    let pic = req.body.pic_base64.replace(/^data:image\/png;base64,/, "");
                    let name = uuid() + req.body.ext;
                    let filename = './public/portraits/' + name;
                    
                    fs.writeFileSync(filename, pic, 'base64');

                    req.body.profile_pic = name;
                }                
                break;
            case 'hojasdevida':
                req.body.prioridad = req.body.score;
                break;
        }

        model.findOne({ _id: req.params.id }).exec((err, collection) => {
            if (err) {                
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                extra.user = global.user;                
                return res.redirect(req.header('Referer') || '/', extra);
            }

            if (collection) {
                switch (req.params.path) {
                    case 'personajes':
                        if ('file' in req &&
                            req.file &&
                            collection.foto_perfil &&
                            req.file.filename != collection.foto_perfil) {
                            // Deletes the old photo:
                            fs.unlinkSync('./public/uploads/' + collection.foto_perfil);
                        }

                        require('./actions/personajes/edit_post')(req, res, collection);
                        return;
                    case 'hojasdevida':
                        require('./actions/hojasdevida/edit_post')(req, res, collection);
                        return;
                        break;
                }
                // Guardo:
                for (var n in req.body) {
                    if (req.body[n]) collection[n] = req.body[n];
                }

                collection.save((err) => {
                    if (err) {                        
                        res.cookie('mensaje','error|Error Transacción : '+err.message);
                        extra.user = global.user;                        
                        return res.redirect(req.header('Referer') || '/', extra);
                    }

                    res.cookie('mensaje','success|Registro_Modificado');
                    switch (req.params.path) {
                        case 'ofertas':
                            return res.redirect('/admin/ofertas');
                        case 'puestosdevotacion':
                            return res.redirect('/admin/puestosdevotacion');
                        case 'hojasdevida':
                            return res.redirect('/admin/hojasdevida');
                        case 'grupos':
                            return res.redirect('/admin/grupos');
                    }

                    res.redirect('/admin/' + req.params.path + '/' + collection._id);
                });
            }
        });
    });

    route.get('/:path/tree', auth, (req, res) => {
        let extra = {};
        extra.user = global.user;
        extra.layout = 'layouts/empty';
        res.render(req.params.path + '/hierarchy', extra);
    });

    // VIEW
    route.get('/:path/:id', auth, (req, res) => {
        let extra = {};
        
        switch (req.params.path) {
            case 'personajes':
                require('./actions/personajes/view_get')(req, res); return;
                break;
            case 'usuarios':
                require('./actions/usuarios/view_get')(req, res); return;
                break;
            case 'contactos':
                require('./actions/contactos/view_get')(req, res); return;
                break;
        }

        let model = global.getModel(req.params.path);
        if (!model) {
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect(req.header('Referer') || '/');
        }

        model = model.findOne({ _id: req.params.id });
        model = global.modelPopulate(model, req.params.path);
        model.lean().exec((err, found) => {
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            if (found) {
                console.log('found');
                console.log(found);
                found.user = global.user;
                res.cookie('mensaje','success|Registro encotnrado');
                res.render(req.params.path + '/view', found);
            } else {
                res.cookie('mensaje','warning|Registro no encontrado');
                extra.user = global.user;
                res.redirect('/admin/' + req.params.path, extra); // Not found
            }
        });
    });

    route.post('/:path/:id/upload', [auth, upload.single('file')], (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) {
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect(req.header('Referer') || '/');
        }

        model.findOne({ _id: req.params.id }).exec((err, obj) => {
            if (err || !obj) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                extra.user = global.user;
                return res.redirect(req.header('Referer') || '/', extra);
            }

            let file = global.models.Upload({
                model: global.getModelName(req.params.path),
                document: obj._id,
                usuario: global.user._id,
                originalname: req.file.originalname,
                filename: req.file.filename,
                path: req.file.path,
                filesize: req.file.size,
                mimetype: req.file.mimetype
            });

            file.save((err, saved) => {
                if (err) {
                    res.cookie('mensaje','error|Error Transacción : '+err.message);
                    extra.user = global.user;
                    return res.redirect(req.header('Referer') || '/', extra);
                }

                if (!('archivos' in obj)) {
                    obj.archivos = [];
                }

                obj.archivos.push(saved._id);
                obj.save(() => {
                    return res.redirect(req.header('Referer') || '/');
                });
            });
        });
    });

    route.post('/:path/:id/comment', auth, (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) {
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect(req.header('Referer') || '/');
        }
        // Dejar comentario a un modelo
        model.findOne({ _id: req.params.id }).exec((err, obj) => {
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            let comment = global.models.Comentario({
                usuario: global.user._id,
                model: global.getModelName(req.params.path),
                referencia: obj._id,
                comentario: req.body.comentario
            });

            comment.save((err, saved) => {
                if (!err) {
                    if (!('comentarios' in obj)) {
                        obj.comentarios = [];
                    }

                    obj.comentarios.push(saved._id);
                    obj.save(() => {
                        res.cookie('mensaje','success|Comentario registrado');
                        return res.redirect(req.header('Referer') || '/');
                    });

                } else {
                    res.cookie('mensaje','error|Error Transacción : '+err.message);
                    return res.redirect(req.header('Referer') || '/');
                }
            })
        })
    });

    app.use(function(req, res, next) {
        res.locals.error_message = req.flash('error');
        next();
    });

    app.use('/admin', route);
}

global.modelPopulate = (model, path) => {
    // Populate actions
    switch (path) {
        case 'personajes':
            return model.populate('hojas_de_vida localidad organizacion referido entidad proyectos puesto_votacion puesto_votacion.localidad referidos potencial_urnas.localidad creador')
                .populate({
                    path: 'comentarios',
                    populate: { path: 'usuario' }
                })
                .populate({
                    path: 'archivos',
                    populate: { path: 'usuario' }
                })
                .populate({
                    path: 'ofertas',
                    populate: { path: 'usuario' }
                })
                .populate({
                    path: 'puesto_votacion',
                    populate: { path: 'localidad' }
                })
                .populate({
                    path: 'ofertas_usadas',
                    populate: { path: 'usuario' }
                });
            break;
        case 'representantes':
            return model.populate('hojas_de_vida asesor localidad organizacion referido entidad proyectos puesto_votacion referidos potencial_urnas.localidad')
            .populate({
                    path: 'puesto_votacion',
                    populate: { path: 'localidad' }
                });
            break;
        case 'usuarios':
            return model.populate('personajes ');                
            break;
        case 'hojasdevida':
            return model.populate('creador personaje referido');
            break;
        case 'proyectos':
            return model.populate('organizacion usuario_responsable personaje entidad tareas.responsable tareas.personaje')
                .populate({
                    path: 'comentarios',
                    populate: { path: 'usuario' }
                })
                .populate({
                    path: 'archivos',
                    populate: { path: 'usuario' }
                });
            break;
        case 'ofertas':
            return model.populate('creador personaje entidad personaje_ofertado.personaje hoja_vida_enviada.hoja_vida hoja_vida_eliminada.hoja_vida ')
                .populate({
                    path: 'hoja_vida_enviada.hoja_vida',
                    populate: { path: 'personaje' }
                })
                .populate({
                    path: 'hoja_vida_enviada.hoja_vida',
                    populate: { path: 'referido' }
                })
                .populate({
                    path: 'hoja_vida_eliminada.hoja_vida',
                    populate: { path: 'personaje' }
                })
                .populate({
                    path: 'hoja_vida_eliminada.creador',
                    populate: { path: 'usuario' }
                });
            break;
        case 'organizaciones':
            return model.populate('integrantes representantes referido');
            break;
        case 'contactos':
            return model.populate('creador');
            break;
        case 'puestosdevotacion':
            return model.populate('localidad registrados')
            .populate({
                path: 'registrados.personaje',
                populate: { path: 'personaje' }
            });
            break;
        case 'logs':
            return model.populate('user document ');
            break;
        //Candidato
        case 'ctoPersonajes':
            return model.populate('grupo hojas_de_vida asesor localidad organizacion referido entidad proyectos puesto_votacion referidos potencial_urnas.localidad')
            .populate({
                    path: 'puesto_votacion',
                    populate: { path: 'localidad' }
                });
            break;
        case 'grupos':
            return model.populate('creador ');
            break;
    }

    return model;
}