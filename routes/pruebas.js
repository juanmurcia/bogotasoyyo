"use strict";
process.env.TZ = 'Americas/Bogota';

const express = require('express');
const route = express.Router();
const jwt = require('jsonwebtoken');
const uuid = require('uuid/v4');
const fs = require('fs');
const multer = require('multer');
const async = require('async');
const acl = require('express-acl');
const moment = require('moment');
const _ = require('lodash');
const auth = require('../middleware/auth');
const checkPermission = require('../middleware/security');
const bcrypt = require('bcrypt-nodejs');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });
const puestoVotacion = require('../utils/puesto-votacion');

// File upload settings:
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads');
    },
    filename: function(req, file, cb) {
        cb(null, uuid() + '.' + file.mimetype.split('/')[1])
    }
});

const fileFilter = (req, file, cb) => {
    // To reject this file pass `false`, like so:
    //cb(null, false)
    // To accept the file pass `true`, like so:
    //cb(null, true)
    // You can always pass an error if something goes wrong:
    //cb(new Error('I don\'t have a clue!'))
    cb(null, true);
}

// `upload` middleware for file uploads.
const upload = multer({ storage: storage, fileFilter: fileFilter });
module.exports = (app, mongoose) => {

    route.get('/', auth, (req, res) => {
        console.log('Estado de Pruebas');
    });

    route.get('/:path', auth, (req, res) => {
        let extra = {};
        extra.user = global.user;

        let model = global.getModel(req.params.path);
        if (!model) {
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect('/admin');
        };

        return res.render(req.params.path+'/up', extra);
    });

    //Insert 
    route.get('/puestosdevotacion/refresh', auth, (req, res) => {
        global.models.PuestoVotacion.deleteMany({municipio:'BOGOTA. D.C.'}).exec((err, puestos) => {
            console.log(err);
        });

        global.models.Localidad.deleteMany({nombre:'Ciudad Bolivar'}).exec((err, puestos) => {
            console.log(err);
        });

        global.models.Localidad.deleteMany({nombre:'Engativa'}).exec((err, puestos) => {
            console.log(err);
        });

        global.models.Localidad.deleteMany({nombre:'Fontibon'}).exec((err, puestos) => {
            console.log(err);
        });

        global.models.Localidad.deleteMany({nombre:'Los Martires'}).exec((err, puestos) => {
            console.log(err);
        });

        global.models.Localidad.deleteMany({nombre:'San Cristobal'}).exec((err, puestos) => {
            console.log(err);
        });

        res.render('puestosdevotacion/list', extra);
    });

    route.get('/:path/new', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new
        extra.user = global.user;
        res.render(req.params.path + '/new', extra);
    });

    route.post('/:path/new', [auth], (req, res) => {
        let model = global.getModel(req.params.path);
        let datos = req.body.data;        
        let asyncArray = [];
        let query = {};
        
        switch (req.params.path) {
            case 'puestosdevotacion':
                query = { $and: [{nombre: datos.nombre}, {municipio:'BOGOTA. D.C.'}] };
                asyncArray.push((callback) => {
                    global.models.Localidad.findOne({ nombre: new RegExp(datos.localidad, 'ig') }).lean().exec((err, localidad) => {
                        if (!localidad) {
                            console.log('Crear Localidad');
                            let localidad_new = {nombre: datos.localidad};
                            let new_localidad = new global.models.Localidad(localidad_new);

                            new_localidad.save((err, local) => {
                                if(err){
                                    console.log('error al crear localidad');
                                    console.log(err);
                                }
                                datos.localidad = local._id;
                                req.body.data = datos;
                            });

                        } else {
                            console.log('Find Localidad');
                            datos.localidad = localidad._id;
                            req.body.data = datos;
                        } 

                        callback(null, localidad);                     
                    })
                });

                break;
            case 'personajes':
                datos.color = 'primary';
                if(datos.documento){
                    query = {documento: datos.documento};                    
                }else{
                    query = { nombre: new RegExp(datos.nombre, 'ig') };
                }

                asyncArray.push((callback) => {
                    if(datos.documento){
                        datos.documento = datos.documento;
                    }else{
                        datos.documento = _.random(0, 1000) + _.random(0, 3000) + _.random(0, 20000);
                    }
                    console.log('datos.documento');
                    console.log(datos.documento);
                    req.body.data = datos;

                    callback(null, datos.documento);                     
                });

                asyncArray.push((callback) => {
                    global.models.Personaje.findOne({ nombre: new RegExp(datos.referido, 'ig') }).lean().exec((err, referido) => {
                        if (!referido) {
                            console.log('Sin Referido '+datos.referido);
                            datos.referido = '';
                            req.body.data = datos;
                        } else {
                            console.log('Find Referido');
                            datos.referido = referido._id;
                            req.body.data = datos;
                        } 

                        callback(null, referido);                     
                    })
                });

                asyncArray.push((callback) => {
                    global.models.Grupo.findOne({ nombre: new RegExp(datos.grupo, 'ig') }).lean().exec((err, grupo) => {
                        if (!grupo) {
                            console.log('Crear Grupo');
                            let grupo_new = {nombre: datos.grupo, creador: global.user._id};
                            let new_grupo = new global.models.Grupo(grupo_new);

                            new_grupo.save((err, local) => {
                                if(err){
                                    console.log('error al crear Grupo');
                                    console.log(err);
                                }
                                console.log('Local');
                                console.log(local);
                                datos.grupo = local._id;
                            });

                        } else {
                            console.log('Find Grupo');
                            datos.grupo = grupo._id;
                        } 
                        req.body.datos = datos;
                        
                        callback(null, grupo);                     
                    })
                });

                break;

        }

        asyncArray.push((callback) => {
            console.log('query');
            console.log(query);
            model.findOne(query).lean().exec((err, modelfind) => {
                if (modelfind) {
                    console.log('FALSE')
                    callback(null, false);
                } else {
                    console.log('TRUE')
                    callback(null, true);
                }                         
            })
        });

        async.parallel(asyncArray, (err, data) => {
            if (err) {
                console.log('error Par');
                console.log(err);
            }
            datos = req.body.data;
            datos.creador = global.user._id;
            if(data[3]){
                let newRecord = new model(datos);
                newRecord.save((err, saved) => {
                    if (err) {
                        console.log('err Crea');
                        console.log(err);
                        return res.json('error|Error Transaccion ');
                    }
                    return res.json('success|Registro Creado '+saved.nombre);
                });    
            }else{
                return res.json('info|Registro duplicado '+datos.nombre);
            }
                          
        });

    });

    route.post('/:path/edit', [auth], (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) return res.redirect(req.header('Referer') || '/');

        let query = {};
        let asyncArray = [];
        let data = req.body.data;
        //req.body.data.creador = global.user._id;
        switch (req.params.path) {
            case 'puestosdevotacion':  
                query = { $and: [{nombre: req.body.data.nombre}, {municipio: req.body.data.municipio}] };
                break;
            case 'personajes':                  
                query = {documento: req.body.data.cedula};
                asyncArray.push((callback) => {
                    global.models.Grupo.findOne({ nombre: new RegExp(data.grupo, 'ig') }).lean().exec((err, grupo) => {
                        if (!grupo) {
                            console.log('Crear Grupo');
                            let grupo_new = {nombre: data.grupo, creador: global.user._id};
                            let new_grupo = new global.models.Grupo(grupo_new);

                            new_grupo.save((err, local) => {
                                if(err){
                                    console.log('error al crear Grupo');
                                    console.log(err);
                                }
                                data.grupo = local._id;
                            });

                        } else {
                            console.log('Find Grupo');
                            data.grupo = grupo._id;
                        } 
                        req.body.data = data;
                        
                        callback(null, grupo);                     
                    })
                });
                break;
        }


        async.parallel(asyncArray, (err, data) => {
            if (err) {
                console.log('error Par');
                console.log(err);
            }
            console.log('cedula');
            console.log(req.body.data);
            model.findOne(query).exec((err, collection) => {
                if (err) {
                    console.log('error1');
                    console.log(err);
                }

                if (collection) {
                    switch (req.params.path) {
                        case 'puestosdevotacion':
                            if(req.body.data.partido){
                                let potencial = {
                                    'partido' : req.body.data.partido, 
                                    'total': req.body.data.total,
                                };

                                if(!collection.potencial){
                                    collection.potencial = [];
                                }else{
                                    let newPotecial = [];
                                    _.each(collection.potencial, (pot) => {
                                        if(pot.partido != potencial.partido){
                                            newPotecial.push(pot);
                                        }
                                    });
                                    collection.potencial = newPotecial;
                                }
                                collection.potencial.push(potencial);
                            }
                            break;
                        case 'personajes':
                            if(req.body.data.etiquetas && req.body.data.etiquetas!=''){                            

                                if(!collection.etiquetas){
                                    collection.etiquetas = [];
                                }else{
                                    let newEtiqueta = [];
                                    _.each(collection.etiquetas, (etiqueta) => {
                                        if(etiqueta != req.body.data.etiquetas){
                                            newEtiqueta.push(req.body.data.etiquetas);
                                        }
                                    });
                                    collection.etiquetas = newEtiqueta;
                                }
                                collection.etiquetas.push(req.body.data.etiquetas);
                            }
                            if(req.body.data.descripcion && req.body.data.descripcion!=''){

                                if(!collection.descripcion){
                                    collection.descripcion = '';
                                }
                                collection.descripcion = collection.descripcion+' '+req.body.data.descripcion;
                            }
                            break;
                    }
                    // Guardo:
                    for (var n in req.body.data) {
                        if (req.body.data[n]) collection[n] = req.body.data[n];
                    }

                    collection.save((err) => {
                        if (err) {
                            console.log('error2');
                            console.log(err);
                            return res.json('error|Error Transaccion: '+req.body.data);                        
                        }

                        return res.json('success|Registro Actualizad: '+collection.nombre);
                    });
                }else{
                    return res.json('error|Registro no encontrado: '+req.body.data);                        
                }
            });
                          
        });
        
    });

    app.use(function(req, res, next) {
        res.locals.error_message = req.flash('error');
        next();
    });

    app.use('/cargues', route);
};