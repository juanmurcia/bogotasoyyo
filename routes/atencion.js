"use strict";
process.env.TZ = 'Americas/Bogota';

const express = require('express');
const route = express.Router();
const jwt = require('jsonwebtoken');
const uuid = require('uuid/v4');
const fs = require('fs');
const multer = require('multer');
const async = require('async');

const moment = require('moment');
const _ = require('lodash');
const auth = require('../middleware/auth');
const checkPermission = require('../middleware/security');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });

// File upload settings:
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads');
    },
    filename: function(req, file, cb) {
        cb(null, uuid() + '.' + file.mimetype.split('/')[1])
    }
});

const fileFilter = (req, file, cb) => {
    // To reject this file pass `false`, like so:
    //cb(null, false)
    // To accept the file pass `true`, like so:
    //cb(null, true)
    // You can always pass an error if something goes wrong:
    //cb(new Error('I don\'t have a clue!'))
    cb(null, true);
}

// `upload` middleware for file uploads.
const upload = multer({ storage: storage, fileFilter: fileFilter });

module.exports = (app, mongoose) => {

    route.get('/', auth, (req, res) => {
        let extra = {};
        extra.user = global.user;
        res.render('callcenter/new', extra);
    });

    route.post('/', [auth], (req, res) => {
        let model = global.getModel('contactos');
        if (!model){return res.redirect('/');} 

        let query = {};
        global.models.User.findOne({documento: req.body.documento}).lean().exec((err, lider) => {
            if(!lider){
                req.flash('error', 'Usuario No Encontrado');
                res.redirect(req.header('Referer'));
            }else{
                query = { creador: lider._id };        

                model = (req.query.deleted) ? model.findDeleted(query) : model.find(query);
                model = global.modelPopulate(model, 'contactos');

                let skipped = 0;
                let next_page = 2;

                if ('page' in req.query) {
                    if (!isNaN(parseInt(req.query.page))) {
                        let page = parseInt(req.query.page)
                        skipped = page * 50;
                        next_page = page + 1;
                        model = model.skip(skipped);
                    }
                }

                model.limit(50).lean().exec((err, found) => {
                    if (err) {
                        console.log(err.message);
                        req.flash('error', err.message);
                        return res.redirect('/');
                    }

                    let size = _.size(found);

                    let counter = global.getModel('contactos');
                    counter = (req.query.deleted) ? counter.countDeleted(query) : counter.count(query);
                    counter.exec((err, total) => {
                        let response = {
                            results: found,
                            query: req.query,
                            total: total,
                            max: size + skipped,
                            next_page: next_page,
                            deleted_view: (req.query.deleted)
                        };
                        
                        response.user = global.user;
                        res.render( 'callcenter/list', response);
                    });
                });
            }
        });
        
    });

    route.get('/:path', auth, (req, res) => {

        let model = global.getModel(req.params.path);
        if (!model){return res.redirect('/');} 

        let query = {};

        switch (req.params.path) {
            case 'contactos':
                query = { creador: global.user._id };
                break;        
        }

        model = (req.query.deleted) ? model.findDeleted(query) : model.find(query);
        model = global.modelPopulate(model, req.params.path);

        let skipped = 0;
        let next_page = 2;

        if ('page' in req.query) {
            if (!isNaN(parseInt(req.query.page))) {
                let page = parseInt(req.query.page)
                skipped = page * 50;
                next_page = page + 1;
                model = model.skip(skipped);
            }
        }

        model.limit(50).lean().exec((err, found) => {
            if (err) {
                console.log(err.message);
                req.flash('error', err.message);
                return res.redirect('/');
            }

            let size = _.size(found);

            let counter = global.getModel(req.params.path);
            counter = (req.query.deleted) ? counter.countDeleted(query) : counter.count(query);
            counter.exec((err, total) => {
                let response = {
                    results: found,
                    query: req.query,
                    total: total,
                    max: size + skipped,
                    next_page: next_page,
                    deleted_view: (req.query.deleted)
                };
                
                response.user = global.user;
                res.render(req.params.path + '/list', response);
            });
        });
    });

    //Insert 
    route.get('/:path/new', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new
        extra.user = global.user;
        res.render(req.params.path + '/new', extra);
    });

    route.post('/:path/new', [auth], (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) return res.redirect(req.header('Referer') || '/');

        req.body.creador = global.user._id;
        switch (req.params.path) {
            case 'contactos':
                req.body.estado_contacto = 'Registrado';
                break;
        }

        let newRecord = new model(req.body);
        newRecord.save((err, saved) => {
            if (err) {
                return res.redirect(req.header('Referer') || '/');
            }

            if (saved) {
                switch (req.params.path) {
                    case 'contactos':
                        res.redirect(req.header('Referer'));
                        break;
                    default:
                        res.redirect(saved._id);
                        break;
                }
            }
        });

    });

    app.use(function(req, res, next) {
        res.locals.error_message = req.flash('error');
        next();
    });

    app.use('/atencion', route);
};