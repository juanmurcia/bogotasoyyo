"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res, extra) => {
    async.parallel([
        (cb) => {
            global.models.Localidad.find({}, {nombre: 1}).lean().exec((err, localidades) => {
                cb(null, localidades);
            });
        },
        (cb) => {
            global.models.User.find({}, { name: 1 }).lean().exec((err, usuarios) => {
                cb(null, usuarios);
            });
        },
        (cb) => {
            global.models.Organizacion.find({}, { nombre: 1 }).lean().exec((err, organizaciones) => {
                cb(null, organizaciones);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
        }

        extra.localidades = data[0];
        extra.usuarios = data[1];
        extra.organizaciones = data[2];
        extra.etiquetas = [];
        extra.user = global.user;

        res.render(req.params.path + '/new', extra);
    });

    return true;
}