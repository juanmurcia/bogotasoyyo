"use strict";
const _ = require('lodash');
const async = require('async');
const moment = require('moment');

module.exports = (req, res) => {
    let total = 0;
    async.parallel([
        (callback) => {
            let nextDays = [];
            let today = Date.now();
            let oneday = (1000 * 60 * 60 * 24);
            let inDays = Date.now() + (oneday * 240);

            //  make an array of all the month/day combos for the next 30 days    
            for (var i = today; i < inDays; i = i + oneday) {
                let thisday = new Date(i);
                nextDays.push({
                    "m": thisday.getMonth() + 1,
                    "d": thisday.getDate()
                });
            }

            global.models.Personaje.aggregate([{
                    $project: {
                        m: { $cond: [{ $ifNull: ['$fecha_nacimiento', false] }, { $month: '$fecha_nacimiento' }, -1] },
                        d: { $cond: [{ $ifNull: ['$fecha_nacimiento', false] }, { $dayOfMonth: '$fecha_nacimiento' }, -1] },
                        nombre: "$nombre", fecha_nacimiento: "$fecha_nacimiento"
                    }
                },
                {
                    $match: {
                        $or: nextDays,
                        deleted: {
                            $ne: true
                        }
                    }
                },
                {   $sort : { 
                        fecha_nacimiento : 1 
                    } 
                }
            ]).exec((err, personajes_cumple) => {
                callback(err, personajes_cumple);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.render('home', {user: global.user});
        }

        let events = [];
        var year = 2017; 
        var actual = moment();
        _.each(data[0], (personaje) => {
            year = actual.year(); 
            if(personaje.m < actual.month()){
                year += 1;
            }
            var mes = _.padStart(personaje.m, 2, '0');
            var edad = moment(personaje.fecha_nacimiento);
            edad = actual.diff(edad,"years") + 1;

            if(personaje.m == actual.month()){
                if(personaje.d == actual.day()){
                    edad = edad - 1;
                }
            }

            events.push({
                _id: personaje._id,
                proyecto: personaje._id,
                title: '<b>'+personaje.nombre+'</b>',
                detail: '<i class="site-menu-icon fa-birthday-cake"></i>Cumple '+edad+' Años</span>',
                url: '/admin/personajes/'+personaje._id,
                start: year+'-'+mes+'-'+personaje.d,
                responsable: personaje._id,
                backgroundColor: '#70CE4A'
            })
        });

        events = _.orderBy(events, 'start', 'asc');
        console.log(events);

        return res.render('calendar_cumple', {user: global.user, events});
    })
}