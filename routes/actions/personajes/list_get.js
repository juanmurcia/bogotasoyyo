"use strict";
const async = require('async');
const _ = require('lodash');
const moment = require('moment');

module.exports = (req, res) => {
    async.parallel([
        
        (cb) => {
            let find = {
                "documento" : 1,"sexo" : 1,"nombre" : 1, "descripcion" : 1, "foto_perfil" : 1, "referido" : 1, "localidad" : 1, "fecha_nacimiento" : 1, "comentarios" : 1 , 
                "ofertas" : 1, "proyectos" : 1, "direccion_residencia" : 1, "telefono_fijo.indicativo" :1, "telefono_fijo.numero" : 1, 
                "puesto_votacion" : 1, "telefono_movil" : 1, "telefono_movil_alternativo" : 1, "email_personal" : 1, "pagina_web": 1
            };
            let query = {};
            console.log('req.query');
            console.log(req.query);
            if (req.query.field && req.query.search) {
                switch (req.query.field) {
                    case 'nombre':
                        query = { nombre: new RegExp(req.query.search, 'ig') };
                        break;
                    case 'email':
                        query = { email_personal: new RegExp(req.query.search, 'ig') };
                        break;
                    case 'telefono':
                        query = {
                            $or: [
                                {telefono_movil: new RegExp(req.query.search, 'ig')},
                                {telefono_movil_alterno: new RegExp(req.query.search, 'ig')},
                                {'telefono_fijo.numero': new RegExp(req.query.search, 'ig')}
                            ]
                        };
                        break;
                    case 'documento':
                        query = { documento: parseInt(req.query.search) };
                        break;

                    case 'formacion':
                        query = {
                            $or: [
                                {'formacion.nivel_academico': new RegExp(req.query.search, 'ig')},
                                {'formacion.descripcion': new RegExp(req.query.search, 'ig')}
                            ]
                        };
                        break;
                }
            }

            if (req.query.referido) {
                query.referido = req.query.referido;
            }

            if (req.query.sinReferido == 'true') {
                query.referido = null;
            }

            if (req.query.localidad) {
                query.localidad = req.query.localidad;
            }

            console.log('===> Query <===');
            console.log(query);

            global.models.Personaje.find(query, find).lean().populate('referido creador localidad').exec((err, found_personajes) => {
                if (err) {
                    cb(err);
                }

                let personajes = [];               

                _.each(found_personajes, (personaje) => {
                    let referido = (personaje.referido ? personaje.referido.nombre : '');
                    let localidad = (personaje.localidad ? personaje.localidad.nombre : '');
                    let color = (personaje.color ? personaje.color : 'default');
                    let imagen = (personaje.foto_perfil ? '/uploads/'+personaje.foto_perfil : '/portraits/default.png');
                    imagen = "'"+imagen+"'";

                    personajes.push({
                        _id: personaje._id,
                        nombre: '<div class="color-'+color+' verImagen" onclick="verImagen('+imagen+');" style="cursor: pointer">&nbsp;'+personaje.nombre+'</div>',
                        descripcion: personaje.descripcion,
                        referido: referido,
                        localidad: localidad,
                        fecha_nacimiento: moment(personaje.fecha_nacimiento).format("YYYY-MM-DD"),
                        items:'<span class="badge badge-success"><i class="fa fa-comment"></i> 1</span>&nbsp;'+
                              '<span class="badge badge-primary"><i class="wb-clipboard"></i> 1</span>&nbsp;'+
                              '<span class="badge badge-warning"><i class="wb-plugin"></i> 1</span>',
                        buttons: '<div>'+
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-dark btn-icon btn-outline btn-round" onclick="fnInfo(this)" title="Datos contacto" data-toolbar="'+personaje._id+'" >'+
                                    '<i class="icon fa-phone" aria-hidden="true"></i>'+
                                  '</a>'+
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-icon btn-outline btn-round detalle" onclick="fnDetalle(this)" data-toolbar="'+personaje._id+'" title="Ver Detalle" rol="tooltip" >'+
                                    '<i class="icon wb-eye" ></i>'+
                                  '</a>'+
                                  '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round editar" onclick="fnEditar(this)" data-toolbar="'+personaje._id+'" title="Editar Personaje" rol="tooltip" >'+
                                    '<i class="icon wb-pencil" aria-hidden="true"></i>'+
                                  '</a>'+
                                '</div>'
                    });
                })

                cb(null, personajes);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/admin');
        }

        data = {"data": data[0]};
        return res.json(data);
    });

    return true;
}