"use strict";

module.exports = (req, res) => {
    let model = global.models.Personaje.findOne({ _id: req.params.id });
    model = global.modelPopulate(model, 'personajes');
    model.lean().exec((err, found) => {
        if (err) {
            res.cookie('mensaje','error|Error Transacción : '+err.message);
            return res.redirect('/');
        }

        if (found) {
            global.models.Proyecto.find({ personaje: found._id }).lean().exec((err, proyectos) => {
                if (proyectos) {
                    found.proyectos = proyectos;
                }
                found.user = global.user;
                res.cookie('mensaje','success|Registro Encontrado');
                res.render(req.params.path + '/view', found);
            });
        } else {
            res.cookie('mensaje','warning|Registro no encontrado');
            res.redirect('/admin/' + req.params.path); // Not found
        }
    })
}