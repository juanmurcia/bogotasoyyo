"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res) => {
    let partidos = [
        {nombre : "LIBRES"},
        {nombre : "MOVIMIENTO ALTERNATIVO INDÍGENA Y SOCIAL MAIS"},
        {nombre : "MOVIMIENTO AUTORIDADES INDÍGENAS DE COLOMBIA"},
        {nombre : "MOVIMIENTO MIRA"},
        {nombre : "MOVIMIENTO SIGNIFICATIVO DE CIUDADANOS PROGRESISTAS"},
        {nombre : "PARTIDO ALIANZA SOCIAL INDEPENDIENTE"},
        {nombre : "PARTIDO ALIANZA VERDE"},
        {nombre : "PARTIDO CAMBIO RADICAL"},
        {nombre : "PARTIDO CENTRO DEMOCRÁTICO"},
        {nombre : "PARTIDO CONSERVADOR COLOMBIANO"},
        {nombre : "PARTIDO DE LA U"},
        {nombre : "PARTIDO LIBERAL COLOMBIANO"},
        {nombre : "PARTIDO OPCIÓN CIUDADANA"},
        {nombre : "PARTIDO POLO DEMOCRÁTICO ALTERNATIVO"},
        {nombre : "PARTIDO UNIÓN PATRIÓTICA"},
        {nombre : "VOTOS EN BLANCO"},
        {nombre : "VOTOS NO MARCADOS"}
    ];

    async.parallel([
        (cb) => {
            let model = global.getModel(req.params.path);
            if (!model) {            
                res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
                return res.redirect('/admin');
            };

            model.count({ municipio: new RegExp('BOGOTA. D.C.', 'ig') }).exec((err, count) => {
                cb(null, count);
            });
        },
        (cb) => {
            let conteo = {};
            _.each(partidos, (e) => {
                conteo[e.nombre] = 0;
            });

            global.models.PuestoVotacion.find({ municipio: new RegExp('BOGOTA. D.C.', 'ig') }, {'potencial':1}).lean().exec((err, puestos) => {
                if (err) {
                    cb(err);
                }

                _.each(puestos, (puesto) => {
                    _.each(puesto.potencial, (votacion) => {
                        conteo[votacion.partido] += votacion.total;
                    });
                });

                cb(null, conteo);
            });
        },
        (cb) => {
            let conteo = {};

            global.models.PuestoVotacion.find({ municipio: new RegExp('BOGOTA. D.C.', 'ig') }, {'potencial':1, 'localidad':1}).populate('localidad').lean().exec((err, puestos) => {
                if (err) {
                    cb(err);
                }
                
                _.each(puestos, (puesto) => {
                    if(puesto.localidad){
                        if(typeof conteo[puesto.localidad.nombre] == 'undefined'){
                            conteo[puesto.localidad.nombre] = 0;
                        }
                        _.each(puesto.potencial, (votacion) => {
                            if(votacion.partido == 'PARTIDO CAMBIO RADICAL'){
                                conteo[puesto.localidad.nombre] += votacion.total;    
                            }
                        });    
                    }
                    
                });

                cb(null, conteo);
            });
        },
        (cb) => {
            let datosLocalidades = {};
            global.models.PuestoVotacion.find({ municipio: new RegExp('BOGOTA. D.C.', 'ig') }, {'potencial':1, 'localidad':1}).populate('localidad').exec((err, puestosVotacion) => {                
                _.each(puestosVotacion, (puesto) => {
                    if(puesto.localidad){
                        if(typeof datosLocalidades[puesto.localidad.nombre] == 'undefined'){
                            datosLocalidades[puesto.localidad.nombre] = {};
                        }
                        _.each(puesto.potencial, (potencial) => {
                            if(typeof datosLocalidades[puesto.localidad.nombre][potencial.partido] == 'undefined'){
                                datosLocalidades[puesto.localidad.nombre][potencial.partido] = 0;
                            }
                            
                            datosLocalidades[puesto.localidad.nombre][potencial.partido] += potencial.total;
                        });
                    }
                });

                cb(null, datosLocalidades);
            });
        },

    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/admin');
        }

        console.log('DATA');

        let total = data[0];
        let general = [];
        let partido = [];
        
        _.each(data[1], (value, label) => {
            if(value > 0){
                general.push(
                    {label : label, value : value}
                );    
            }
        });

        _.each(data[2], (value, label) => {
            if(value > 0){
                partido.push(
                    {label : label, value : value}
                );    
            }
        });

        let principales = ["PARTIDO CAMBIO RADICAL", "PARTIDO DE LA U","PARTIDO ALIANZA VERDE", "PARTIDO CONSERVADOR COLOMBIANO", "PARTIDO CENTRO DEMOCRÁTICO", "LIBRES", "PARTIDO LIBERAL COLOMBIANO"];
        let localidad = [];
        let datPartido = {};
        let serie = [];
        _.each(data[3], (value, label) => {
            localidad.push(label);

            _.each(value, (total, partidos) => {
                if(principales.includes(partidos)){
                    if(typeof datPartido[partidos] == 'undefined'){
                        datPartido[partidos] = {};
                    }

                    datPartido[partidos][label] = total;
                }else{
                    if(typeof datPartido['OTROS'] == 'undefined'){
                        datPartido['OTROS'] = {};
                    }
          
                    if(typeof datPartido['OTROS'][label] == 'undefined'){
                        datPartido['OTROS'][label] = 0;
                    }

                    datPartido['OTROS'][label] += total;
                }
                
            });
        });

        _.each(datPartido, (value, label) => {
            let total = [];
            _.each(localidad, (nombre) => {
                if(value[nombre]){
                    total.push(value[nombre]);
                }else{
                    total.push(0);
                }

            });

            serie.push({
                name: label,
                data: total
            });
        });

        res.render('puestosdevotacion/list', { total : total, general : general, partido, localidad, serie, user : global.user });
    });

    return true;
}