"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res) => {
    async.parallel([
        (cb) => {
            global.models.PuestoVotacion.find({municipio : 'BOGOTA. D.C.'}).lean().populate('localidad').exec((err, puestosdevotacion) => {
                if (err) {
                    cb(err);
                }

                cb(null, puestosdevotacion);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/admin');
        }
       
        res.render('puestosdevotacion/map', { user: global.user, results : data[0], layout: 'layouts/empty'});
    });

    return true;
}