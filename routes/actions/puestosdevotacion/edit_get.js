"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res) => {
    let extra = {};
    async.parallel([
        (cb) => {
            global.models.Localidad.find({}, {nombre: 1}).lean().exec((err, localidades) => {
                cb(null, localidades);
            });
        },
        (cb) => {
            let model = global.models.PuestoVotacion.findOne({ _id: req.params.id });
            model = global.modelPopulate(model, 'puestosdevotacion');
            model.lean().exec((err, puestos) => {
                if (puestos) {
                    cb(null, puestos);
                } else {
                    cb(true);
                }
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            res.redirect('/admin');
            return;
        }

        extra = data[1];
        extra.localidades = data[0];
        extra.user = global.user;
        res.render(req.params.path + '/edit', extra);
    });

    return true;
}