"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res) => {
    async.parallel([
        (cb) => {
            global.models.Localidad.find().lean().exec((err, localidad) => {
                if (err) {
                    cb(err);
                }

                cb(null, localidad);
            });
        },
        (cb) => {   
            global.models.Personaje.aggregate({

                $unwind : "$potencial_urnas"},{
                $group: {
                    _id : '$potencial_urnas.localidad',
                    potencial_urnas : { $sum: '$potencial_urnas.votos' }
                }        
            }, (err, potencial) => {
                if (err) {
                    cb(err);
                }

                cb(null, potencial);  
            });
        },
        (cb) => {
            global.models.PuestoVotacion.aggregate({
                $group: {
                    _id : '$localidad', 
                    urnas : { $addToSet: '$_id' }
                }        
            }, (err, potencial_real) => {
                if (err) {
                    cb(err);
                }
                let personaje_localidad = [];
                let total = _.size(potencial_real);
                _.each(potencial_real, (real) => {

                    global.models.Personaje.find({puesto_votacion : { $in: real.urnas }}).count().exec((err, count_personajes) => {
                        real.count = count_personajes;
                        personaje_localidad.push(real);
                        total = total - 1;

                        if(total == 0){
                            cb(null, personaje_localidad);
                        }
                    });                        

                    
                });                
            });
        }
    ], (err, data) => {
        if (err) {
            console.log(err);
            req.flash('error', err.message);
            return res.redirect('/admin');
        }

        let jsonLocalidad = {
            1: {nom : "Antonio Nariño", 'geo': "4.58806, -74.09729"}, 
            2: {nom : "Barrios Unidos", 'geo': "4.66976, -74.07094"}, 
            3: {nom : "Bosa", 'geo': "4.60928, -74.18501"}, 
            4: {nom : "Chapinero", 'geo': "4.64675, -74.06399"}, 
            5: {nom : "Ciudad Bolívar", 'geo': "4.5369, -74.15291"}, 
            6: {nom : "Engativá", 'geo': "4.7169, -74.11394"}, 
            7: {nom : "Fontibón", 'geo': "4.67241, -74.1445"}, 
            8: {nom : "Kennedy", 'geo': "4.63272, -74.15239"}, 
            9: {nom : "La Candelaria", 'geo': "4.59701, -74.0728766"}, 
            10: {nom : "Los Mártires", 'geo': "4.60466, -74.08558"}, 
            11: {nom : "Puente Aranda", 'geo': "4.61334, -74.10665"}, 
            12: {nom : "Rafael Uribe Uribe", 'geo': "4.58052, -74.11535"}, 
            22: {nom : "Rafael Uribe", 'geo': "4.58052, -74.11535"}, 
            13: {nom : "San Cristóbal", 'geo': "4.55666, -74.07635"}, 
            14: {nom : "Santa Fé", 'geo': "4.59371, -74.0324"}, 
            15: {nom : "Suba", 'geo': "4.74375, -74.08939"}, 
            16: {nom : "Sumapaz", 'geo': ""}, 
            17: {nom : "Teusaquillo", 'geo': "4.64145, -74.08716"}, 
            21: {nom : "teusaquillo", 'geo': "4.64145, -74.08716"}, 
            18: {nom : "Tunjuelito", 'geo': "4.56068, -74.12776"}, 
            19: {nom : "Usaquén", 'geo': "4.69431, -74.03172"}, 
            20: {nom : "Usme", 'geo': "4.47093, -74.12579"}, 
        }
        let geoLocalidad = _.values(jsonLocalidad);

        let localidades = [];
        let dataVotos = 0;
        _.each(data[0], (localidad) => {

            _.each(jsonLocalidad, (localidadGs) => {
                if(localidadGs.nom === localidad.nombre){
                    dataVotos = localidadGs.geo;
                }
            });

            localidad.geo = dataVotos;
            dataVotos = 0;
            _.each(data[1], (infoVoto) => {
                var infoVoto_id  = "'"+infoVoto._id+"'";
                var localidad_id = "'"+localidad._id+"'";

                if(infoVoto_id === localidad_id){
                    dataVotos = infoVoto.potencial_urnas;
                }
            });
            localidad.potencial = dataVotos;
            dataVotos = 0;
            _.each(data[2], (infoVoto) => {
                var infoVoto_id  = "'"+infoVoto._id+"'";
                var localidad_id = "'"+localidad._id+"'";

                if(infoVoto_id === localidad_id){
                    dataVotos = infoVoto.count;
                }
            });
            let avance = _.toNumber((dataVotos * 100) / localidad.potencial);
            console.log('avance');
            console.log(avance);
            localidad.potencial = '<div class="text-center">'+localidad.potencial+' </div>';
            localidad.personajes = '<div class="text-center">'+dataVotos+' </div>';
            localidad.avance = '<div class="text-center">'+_.round(avance, 2)+' %</div>';

            localidades.push(localidad);       
        });
        console.log('localidades');
        console.log(localidades);
        res.json({"data" :localidades});
    });

    return true;
}