"use strict";
const async = require('async');
const _ = require('lodash');
const moment = require('moment');
const columnas = {
    reunion: 'Reunión',
    gestion_entidad_publica: 'Gestión entidad Pública',
    gestion_entidad_privada: 'Gestión entidad Privada',
    gestion_educativa: 'Gestión educativa',
    hoja_de_vida: 'Tramitar Hoja de Vida',
    realizar_pago: 'Realizar pago',
    consecucion_recurso: 'Consecución Recursos Economicos',
    programar_llamada: 'Programar llamada',
    programar_reunion: 'Programar reunión',
    comercial: 'Comercial',
    compromiso_congresista: 'Compromiso congresista',
    presentacion_lideres: 'Presentación de Líder(es)'
};

module.exports = (req, res) => {
    async.parallel([
        (cb) => {

            let query = { finalizado: false, 'tareas.responsable': global.user._id};
            if(req.query.filtro){
                query = { finalizado: false, 'tareas.responsable': global.user._id , tipo : req.query.filtro};    
            }

            global.models.Proyecto.find(query).populate('personaje tareas.personaje').populate({
                path: 'tareas.comentarios',
                populate: { path: 'usuario' }
            }).sort({'tareas.fecha_fin': 1}).lean().exec((err, found_projects) => {
                if (err) {
                    cb(err);
                }

                let tareas = [];
                _.each(found_projects, (project) => {
                    if ('tareas' in project && project.tareas) {
                        _.each(project.tareas, (tarea) => {
                            if (tarea.responsable == global.user._id) {
                                tareas.push({
                                    _id: tarea._id,
                                    proyecto: project,
                                    categoria: columnas[project.tipo],
                                    titulo: tarea.titulo,
                                    descripcion: tarea.descripcion,
                                    inicio: tarea.fecha_inicio,
                                    fin: tarea.fecha_fin,
                                    comentarios: ('comentarios' in tarea) ? tarea.comentarios : [],
                                    estado: ('finalizado' in tarea) ? (tarea.finalizado  ? 'FINALIZADO' : 'PENDIENTE') : 'PENDIENTE',
                                    personaje: tarea.personaje
                                });
                            }
                        });
                    }
                });

                cb(null, tareas);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/admin');
        }

        let [tareas] = data;
        let columnas_vista = [];
        let work = [];
        for(var i in columnas) {
            let columna = {nombre: columnas[i], tareas: []};
            _.each(tareas, (tarea) => {
                if(tarea.categoria == columnas[i]) {
                    columna.tareas.push(tarea);
                    var hoy = moment();
                    var fin = moment(tarea.fin, "YYYY MM DD");
                    var dif = fin.diff(hoy,"days");
                    var background = (tarea.estado == 'PENDIENTE' ? (dif < 0 ? '#F32421' : '#FFAA14') : '#49D800');
                    var color = (tarea.estado == 'PENDIENTE' ? (dif < 0 ? 'red' : 'yellow') : 'green');
                    var personaje = (tarea.proyecto.personaje ? tarea.proyecto.personaje.nombre.nombre : '');
                    work.push({
                        _id: tarea._id,
                        proyecto: tarea.proyecto._id,
                        descripcion: tarea.descripcion,
                        estado: tarea.estado,
                        title: '<b>'+tarea.titulo+'</b>',
                        detail:
                            '<span class="badge badge-warning">Categoria: '+tarea.categoria+'</span><br>'+                            
                            '<span class="badge badge-info" style="color: black">Personaje: '+personaje+'</span><br>'+
                            '<span class="badge badge-default" style="color: black">'+tarea.estado+'</span><br>',
                        url: '/admin/proyectos/'+tarea.proyecto._id,
                        start: tarea.inicio,
                        end: tarea.fin,
                        divStart: moment(tarea.inicio).format('MMMM Do YYYY, h:mm a'),
                        divEnd: moment(tarea.fin).format('MMMM Do YYYY, h:mm a'),
                        responsable: tarea.personaje,
                        backgroundColor: background,
                        color: color
                    });
                }                
            });

            columnas_vista.push(columna);
        }
        res.render('tareas/list', { user: global.user, columnas: columnas_vista, tareas: work});
    });

    return true;
}