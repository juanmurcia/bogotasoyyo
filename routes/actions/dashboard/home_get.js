"use strict";
const _ = require('lodash');
const async = require('async');
const moment = require('moment');

module.exports = (req, res) => {
    let total = 0;
    async.parallel([
        (cb) => {
            global.models.Log.find({}).populate('user document').sort({ createdAt: 'desc' }).limit(10).lean().exec((err, logs) => {
                cb(err, logs);
            });
        },
        (cb) => {
            let potencial = {
                necesario : "35000",
                meta : "50000"
            };
            global.models.Personaje.aggregate({
                $group: {
                    _id: '',
                    potencial_declarado: { $sum: '$potencial_declarado_urna' },
                }
            }, (err, data) => {
                potencial.potencial_declarado = data[0].potencial_declarado;    

                global.models.Personaje.find().count().exec((err, personajes) => {
                    potencial.potencial_esperado = personajes;
                    cb(err, potencial);    
                });
            });
        },
        (cb) => {
            global.models.Proyecto.find({
                finalizado: false,
                'tareas.responsable': global.user._id,
                'tareas.finalizado': false,
                'tareas.fecha_inicio': {
                    $gte: moment().startOf('day').toDate(),
                    $lte: moment().endOf('day').toDate()
                }
            }).lean().exec((err, proyects) => {
                if (err) {
                    return cb(err);
                }

                let todo = [];
                _.each(proyects, (proyect) => {
                    if ('tareas' in proyect && proyect.tareas) {
                        _.each(proyect.tareas, (tarea) => {
                            var time = moment(tarea.fecha_inicio).unix();
                            var start = moment().startOf('day').unix();
                            var end = moment().endOf('day').unix();

                            tarea.proyecto_id = proyect._id;
                            if (tarea.responsable == global.user._id &&
                                time >= start && time <= end) {
                                todo.push(tarea);
                            }
                        })
                    }
                });

                cb(null, todo);
            });
        },
        (callback) => {
            let nextDays = [];
            let today = Date.now();
            let oneday = (1000 * 60 * 60 * 24);
            let inDays = Date.now() + (oneday * 7);

            //  make an array of all the month/day combos for the next 30 days    
            for (var i = today; i < inDays; i = i + oneday) {
                let thisday = new Date(i);
                nextDays.push({
                    "m": thisday.getMonth() + 1,
                    "d": thisday.getDate()
                });
            }

            global.models.Personaje.aggregate([{
                    $project: {
                        m: { $cond: [{ $ifNull: ['$fecha_nacimiento', false] }, { $month: '$fecha_nacimiento' }, -1] },
                        d: { $cond: [{ $ifNull: ['$fecha_nacimiento', false] }, { $dayOfMonth: '$fecha_nacimiento' }, -1] },
                        nombre: "$nombre", fecha_nacimiento: "$fecha_nacimiento"
                    }
                },
                {
                    $match: {
                        $or: nextDays,
                        deleted: {
                            $ne: true
                        }
                    }
                },
                {
                    $group: {
                        _id: {
                            month: "$m",
                            day: "$d",
                        },
                        personajes: { $push: { _id: "$_id", nombre: "$nombre", fecha_nacimiento: "$fecha_nacimiento" } }
                    }
                },
                {   $sort : { 
                        fecha_nacimiento : 1 
                    } 
                }
            ]).exec((err, personajes_cumple) => {
                callback(err, personajes_cumple);
            });
        },
        (cb) => {
            let potencial = {"urnas" : 0, "otro": 0};
            let err = {};

            global.models.Personaje.aggregate({
                $group: {
                    _id: '',
                    potencial_declarado: { $sum: '$potencial_declarado_urna' },
                }
            }, (err, data) => {
                 if(data){
                    potencial.urnas = data[0].potencial_declarado;
                }

                global.models.Contacto.find().count().exec((err, contactos) => {
                    let registrados = {};
                    registrados.valor = contactos;
                    registrados.porcentaje = (contactos * 100) / potencial.urnas;
                    cb(err, registrados);
                });
            });
        },
        (cb) => {
            let potencial = {"urnas" : 0, "otro": 0};
            let err = {};

            global.models.Personaje.aggregate({
                $group: {
                    _id: '',
                    potencial_declarado: { $sum: '$potencial_declarado_urna' },
                }
            }, (err, data) => {
                 if(data){
                    potencial.urnas = data[0].potencial_declarado;
                }

                global.models.Contacto.find({"estado_contacto" : "Confirmado"}).count().exec((err, contactos) => {
                    let registrados = {};
                    registrados.valor = contactos;
                    registrados.porcentaje = (contactos * 100) / potencial.urnas;
                    cb(err, registrados);
                });
            });
        },
        (cb) => {
            let potencial = {"urnas" : 0, "otro": 0};
            let err = {};

            global.models.Personaje.aggregate({
                $group: {
                    _id: '',
                    potencial_declarado: { $sum: '$potencial_declarado_urna' },
                }
            }, (err, data) => {
                 if(data){
                    potencial.urnas = data[0].potencial_declarado;
                }

                global.models.Contacto.find({"estado_contacto" : "Descartado"}).count().exec((err, contactos) => {
                    let registrados = {};
                    registrados.valor = contactos;
                    registrados.porcentaje = (contactos * 100) / potencial.urnas;
                    cb(err, registrados);
                });
            });
        }


    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.render('home', {user: global.user});
        }

        let mensaje = [];
        _.each(req.flash(), (msj, type) => {
            mensaje.type = type;
            mensaje.val = msj[0];
        });

        return res.render('home', {
            user: global.user, logs: data[0], totales: data[1], tareas: data[2],             
            personajes_cumple: data[3], registrados: data[4], confirmados: data[5], descartados: data[6], messages: mensaje.val, typeMsj : mensaje.type }
        );
    })
}