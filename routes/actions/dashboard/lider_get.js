"use strict";
const _ = require('lodash');
const async = require('async');
const moment = require('moment');

module.exports = (req, res) => {
    let total = 0;
    async.parallel([
        (cb) => {
            global.models.Log.find({}).populate('user document').sort({ createdAt: 'desc' }).limit(10).lean().exec((err, logs) => {
                cb(err, logs);
            });
        },
        (cb) => {
            let potencial = {"urnas" : 0, "otro": 0};
            let err = {};

            global.models.Personaje.findOne({"_id" : global.user.personaje},{"potencial_declarado_urna":1, "potencial_declarado_otro": 1}).lean().exec((err, lider) => {                
                if(lider){
                    potencial.urnas = lider.potencial_declarado_urna;
                    potencial.otro = lider.potencial_declarado_otro;    
                }

                global.models.Contacto.find({"creador" : global.user._id}).count().exec((err, contactos) => {
                    let registrados = {};
                    registrados.valor = contactos;
                    registrados.porcentaje = (contactos * 100) / potencial.urnas;
                    cb(err, registrados);
                });
            });           
        },
        (cb) => {
            let potencial = {"urnas" : 0, "otro": 0};
            let err = {};

            global.models.Personaje.findOne({"_id" : global.user.personaje},{"potencial_declarado_urna":1, "potencial_declarado_otro": 1}).lean().exec((err, lider) => {                
                if(lider){
                    potencial.urnas = lider.potencial_declarado_urna;
                    potencial.otro = lider.potencial_declarado_otro;    
                }

                global.models.Contacto.find({"creador" : global.user._id, "estado_contacto" : "Confirmado"}).count().exec((err, contactos) => {                    
                    let confirmados = {};
                    confirmados.valor = contactos;
                    confirmados.porcentaje = (contactos * 100) / potencial.urnas;
                    cb(err, confirmados);
                });
            });           
        },
        (cb) => {
            let potencial = {"urnas" : 0, "otro": 0};
            let err = {};

            global.models.Personaje.findOne({"_id" : global.user.personaje},{"potencial_declarado_urna":1, "potencial_declarado_otro": 1}).lean().exec((err, lider) => {                
                if(lider){
                    potencial.urnas = lider.potencial_declarado_urna;
                    potencial.otro = lider.potencial_declarado_otro;    
                }

                global.models.Contacto.find({"creador" : global.user._id, "estado_contacto" : "Descartado"}).count().exec((err, contactos) => {
                    let descartados = {};
                    descartados.valor = contactos;
                    descartados.porcentaje = (contactos * 100) / potencial.urnas;
                    cb(err, descartados);
                });
            });           
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.render('lider', {user: global.user});
        }
        return res.render('lider', {user: global.user, logs: data[0], registrados: data[1], confirmados: data[2], descartados: data[3] });
    })
}