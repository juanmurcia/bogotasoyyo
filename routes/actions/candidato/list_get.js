"use strict";
const _ = require('lodash');
const async = require('async');
const moment = require('moment');

module.exports = (req, res) => {
    let total = 0;
    async.parallel([
        (cb) => {
            let potencial = {
                necesario : "25000",
                meta : "50000"
            };
            global.models.Personaje.aggregate({
                $group: {
                    _id: '',
                    potencial_declarado: { $sum: '$potencial_declarado_urna' },
                    potencial_esperado: { $sum: '$potencial_esperado' },
                }
            }, (err, data) => {
                potencial.potencial_declarado = data[0].potencial_declarado;    
                potencial.potencial_esperado = data[0].potencial_esperado;    

                global.models.Personaje.count().exec((err, personajes) => {
                    potencial.potencial_real = personajes;
                    potencial.porcentaje = _.round((personajes * 100) / potencial.potencial_esperado,2);
                    cb(err, potencial);    
                });
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.render('home', {user: global.user});
        }

        return res.render('candidato/list', {user: global.user, totales: data[0]});
    })
}