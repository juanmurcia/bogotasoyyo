"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res) => {
    // Hago busqueda en diferentes modelos, unifico el restultado
    // y la vista lo muestra..
    async.parallel([
        (cb) => {
            if(isNaN(req.query.text)){
                global.models.Personaje.find({
                    $text: {$search: req.query.text}
                }, {
                    nombre: 1,
                    score: {$meta: "textScore"}
                }).sort({score:{$meta:"textScore"}}).exec((err, personajes) => {
                    cb(err, personajes);
                });
            }else{
                global.models.Personaje.find({
                    documento :  req.query.text
                }, {
                    nombre: 1,
                    score: {$meta: "textScore"}
                }).sort({score:{$meta:"textScore"}}).exec((err, personajes) => {
                    cb(err, personajes);
                });
            }
            
        },
        (cb) => {
            global.models.Comentario.find({
                $text: {$search: req.query.text}
            }, {
                comentario: 1,
                referencia: 1,
                model: 1,
                score: {$meta: "textScore"}
            }).sort({score:{$meta:"textScore"}}).exec((err, comentarios) => {
                cb(err, comentarios);
            });
        },
        (cb) => {
            global.models.Oferta.find({
                $text: {$search: req.query.text}
            }, {
                tipo: 1,
                descripcion: 1,
                score: {$meta: "textScore"}
            }).sort({score:{$meta:"textScore"}}).exec((err, ofertas) => {
                cb(err, ofertas);
            });
        },
        (cb) => {
            global.models.Proyecto.find({
                $text: {$search: req.query.text}
            }, {
                titulo: 1,
                score: {$meta: "textScore"}
            }).sort({score:{$meta:"textScore"}}).exec((err, proyectos) => {
                cb(err, proyectos);
            });
        },
        (cb) => {
            global.models.User.find({
                $text: {$search: req.query.text}
            }, {
                titulo: 1,
                score: {$meta: "textScore"}
            }).sort({score:{$meta:"textScore"}}).exec((err, usuarios) => {
                cb(err, usuarios);
            });
        },
        (cb) => {
            if(isNaN(req.query.text)){
                global.models.Personaje.findDeleted({
                    $text: {$search: req.query.text}
                }, {
                    nombre: 1,
                    score: {$meta: "textScore"}
                }).sort({score:{$meta:"textScore"}}).exec((err, personajesDel) => {
                    cb(err, personajesDel);
                });
            }else{
                global.models.Personaje.findDeleted({
                    documento :  req.query.text
                }, {
                    nombre: 1,
                    score: {$meta: "textScore"}
                }).sort({score:{$meta:"textScore"}}).exec((err, personajesDel) => {
                    cb(err, personajesDel);
                });
            }
        },
    ], (err, data) => {
        if (err) {
            console.log(err);
            req.flash('error', err.message);
            return res.redirect('/admin');
        }

        let [personajes, comentarios, ofertas, proyectos, usuarios, personajesDel] = data;
        res.render('search', { personajes, comentarios, ofertas, proyectos, usuarios, personajesDel });
    });

    return true;
}