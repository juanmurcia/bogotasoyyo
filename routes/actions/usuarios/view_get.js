"use strict";
const _ = require('lodash');
const async = require('async');
const moment = require('moment');

module.exports = (req, res) => {
    let total = 0;
    async.parallel([
        (cb) => {
            let potencial = {"urnas" : 0, "otro": 0};
            let err = {};

            global.models.User.findOne({"_id" : req.params.id}).lean().exec((err, user) => {
                global.models.Personaje.findOne({"_id" : user.personaje},{"potencial_declarado_urna":1, "potencial_declarado_otro": 1}).lean().exec((err, lider) => {                
                    if(lider){
                        potencial.urnas = lider.potencial_declarado_urna;
                        potencial.otro = lider.potencial_declarado_otro;    
                    }
                    let registrados = {};
                    registrados.potencial = potencial;

                    global.models.Contacto.find({"creador" : req.params.id}).count().exec((err, contactos) => {
                        
                        registrados.valor = contactos;
                        registrados.porcentaje = (contactos * 100) / potencial.urnas;
                        cb(err, registrados);
                    });
                });           
            });           
        },
        (cb) => {
            let potencial = {"urnas" : 0, "otro": 0};
            let err = {};

            global.models.User.findOne({"_id" : req.params.id}).lean().exec((err, user) => {
                global.models.Personaje.findOne({"_id" : user.personaje},{"potencial_declarado_urna":1, "potencial_declarado_otro": 1}).lean().exec((err, lider) => {                
                    if(lider){
                        potencial.urnas = lider.potencial_declarado_urna;
                        potencial.otro = lider.potencial_declarado_otro;    
                    }

                    global.models.Contacto.find({"creador" : global.user._id, "estado_contacto" : "Confirmado"}).count().exec((err, contactos) => {                    
                        let confirmados = {};
                        confirmados.valor = contactos;
                        confirmados.porcentaje = (contactos * 100) / potencial.urnas;
                        cb(err, confirmados);
                    });
                });           
            });           
        },
        (cb) => {
            let potencial = {"urnas" : 0, "otro": 0};
            let err = {};

            global.models.User.findOne({"_id" : req.params.id}).lean().exec((err, user) => {
                global.models.Personaje.findOne({"_id" : user.personaje},{"potencial_declarado_urna":1, "potencial_declarado_otro": 1}).lean().exec((err, lider) => {                
                    if(lider){
                        potencial.urnas = lider.potencial_declarado_urna;
                        potencial.otro = lider.potencial_declarado_otro;    
                    }

                    global.models.Contacto.find({"creador" : global.user._id, "estado_contacto" : "Descartado"}).count().exec((err, contactos) => {
                        let descartados = {};
                        descartados.valor = contactos;
                        descartados.porcentaje = (contactos * 100) / potencial.urnas;
                        cb(err, descartados);
                    });
                });           
            });             
        },
        (cb) => {
            global.models.User.findOne({"_id" : req.params.id}).lean().exec((err, user) => {
                cb(err, user);
            });             
        }
    ], (err, data) => {
        if (err) {
            res.cookie('mensaje','error|Error Transacción : '+err.message);
            return res.redirect(req.header('Referer') || '/');
        }
        res.cookie('mensaje','success|Registro encontrado');
        return res.render(req.params.path + '/view', {user: global.user, found: data[3], registrados: data[0], confirmados: data[1], descartados: data[2]});
    })
}