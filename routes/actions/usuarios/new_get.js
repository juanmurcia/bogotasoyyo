"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res, extra) => {
    async.parallel([
        (cb) => {

        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
        }

        extra.profile = [{"_id" : "1", "nombre" : "Administrador"}];
        res.render(req.params.path + '/new', extra);
    });

    return true;
}