"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res) => {
    async.parallel([
        (cb) => {
            global.models.User.find({}).lean().exec((err, _members) => {
                if (err) {
                    cb(err);
                }

                let members = [];
                _.each(_members, (member) => {
                    members.push({
                        id: member._id,
                        name: member.name,
                        avatar: member.profile_pic
                    })
                });

                cb(null, members);
            });
        },
        (cb) => {
            global.models.Organizacion.find({}, { nombre: 1 }).lean().exec((err, organizaciones) => {
                cb(null, organizaciones);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/admin');
        }

        let members = data[0];

        res.render('proyectos/new', { members, organizaciones: data[1], query: req.query });
    });

    return true;
}