"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res) => {
    async.parallel([
        (cb) => {
            global.models.Organizacion.find({}).lean().exec((err, organizaciones) => {
                if (err) {
                    cb(err);
                }

                cb(null, organizaciones);
            });
        },
        (cb) => {
            global.models.User.find({}).lean().exec((err, _members) => {
                if (err) {
                    cb(err);
                }

                let members = [];
                _.each(_members, (member) => {
                    members.push({
                        id: member._id,
                        name: member.name,
                        avatar: member.profile_pic
                    })
                });

                cb(null, members);
            });
        },
        (cb) => {
            let query = { finalizado: false };
            if ('filtro' in req.query && req.query.filtro) {
                query.tipo = req.query.filtro;
            }

            global.models.Proyecto.find(query).lean().exec((err, found_projects) => {
                if (err) {
                    cb(err);
                }

                let projects = [];
                let events = [];

                _.each(found_projects, (project) => {
                    projects.push({
                        _id: project._id,
                        titulo: project.titulo,
                        tareas: project.tareas,
                        color: project.color
                    });

                    if ('tareas' in project && project.tareas) {
                        _.each(project.tareas, (tarea) => {
                            events.push({
                                _id: tarea._id,
                                proyecto: project._id,
                                title: project.titulo + ': ' + tarea.titulo,
                                start: tarea.fecha_inicio,
                                responsable: tarea.responsable,
                                end: tarea.fecha_fin,
                                backgroundColor: project.color + '-600'
                            })
                        })
                    }
                })

                cb(null, [projects, events]);
            });
        }
    ], (err, data) => {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/admin');
        }

        let organizaciones = data[0];
        let members = data[1];

        let projects = data[2][0];
        let events = data[2][1];

        res.render('proyectos/list', { events, projects, members, organizaciones, query: req.query });
    });

    return true;
}