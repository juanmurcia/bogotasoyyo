"use strict";
const _ = require('lodash');
const async = require('async');
const moment = require('moment');

module.exports = (req, res) => {
    let total = 0;
    async.parallel([
        (cb) => {
            global.models.Contacto.find({"creador" : req.params.id}).lean().exec((err, contacto) => {                
                cb(err, contacto);
            });           
        }
    ], (err, data) => {
        if (err) {
            res.cookie('mensaje','error|Error Transacción : '+err.message);
            return res.redirect(req.header('Referer') || '/');
        }
        res.cookie('mensaje','success|Listando Contactos');
        return res.render(req.params.path + '/list', {results : data[0]});
    })
}