"use strict";
const async = require('async');
const _ = require('lodash');

module.exports = (req, res, saved) => {
    async.waterfall([
        (cb) => { // Crea el puesto de Votacion para alimentar la base
            global.models.Personaje.findOne({ _id: req.body.personaje }).lean().exec((err, personaje) => {
                if (!personaje) {
                    let error = {};
                    error.message = 'Personaje No encontrado para cargar Hoja de Vida';
                    cb(error);
                } else {
                    saved.referido = personaje.referido;
                    saved.save((err) => {
                        cb(err);
                    });
                }
            })
        }
    ], (err) => {
        if (err) {
            res.cookie('mensaje','error|Error Transacción : '+err.message);
        }
        res.cookie('mensaje','success|Registro Modificado ');
        res.redirect('/admin/hojasdevida');
    });

    return true;
};