"use strict";
process.env.TZ = 'Americas/Bogota';

const express = require('express');
const route = express.Router();
const jwt = require('jsonwebtoken');
const uuid = require('uuid/v4');
const fs = require('fs');
const multer = require('multer');
const async = require('async');
const acl = require('express-acl');
const moment = require('moment');
const _ = require('lodash');
const auth = require('../middleware/auth');
const checkPermission = require('../middleware/security');
const bcrypt = require('bcrypt-nodejs');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });
const puestoVotacion = require('../utils/puesto-votacion');

// File upload settings:
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/uploads');
    },
    filename: function(req, file, cb) {
        cb(null, uuid() + '.' + file.mimetype.split('/')[1])
    }
});

const fileFilter = (req, file, cb) => {
    // To reject this file pass `false`, like so:
    //cb(null, false)
    // To accept the file pass `true`, like so:
    //cb(null, true)
    // You can always pass an error if something goes wrong:
    //cb(new Error('I don\'t have a clue!'))
    cb(null, true);
}

// `upload` middleware for file uploads.
const upload = multer({ storage: storage, fileFilter: fileFilter });
module.exports = (app, mongoose) => {

    route.get('/', auth, (req, res) => {        
        require('./actions/dashboard/candidato_get')(req, res);
    });

    route.get('/:path', auth, (req, res) => {
        let extra = {};
        extra.user = global.user;

        let model = global.getModel(req.params.path);
        if (!model) {            
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect('/admin');
        };
        
        let path = req.params.path;
        switch (req.params.path){
            case 'ctoPersonajes' :
                require('./actions/candidato/list_get')(req, res);
                return;
            break;
        }
        model.count({}).exec((err, count) => {
            extra.total = count;            
            return res.render(path+'/list', extra);
        });
    });

    route.get('/:path/:id/detail', auth, (req, res) => {
        let extra = {};
        extra.user = global.user;

        let model = global.getModel(req.params.path);
        if (!model) {            
            res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
            return res.redirect('/admin');
        };
        
        let path = req.params.path;
        switch (req.params.path){
            case 'ctoPersonajes' :
                path = 'candidato';
            break;
        }
        model.findOne({_id : req.params.id}).exec((err, found) => {
            extra.result = found;
            extra.layout= 'layouts/empty';
            return res.render(path+'/panel', extra);
        });
    });

    route.get('/:path/list', auth, (req, res) => {

        let model = global.getModel(req.params.path);
        if (!model){return res.redirect('/');} 

        let query = {};

        switch (req.params.path) {
            case 'contactos':
                if(global.user.role == 'lider'){
                    query = { creador: global.user._id };    
                }
                break;
            case 'ctoPersonajes':
                if (req.query.nombre) {
                    query = { nombre: new RegExp(req.query.nombre, 'ig') };
                }         
                if (req.query.documento) {
                    query = { documento: parseInt(req.query.documento) };
                }

                if (req.query.referido) {
                    query.referido = req.query.referido;
                }

                if (req.query.sinReferido == 'true') {
                    query.referido = null;
                }

                if (req.query.localidad) {
                    query.localidad = req.query.localidad;
                }

                if (req.query.color) {
                    query.color = req.query.color;
                }
            break;     
        }
        model = (req.query.deleted) ? model.findDeleted(query) : model.find(query);
        model = global.modelPopulate(model, req.params.path);
        model = model.sort({'nombre': 1});

        model.lean().exec((err, found) => {
            if (err) {
                req.flash('error', err.message);
                return res.redirect('/');
            }
            switch (req.params.path) {
                case 'ctoPersonajes':
                    let personajes = [];               

                    _.each(found, (personaje) => {
                        let divGrupo = '';
                        if(personaje.grupo){
                            divGrupo = '<div class="dropAjax" data-personaje="'+personaje._id+'">'+personaje.grupo.nombre+'</div>';
                        }else{
                            divGrupo = '<select class="form-control selectAjax addAjax" data-personaje="'+personaje._id+'" name="grupo" data-model="grupos"></select>';
                        }

                        let referido = (personaje.referido ? personaje.referido.nombre : '');
                        let localidad = (personaje.localidad ? personaje.localidad.nombre : '');
                        let color = (personaje.color ? personaje.color : 'default');
                        let imagen = (personaje.foto_perfil ? '/uploads/'+personaje.foto_perfil : '/portraits/default.png');
                        imagen = "'"+imagen+"'";

                        personajes.push({
                            _id: personaje._id,
                            nombre: personaje.nombre,
                            foto: '<div class="color-'+color+' verImagen" onclick="verImagen('+imagen+');" style="cursor: pointer">&nbsp;<img class="avatar" src='+imagen+' title="'+personaje.nombre+'"></div>',
                            descripcion: (personaje.descripcion ? personaje.descripcion : 'Sin Descripcion'),
                            referido: referido,
                            grupo: divGrupo,
                            localidad: localidad,
                            buttons: '<div class="text-center">'+
                                      '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round" rol="tooltip" >'+
                                        '<i class="icon wb-menu" data-url="/candidato/ctoPersonajes/'+personaje._id+'/detail" data-toggle="slidePanel"></i>'+
                                      '</a>'+
                                    '</div>'
                        });
                    });

                    return res.json({"data" :personajes});
                break;
            }
        });
    });

    //Insert 
    route.get('/:path/new', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new
        extra.user = global.user;
        res.render(req.params.path + '/new', extra);
    });

    route.post('/:path/new', [auth], (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) {
            return res.redirect('/');
        }

        req.body.creador = global.user._id;
        switch (req.params.path) {
            case 'contactos':
                req.body.estado_contacto = 'Registrado';
                break;
        }

        let newRecord = new model(req.body);
        newRecord.save((err, saved) => {
            if (err) {
                console.log('err');
                console.log(err);
                return res.redirect('/');
            }

            console.log('Saved Contact');
            return res.redirect(req.header('Referer'));
        });

    });

    route.get('/:path/:id/edit', auth, (req, res) => {
        let extra = {}; // Se llena con modelos extra para construir los new

        switch (req.params.path) {
            
        }

        let model = global.getModel(req.params.path);
        if (!model) return res.redirect(req.header('Referer') || '/');

        model = model.findOne({ _id: req.params.id });
        model = global.modelPopulate(model, req.params.path);
        model.exec((err, found) => {
            if (err) {
                req.flash('error', err.message);
                extra.user = global.user;
                extra.error = err.message;
                return res.redirect(req.header('Referer') || '/', extra);
            }

            if (found) {
                found.user = global.user;
                res.render(req.params.path + '/edit', found);
            } else {
                extra.user = global.user;
                res.redirect('/', extra); // Not found
            }
        });
    });

    route.post('/:path/:id/edit', [auth], (req, res) => {
        let model = global.getModel(req.params.path);
        if (!model) return res.redirect(req.header('Referer') || '/');

        req.body.creador = global.user._id;
        switch (req.params.path) {
            
        }

        model.findOne({ _id: req.params.id }).exec((err, collection) => {
            if (err) {
                req.flash('error', err.message);
                extra.user = global.user;
                extra.error = err.message;
                return res.redirect(req.header('Referer') || '/', extra);
            }

            if (collection) {
                switch (req.params.path) {
                    
                }
                // Guardo:
                for (var n in req.body) {
                    if (req.body[n]) collection[n] = req.body[n];
                }

                collection.save((err) => {
                    if (err) {
                        req.flash('error', err.message);
                        extra.user = global.user;
                        extra.error = err.message;
                        return res.redirect(req.header('Referer') || '/', extra);
                    }

                    res.redirect('/lider/contactos');
                });
            }
        });
    });

    // DELETE
    route.get('/:path/:id/delete', auth, (req, res) => {
        let extra = {};
        extra.user = global.user;
        res.render(req.params.path + '/delete', extra);
    });

    route.post('/:path/:id/delete', auth, (req, res) => {
        console.log('Eliminando ...');
        if (!req.body.eliminar) {
            return res.redirect(req.header('Referer') || '/');
        }

        switch (req.params.path) {
           
        }

        let model = global.getModel(req.params.path);
        if (!model) return res.redirect('/');
        model.findOne({ _id: req.params.id }).exec((error, record) => {
            console.log('Eliminando Model ...');

            if (error) {
                console.log('error');
                console.log(error);
                return res.redirect('/');
            }

            record.remove(mongoose.Types.ObjectId(global.user._id), (err) => {
                if (err) {
                    req.flash('error', err.message);
                    console.log('err.message');
                    console.log(err);
                    return res.redirect(req.header('Referer') || '/');
                } else {
                    console.log('Eliminado');
                    return res.redirect('/lider/contactos');
                }
            })
            
        });
    });

    app.use(function(req, res, next) {
        res.locals.error_message = req.flash('error');
        next();
    });

    app.use('/candidato', route);
};