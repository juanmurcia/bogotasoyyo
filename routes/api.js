"use strict";
const express     = require('express');
const route       = express.Router();
const jwt = require('jsonwebtoken');
const _           = require('lodash');
const auth        = require('../middleware/auth');
const moment      = require('moment');
const diacritics  = require('diacritics').remove;
const bcrypt = require('bcrypt-nodejs');
const Antecedentes = require('../utils/antecedentes');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });
const PuestoVotacion = require('../utils/puesto-votacion');
const async = require('async');

module.exports = (app, mongoose) => {
  //rutas de Acceso y cierre de Seccion
  route.get('/login', csrfProtection, (req, res) => {    
    res.render('login', { csrfToken: req.csrfToken(), layout: 'layouts/login'});
  });

  route.post('/login', csrfProtection, (req, res) => {
      // find the user
      global.models.User.findOne({
          email: req.body.email,
          active: true
      }, function(err, client) {
          if (err) {
              res.cookie('mensaje','error|Error Transacción : '+err.message);
              return res.redirect('/api/login');
          }

          if (!client) {
              res.cookie('mensaje','error|Usuario no encontrado');              
              res.redirect('/api/login');
          } else {
              bcrypt.compare(req.body.password, client.password, function(err, validation) {
                  if (err) {
                    res.cookie('mensaje','error|Se encotnraron algunos problemas en la autenticación, verifique sus datos');
                    res.redirect('/api/login');
                  }

                  // check if password matches
                  if (!validation) {
                      res.cookie('mensaje','error|Se encotnraron algunos problemas en la autenticación, verifique sus datos');
                      res.redirect('/api/login');
                  } else {
                      // create a token
                      var token = jwt.sign(client, global.secret, {
                          expiresIn: (60 * 60 * 24) * 7 // expires in 24 hours
                      });
                      let passToken = bcrypt.hashSync(client.token);
                      res.cookie('jwt_auth', token, { expires: new Date(2147483647000), httpOnly: true, sameSite: true });
                      res.cookie('role',client.role, { expires: new Date(2147483647000), httpOnly: true, sameSite: true });
                      res.cookie('mms_ts', passToken, { expires: new Date(2147483647000), httpOnly: true, sameSite: true });                  
                      let path = 'admin';
                      if(client.role == 'lider') { 
                          path ='lider';
                      }
                      res.cookie('mensaje','success|Bienvenido a Bogota Soy Yo !');                    
                      res.redirect('/'+path);
                  }
              });
          }

      });
  });

  route.get('/logout', auth, (req, res) => {
      req.session.destroy(function(err) {
          res.clearCookie('jwt_auth', { httpOnly: true, sameSite: true });
          res.clearCookie('mms_ts', { httpOnly: true, sameSite: true });
          res.cookie('role','guest', { expires: new Date(2147483647000), httpOnly: true, sameSite: true });
          global.user.role = 'guest';
          res.redirect('/'); //Inside a callback… bulletproof!
      });
  });

  app.get('/forgot', csrfProtection, (req, res) => {
      res.render('forgot', { csrfToken: req.csrfToken(), layout: 'layouts/login' });
  });

  app.post('/forgot', csrfProtection, (req, res) => {
    global.models.User.findOne({ email: req.body.email }).exec((err, user) => {
        if (err) return res.redirect('/');
        user.recover_string = uuid();
        user.save((err, usuario) => {
            if (err) {
                req.flash('error', err.message);
                return res.redirect('/');
            }

            global.sendMail(
                usuario.name,
                usuario.email,
                'Define una nueva contraseña para tu cuenta',
                'forgot.handlebars', { token: usuario.recover_string },
                () => {
                    res.render('forgot_sent', { layout: 'layouts/login' });
                }
            );
        });
    });
  });

  app.get('/recover', csrfProtection, (req, res) => {
    // Busco el usuario que coincida con el link de recover:
    global.models.User.findOne({ recover_string: req.query.token }).exec((err, user) => {
        if (!err && user) {
            // Login
            res.render('recover', { csrfToken: req.csrfToken(), layout: 'layouts/login' });
        } else {
            req.flash('error', err.message);
            res.redirect('/');
        }
    });
  });

  app.post('/recover', csrfProtection, (req, res) => {
    if (req.body.password != req.body.confirm) {
        return res.redirect('/');
    }

    // Busco el usuario que coincida con el link de recover:
    global.models.User.findOne({ recover_string: req.query.token }).exec((err, user) => {
        if (!err && user) {
            // Actualizo el pass
            bcrypt.hash(req.body.password, null, null, function(err, hashed) {
                if (err) throw err;

                user.password = hashed;
                user.recover_string = null;
                user.save(() => {
                    res.redirect('/');
                });
            });
        } else {
            req.flash('error', err.message);
            res.redirect('/');
        }
    });
  });

  app.post('/cambio', auth, (req, res) => {
    global.models.User.findOne({_id: global.user._id, active: true}, function(err, usuario) {
      if (err) {
        res.cookie('mensaje','error|Error Transacción : '+err.message);
        return res.redirect('/');
      }


      bcrypt.compare(req.body.passActl, usuario.password, function(err, validation) {
        if (err) return;

        // check if password matches
        if (!validation) {
          res.cookie('mensaje','error|Error Transacción : Contraseña incorrecta');
          res.redirect('/');
        } else {

          let newPassword = bcrypt.hashSync(req.body.passNvaC);

          global.models.User.update(
            {_id: global.user._id},
            {password: newPassword}
          ).exec((err, personaje) => {
            if(err){
              res.cookie('mensaje','error|Error Transacción : '+err.message);
              return res.sendStatus(500);
            }

            res.cookie('mensaje','success|Contraseña actualizada');
            res.redirect('/api/logout');
          });
        }
      });
    });  
  });  

  /* ============== */

  route.get('/:path/countdel', (req, res) => {
    let model = global.getModel(req.params.path);
    if (!model) {            
        res.cookie('mensaje','warning|Error Transacción : Modelo no encontrado');
        return res.redirect('/admin');
    };

    model.countDeleted({}).exec((err, count) => {
      return res.json(count);
    });
  });

  route.post('/organizaciones/:id/add', (req, res) => {
    global.models.Personaje.findOne({_id: req.body._id}).exec((err, personaje) => {
      if(err || !personaje) {
        return res.sendStatus(404);
      }

      if(personaje.organizacion) {
        global.models.Organizacion.findOneAndUpdate(
          { _id: personaje.organizacion },
          { $pull: { integrantes: personaje._id }}
        ).exec((err, organizacion) => {
          global.models.Organizacion.findOneAndUpdate(
            { _id: req.params.id},
            { $addToSet: { integrantes: personaje._id } }
          ).exec((err, organizacion) => {
            if(err) {
              return res.sendStatus(500);
            }

            global.models.Personaje.update(
              {_id: req.body._id},
              {organizacion: mongoose.Types.ObjectId(req.params.id)}
            ).exec((err, personaje) => {
              if(err) return res.sendStatus(500);
              res.sendStatus(200);
            });
          });
        });
      } else {
         global.models.Organizacion.findOneAndUpdate(
            { _id: req.params.id},
            { $addToSet: { integrantes: personaje._id } }
          ).exec((err, organizacion) => {
            if(err) {
              return res.sendStatus(500);
            }

            global.models.Personaje.update(
              {_id: req.body._id},
              {organizacion: mongoose.Types.ObjectId(req.params.id)}
            ).exec((err, personaje) => {
              if(err) return res.sendStatus(500);
              res.sendStatus(200);
            });
          });
      }
    });
  });

  route.get('/personajes/:id/puesto_votacion', (req, res) => {
    global.models.Personaje.findOne({_id: req.params.id}).populate('puesto_votacion').exec((err, personaje) => {
      if(personaje) {
        Antecedentes(personaje.documento, (details) => {
          if(!details) {
            res.sendStatus(401);
            return;
          }
          PuestoVotacion(personaje.documento, (puesto) => {
            personaje.antecedentes = details.antecedentes;
            puesto.nombre = _.toUpper(puesto.nombre);
            //Validacion para recargar puestos de Votación
            if(typeof personaje.puesto_votacion != 'undefined'){
              if(!personaje.puesto_votacion){
                personaje.puesto_votacion = {
                  nombre : '-'
                };  
              }
            }

            if(typeof personaje.puesto_votacion == 'undefined' || personaje.puesto_votacion.nombre != puesto.nombre) {
              global.models.PuestoVotacion.findOne({ nombre: puesto.nombre }).lean().exec((err, existing_puesto) => {
                if (!existing_puesto) {
                  let nuevo = global.models.PuestoVotacion(puesto);
                      nuevo.save((err, saved) => {
                        if(err) {
                          return res.sendStatus(501);
                        }

                        personaje.puesto_votacion = saved._id;
                        personaje.mesa_votacion = puesto.mesa;
                        personaje.save((err) => {
                          res.sendStatus((err) ? 500 : 200);
                        });
                      });
                } else {
                  personaje.puesto_votacion = existing_puesto._id;
                  personaje.mesa_votacion = puesto.mesa;
                  personaje.save((err) => {
                    res.sendStatus((err) ? 500 : 200);
                  });
                }
              });
            } else {
              personaje.save((err) => {
                res.sendStatus((err) ? 500 : 200);
              });
            }
          });
        });
      } else {
        res.sendStatus(404);
      }
    });
  });

  route.post('/tree/personajes', (req, res) => {
    let csv = req.body.docs;

    res.csv(csv);
    return true;
  });

  route.get('/tree/personajes', (req, res) => {
    let model = global.models.Personaje;    
    let root = { result : [] };

    // Busco los children
    model.find(
      {referido: null}, 
      {_id: 1, representante: 1, documento :1, nombre: 1, referidos: 1, telefono_movil : 1, direccion_residencia : 1,email_personal : 1, foto_perfil:1, potencial_declarado_urna: 1}
    ).lean().sort({nombre : 1}).exec((err, personajes) => {
      if(err) {
        res.cookie('mensaje','error|Error Transacción : '+err.message);
        return res.sendStatus(500);
      }

      _.each(personajes, (personaje) => {
        personaje.id = personaje._id;
        personaje.hasChild = ('referidos' in personaje && personaje.referidos.length > 0);

        delete personaje._id;
        if('referidos' in personaje) delete personaje.referidos;

        root.result.push(personaje);
      });

      return res.json(root);
    });
  });

  route.get('/tree/personajes/:id', (req, res) => {
    let model = global.models.Personaje; //global.getModel(req.params.path);
    //if (!model) return res.sendStatus(500);
    let root = {
      result: []
    };

    // Busco los children
    model.find(
      {referido: req.params.id}, 
      {_id: 1, representante: 1, documento :1, nombre: 1, referidos: 1, telefono_movil : 1, direccion_residencia : 1,email_personal : 1, foto_perfil:1, potencial_declarado_urna: 1}
    ).lean().sort({nombre : 1}).exec((err, personajes) => {
      if(err) {
        res.cookie('mensaje','error|Error Transacción : '+err.message);
        return res.sendStatus(500);
      }

      _.each(personajes, (personaje) => {
        personaje.id = personaje._id;
        personaje.hasChild = ('referidos' in personaje && personaje.referidos.length > 0);

        delete personaje._id;
        if('referidos' in personaje) delete personaje.referidos;

        root.result.push(personaje);
      });

      return res.json(root);
    });
  });

  route.get('/tree/personajes/:id/detail', (req, res) => {
    let model = global.models.Personaje; //global.getModel(req.params.path);
    //if (!model) return res.sendStatus(500);
    let root = {
      result: []
    };

    // Busco los children
    model.findOne(
      {_id: req.params.id}, 
      {_id: 1, representante: 1, telefono_fijo :1, nombre: 1, telefono_movil : 1, direccion_residencia : 1,email_personal : 1}
    ).lean().sort({nombre : 1}).exec((err, personaje) => {
      if(err) {
        res.cookie('mensaje','error|Error Transacción : '+err.message);
        return res.sendStatus(500);
      }

      root.result.push(personaje);
      
      return res.json(root);
    });
  });

  route.post('/personajes/:id/referido', (req, res) => {
    let a = req.params.id;
    let b = req.body.set;

    // Elimino del que me tenga como referido
    global.models.Personaje.update
    ({referidos: a}, {$pull: { referidos: a}}, (err, num) => {
      // Modifico a con referido b
      global.models.Personaje.update({_id: a}, {$set: {referido: b}}, (err, num) => {
        // Agrego al personaje b el referido a
        if(b == '') {
          res.json({updated: num});
        } else {
          global.models.Personaje.update({_id: b}, {$push: {referidos: a}}, (err, num) => {
            res.json({updated: num});
          });
        }
      });
    });
  });

  route.post('/personajes/:id/:field', (req, res) => {
    let valid = [
      'color',
      'agregado_contactos_google',
      'enviar.sms',
      'enviar.llamadas',
      'enviar.email',
      'enviar.correspondencia',
    ];

    if(valid.indexOf(req.params.field) >= 0) {
      let set = {$set: {}};
          set['$set'][req.params.field] = req.body.set;

      global.models.Personaje.update(
        { _id: req.params.id },
        set,
        (err, numAffected) => {
          res.json({updated: numAffected});
        }
      );
    } else {
      res.json({});
    }
  });

  route.post('/ofertas/:id/addhoja', (req, res) => {

    global.models.Oferta.findOne({ _id: req.params.id }).exec((err, oferta) => {
        if (err) {
            res.cookie('mensaje','error|Error Transacción : '+err.message);
            return res.redirect(req.header('Referer') || '/');
        }

        if (!oferta) {
            res.cookie('mensaje','error|Oferta no encotrada');
            return res.redirect(req.header('Referer') || '/');
        }
        
        if (!('hoja_vida_enviada' in oferta)) {
            oferta.hoja_vida_enviada = [];
        }

        let hojaVida = {
          hoja_vida: req.body.set,
          fecha: moment().format(),
          estado: 'En proceso',
          creador: global.user._id,
        };

        oferta.hoja_vida_enviada.push(hojaVida);
        oferta.usado = oferta.usado + 1;
        
        oferta.save((err, saved) => {
            if (err) {
                res.cookie('mensaje','error|Error Transacción : '+err.message);
                return res.redirect(req.header('Referer') || '/');
            }

            global.models.HojaDeVida.findOneAndUpdate(
              {_id: req.body.set}, 
              {estado: 'Procesando'},
              (err) => {
              return res.json({err});
            });
        })
    })
  });

  route.post('/ofertas/:id/drophoja', (req, res) => {
    
    global.models.Oferta.findOne({ _id: req.params.id}).exec((err, oferta) => {
        if (err) {
          res.cookie('mensaje','error|Error Transacción : '+err.message);
          return res.redirect(req.header('Referer') || '/');
        }

        if (!oferta) {
          res.cookie('mensaje','error|Oferta no encotrada');
          return res.redirect(req.header('Referer') || '/');
        }

        if (!('hoja_vida_eliminada' in oferta)) {
          oferta.hoja_vida_eliminada = [];
        }

        let hojas = [];
        _.each(oferta.hoja_vida_enviada, (data) => {
          if(data.hoja_vida == req.body.set){            
            let hojaVida = {
              hoja_vida: req.body.set,
              fecha: moment().format(),
              creador: global.user._id,
            };

            oferta.hoja_vida_eliminada.push(hojaVida);
          }else{
            hojas.push(data);  
          }
        });

        oferta.hoja_vida_enviada = hojas;
        if (!('usado' in oferta)) {
          oferta.usado = 0;
        }else{
          oferta.usado = oferta.usado - 1;  
        }
        
        
        oferta.save((err, saved) => {
          console.log(err);      
          console.log(saved);
          if (err) {
              res.cookie('mensaje','error|Error Transacción : '+err.message);
              return res.redirect(req.header('Referer') || '/');
          }

          global.models.HojaDeVida.findOneAndUpdate(
            {_id: req.body.set}, 
            {estado: 'Recolectada'},
            (err) => {
            return res.json({err});
          });
        })
    })
  });

  route.get('/:path/limpiar', (req, res) => {
    let model = global.getModel(req.params.path);
    if (!model) return res.sendStatus(500);

    switch (req.params.path) {
      case 'ctoPersonajes':
        model.find()
        .exec((error, record) => {
          if (error) {
            console.log(error);
            res.cookie('mensaje','error|Error Transacción : '+error.message);
            res.sendStatus(404);
          }

          _.each(record, (personaje) => {
            if(!personaje.potencial_declarado_urna > 0){
              personaje.potencial_declarado_urna = 10;  
            }
            personaje.potencial_declarado_otro = 1;
            personaje.potencial_esperado = 1;
            personaje.potencial_real = 1;
            personaje.color = 'default';

            personaje.save((err, saved) => {
              if (err) {
                  console.log('===== > err');
                  console.log(err);
              }
            })
          });
          res.cookie('mensaje','success|Modelo Limpiado');
          return res.redirect('/admin')      
        });
      break;
      default :
        model.deleteMany({},(error, record) => {
          if (error) {
            console.log(error);
            res.cookie('mensaje','error|Error Transacción : '+error.message);
            res.sendStatus(404);
          } 
          res.cookie('mensaje','success|Modelo Limpiado');
          return res.redirect('/admin')      
        });
      break;
    }    
  });


  route.post('/:path/delete', (req, res) => {
    let model = global.getModel(req.params.path);
    if (!model) return res.sendStatus(500);

    model.findOne({ _id: req.body._id }).exec((error, record) => {
        if (!error) {
          record.delete(mongoose.Types.ObjectId(global.user._id), (err) => {
              if (err) {
                    res.sendStatus(500);
              } else {
                    res.sendStatus(200);
              }
          });
        } else {
          res.cookie('mensaje','error|Error Transacción : '+error.message);
          res.sendStatus(404);
        }
    });
  });

  route.post('/personajes/:id/organizacion', (req, res) => {
    global.models.Personaje.findOne({
      _id: req.params.id
    }).exec((err, personaje) => {
      if(err) {
        return res.json({err});
      }

      let old = personaje.organizacion;
      personaje.organizacion = req.body.organizacion;
      personaje.save((err, saved) => {
        if(err) {
          return res.json({err});
        }

        // Actualizo la organización anterior para sacar al usuario de ahi
        global.models.Organizacion.update(
          {_id: old},
          {$pull: { integrantes: {_id: personaje._id}, representantes: {_id: personaje._id}}},
          {safe: true}, (err) => {
          // Actualizo la organización actual con el usuario
          if(err) {
            return res.json({err});
          }
          
          global.models.Organizacion.findOneAndUpdate(
            {_id: req.body.organizacion}, 
            {$addToSet: {integrantes: personaje._id}},
            (err) => {
            return res.json({err});
          });
        });
      });
    });
  });

  route.post('/asesor/:id', (req, res) => {
      
    global.models.Personaje.findOne({_id: req.params.id}).exec((err, personaje) => {
      if(err) {
        res.cookie('mensaje','error|Error Transacción : '+err.message);
        return res.json({err});
      }

      if(req.body.asesor == 0){
        console.log('Eliminando Asesor')
        personaje.asesor = null;
        delete personaje.asesor;
      }else{
        personaje.asesor = req.body.asesor;  
      }
      
      personaje.save((err, saved) => {
        if(err) {
          res.cookie('mensaje','error|Error Transacción : '+err.message);
          return res.json({err});
        }
        res.json(saved);
      });
    });  
  });

  route.post('/grupo/:id', (req, res) => {
      
    global.models.Personaje.findOne({_id: req.params.id}).exec((err, personaje) => {
      if(err) {
        res.cookie('mensaje','error|Error Transacción : '+err.message);
        return res.json({err});
      }

      if(req.body.grupo == 0){
        console.log('Eliminando Grupo')
        personaje.grupo = null;
        delete personaje.grupo;
      }else{
        personaje.grupo = req.body.grupo;  
      }
      
      personaje.save((err, saved) => {
        if(err) {
          res.cookie('mensaje','error|Error Transacción : '+err.message);
          return res.json({err});
        }
        res.json(saved);
      });
    });  
  });

  route.post('/task/:id', (req, res) => {
      global.models.Proyecto.update(
        { 'tareas._id': req.params.id },
        { 'tareas.$.finalizado': req.body.checked },
        (err, numAffected) => {
          res.cookie('mensaje','success|Transacción finalizada');
          res.json({updated: numAffected});
        }
      )
  });

  route.post('/tarea/update', (req, res) => {
      global.models.Proyecto.update(
        { 'tareas._id': req.body._id },
        req.body.set,
        (err, numAffected) => {
          res.json({error: err, updated: numAffected});
        }
      )
  });

  route.post('/tarea/comentario', (req, res) => {
    let comment = global.models.Comentario({
      usuario: global.user._id,
      model: 'Tarea',
      referencia: req.body._id,
      comentario: req.body.comentario
    });

    comment.save((err, comment) => {
      if(!err) {
        global.models.Proyecto.update(
          { 'tareas._id': req.body._id },
          { $push: { 'tareas.$.comentarios': comment._id } },
          (err, numAffected) => {
            res.json({error: err, updated: numAffected, _id: comment._id});
          }
        )
      }
    });
  });

  route.post('/tarea/comentario/delete', (req, res) => {
   global.models.Proyecto.update(
      { 'tareas._id': req.body._id },
      { $pull: { 'tareas.$.comentarios': req.body.comentario } },
      (err, numAffected) => {
        if(err == null) {
          global.models.Comentario.remove({_id: req.body.comentario}).exec((err, numAffected) => {
            res.cookie('mensaje','success|Registro eliminado');
            res.json({error: err, updated: numAffected});
          });
        } else {
          res.cookie('mensaje','error|Error Transacción : '+err.message);
          res.json({error: err, updated: numAffected});
        }
      }
    );
  });

  route.get('/document/:id', (req, res) => {
    global.models.Personaje.findOne({documento: req.params.id}).lean().exec((err, personaje) => {
      if(!personaje) {

        global.models.Contacto.findOne({documento: req.params.id}).lean().exec((err, contacto) => {          
          Antecedentes(req.params.id, (details) => {
            if(!details) {
              res.json({error: 'El documento no existe en la registraduria nacional.'});
              return;
            }

            // Si el usuario existe, busco el puesto de votación
            PuestoVotacion(req.params.id, (puesto) => {
              if(contacto) {
                res.json({
                  nombre: details.name,
                  antecedentes: details.antecedentes,
                  puesto_votacion: puesto,
                  info: 'El documento ya se encuentra registrado como contacto.'
                });  
              }else{
                res.json({
                  nombre: details.name,
                  antecedentes: details.antecedentes,
                  puesto_votacion: puesto
                });  
              }
            });
          });

        });
      } else {
        res.json({error: 'El documento ya se encuentra registrado.'});
      }
    });
  });

  route.get('/personaje/:id', (req, res) => {
    global.models.Personaje.findOne({documento: req.params.id}).lean().exec((err, personaje) => {
      if(!personaje) {
        res.json({error: 'El documento No se encuentra registrado.'});
      }else{
        res.json(personaje);
      }
    });
  });

  //Cruce contactos
  route.get('/contactos/cargar', auth, (req, res) => {
    let asyncArray = [];

    asyncArray.push((callback) => {
      global.models.Grupo.findOne({nombre : 'PERSONAS AMIGAS'}).lean().exec((err, grupo) => {
        callback(null, grupo._id);        
      });
      
    });

    asyncArray.push((callback) => {
      let arrPersonaje = [];
      global.models.Personaje.find().lean().exec((err, personajes) => {
        _.each(personajes, (doc) => {
          if(!doc.grupo){
            arrPersonaje.push({_id : doc._id, nombre: doc.nombre});
          }
        });
        callback(null, arrPersonaje);
      });
    });

    asyncArray.push((callback) => {
      global.models.Personaje.updateMany({},{$set: {representante: false} }).exec((err, coll) => {
        console.log('err');
        console.log(err);
        console.log('coll');
        console.log(coll);
        callback(null, coll);        
      });
      
    });
    
    async.parallel(asyncArray, (err, data) => {
      if (err) {
        console.log('error Par');
        console.log(err);
      }
      console.log('data');
      console.log(data);
      _.each(data[1], (personaje) => {
        global.models.Personaje.findOne({_id : personaje._id}).exec((err, doc) =>{
          if(err){
            console.log('err');
            console.log(err);
          }
          console.log('doc');
          console.log(doc);
          doc.grupo = data[0];
          doc.save((err, local) => {
            if(err){
              console.log('err');
              console.log(err);
            }

            console.log('GUARDADO !!!');
            console.log(local);
          });
        });
      });
    });

    /*

    let asyncArray = [];

    asyncArray.push((callback) => {
        global.models.Personaje.find({ potencial_declarado_urna : {$gt:3} }).lean().exec((err, personaje) => {
            if(err){
              console.log('err');
              console.log(err);
            }
            callback(null, personaje);                     
        })
    });

    async.parallel(asyncArray, (err, data) => {
      if (err) {
        console.log('error Par');
        console.log(err);
      }

      _.each(data[0], (personaje) => {
        console.log(personaje._id+' => '+personaje.nombre);

        global.models.Personaje.findOne({_id : personaje._id}, function(err, doc){
          if(err){
            console.log('err');
            console.log(err);
          }

          console.log('doc');
          console.log(doc);
          doc.representante = true;

          doc.save((err, local) => {
            if(err){
              console.log('err');
              console.log(err);
            }

            console.log('doc');
            console.log(local);
          });
          
        });
      });

    });

    global.models.Personaje.find({representante : true}).lean().exec((err, found) => {
      _.each(found, (personaje) => {
        global.models.User.findOne({personaje : personaje._id}).lean().exec((err,creador) =>{
          if(creador){
            global.models.Contacto.find({creador : creador._id}).lean().exec((err,contactos) => {
              _.each(contactos, (contacto) => {
                if(contacto.documento){
                  let personaje_new = {
                    documento : contacto.documento,
                    nombre : contacto.nombre,
                    direccion : contacto.direccion,
                    telefono_fijo : contacto.telefono_fijo,
                    telefono_movil : contacto.telefono_movil,
                    email : contacto.email_personal,
                    referido : personaje._id,
                    color : 'warning',
                    potencial_declarado_urna : 1,
                    potencial_esperado : 1,
                    potencial_real : 1,
                    descripcion : 'Contacto cargado por referido',
                    creador : creador._id,
                  };

                  let new_personaje = new global.models.Personaje(personaje_new);

                  new_personaje.save((err, local) => {
                      if(err){
                          console.log('error al crear personaje-contacto');
                          console.log(err);
                      }
                  });  
                }
              });
            });            
          }
        });
      });
    });
    */
   res.json('found');
  });

  // Devuelve un arreglo de personajes que coinciden con la búsqueda por nombre
  route.get('/search/ofertas', auth, (req, res) => {
    let query = {};

    if(req.query.categoria) {
      query.categoria = new RegExp(req.query.categoria, 'ig');
    }

    if(req.query.buscar) {
      query.$or = [
        { tipo: new RegExp(req.query.buscar, 'ig') },
        { descripcion: new RegExp(req.query.buscar, 'ig') }
      ];
    }

    global.models.Oferta
      .find(query)
      .select('tipo descripcion categoria fecha_fin personaje')
      .populate('personaje')
      .limit(10)
      .lean()
      .exec((err, found) => {
        if(err) throw err;
        res.json(found);
      });
  });

  route.get('/search/hojasdevida', auth, (req, res) => {

    let asyncHV = [];
    asyncHV.push((callback) => {
      global.models.HojaDeVida.find({ personaje: req.query.personaje, estado: "Recolectada"  }).lean().populate('personaje').exec((err, referidos) => {
        callback(null, referidos);
      });
    });

    asyncHV.push((callback) => {
      global.models.HojaDeVida.find({ referido: req.query.personaje, estado: "Recolectada" }).lean().populate('personaje').exec((err, personajes) => {
        callback(null, personajes);
      });
    });

    async.parallel(asyncHV, (err, data) => {
        if (err) {
            res.cookie('mensaje','error|Error Transacción : '+err.message);
        }

        let final = [];

        _.each(data, (datos) => {
          if(datos[0]){
            final.push(datos[0]);
          }
          
        });
        res.json(final);
    });
  });

  //Genera un usuario Lider 
  route.get('/userAdd/:id', (req, res) => {
    global.models.User.findOne({personaje: req.params.id}).lean().exec((err, usuario) => {
      if(usuario){
        res.redirect('/admin/usuarios/'+usuario._id);
      }else{
        global.models.Personaje.findOne({_id: req.params.id}).lean().exec((err, personaje) => {
          if(err) throw err;

          if(!personaje.email_personal){
            res.json({error: 'el personaje no registra email para procesar.'});
          }else{
            bcrypt.hash(personaje.documento, null, null, function(err, hashed) {
              if (err) throw err;

              global.models.User.create([{
                  name: personaje.nombre,
                  email: personaje.email_personal,
                  role : 'lider',
                  creador: global.user._id,
                  documento : personaje.documento,
                  personaje: req.params.id,
                  password: hashed,
                  active: true
              }], () => {
                  res.redirect('/api/logout');
              });

            });
          }        
        });  
      }
    });      
  });

  // Devuelve un arreglo de personajes que coinciden con la búsqueda por nombre
  route.get('/search/:path', auth, (req, res) => {
    let model = global.getModel(req.params.path);
    if(!model) {
      return res.json({results: [], error: 'Model Not found'});
    }

    let query = {
      nombre: new RegExp(req.query.q, 'ig')
    };

    if(global.getModelName(req.params.path) == 'User') {
      query = {
        name: new RegExp(req.query.q, 'ig')
      };
    }

    if(req.params.path == 'representante') {
      query.representante = true;
    }

    if('organizacion' in req.query) {
      query.organizacion = req.query.organizacion;
    }

    model.find(query).limit(10).lean().exec((err, found) => {
      if(err) throw err;
      if(found) {
        let result = [];

        _.each(found, (e) => {
          result.push({id: e._id, text: e.nombre || e.name});
        });

        res.json({results: result});
      } else {
        res.json({results: []});
      }
    });
  });

  route.get('/projectos/tareas', auth, (req, res) => {
    let model = global.models.Proyecto;

    let date = moment(req.query.date);
    let query = {
      'tareas.fecha_fin': {$lt: date}
    };

    model.find(query).populate('tareas.responsable').lean().exec((err, proyectos) => {
      if(err) throw err;

      let tareas = [];
      _.each(proyectos, (proyecto) => {
        _.each(proyecto.tareas, (tarea) => {
          tareas.push({
            title: tarea.responsable.nombre,
            start: tarea.fecha_inicio,
            end: tarea.fecha_fin
          });
        });
      });

      res.json({events: tareas});
    });
  });

  route.get('/seen/:model/:id.png', (req, res) => {
    switch(req.params.model) {
      case 'tarea':
        global.models.Proyecto.update(
          { 'tareas._id': req.params.id },
          { 'tareas.$.visto': true },
          (err, numAffected) => {
            // Marca como visto:
            var img = new Buffer('iVBORw0KGgoAAAANSUhEUgAAAPAAAAE', 'base64');

            res.writeHead(200, {
               'Content-Type': 'image/png',
               'Content-Length': img.length
            });

            res.end(img);
          }
        );
      break;
    }
  })

  app.use('/api', route);
}
