"use strict";
process.env.TZ = 'America/Bogota';
const cron = require('node-cron');
const PORT = process.env.PORT || 80;
/* Módulos */
const express       = require('express');
const request       = require('request');
const exphbs        = require('express-hbs');
const sendinblue  = require('sendinblue-api');
const mail = new sendinblue({ "apiKey": 'K70dxg53vfG2PHhp', "timeout": 5000 });

global.sendMail = (name, email, subject, render, content, callback) => {
  let defaults = {
    subject: subject,
    from: ['info@bogotasoyyo.co', 'Información Importante']
  };

  content.layout = 'layouts/email';
  app.render('emails/' + render, content, function(err, html) {
    /*let send_email = {};
        send_email[email] = name;
    */
    mail.send_email({
      to: email,
      from: defaults.from,
      subject: defaults.subject,
      html: html
    }, function(res) {
      console.log('Respuesta Cron SendInBlue:', arguments);

      try {
        callback(arguments);
      } catch(e) { console.log(e); }
    });
  });
};

// Ejecuta los crons
// 
cron.schedule('*/5 * * * *', function(){
  console.log(' ======= > Reinicia Token');
  require('./crons/pass_personaje')();
});

cron.schedule('45 22 * * *', function(){
  console.log(' ======= > Actualiza Puesto ');
  require('./crons/actualiza_puesto')();
});


cron.schedule('50 15 * * *', function(){
  console.log(' ======= > Tarea 1');
  //require('./crons/reporte_dia')();
});


cron.schedule('00 00 00 * * 1', function(){
  console.log(' ======= > Ejecutando Cron ...');
  require('./crons/recordatorios')();
});
